<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Profitloss extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function openingStock($item){
        $company = Auth::user()->company_id;
        $data = DB::table('openingstock')
			->select(DB::raw("SUM(amount) as amount"),DB::raw("SUM(quantity) as quantity"))
			->where([
                ['status','1'],
                ['item_id',$item],
				['company_id',$company]
				])
            ->first();
        return $data;
    }
    public function purchase($from,$to,$item){
        $company = Auth::user()->company_id;
        $data = DB::table('purchaseregister')
			->select(DB::raw("SUM(purchaseregister_item.amount) as amount"),DB::raw("SUM(purchaseregister_item.tax_amount) as tax"),DB::raw("SUM(purchaseregister_item.quantity) as quantity"))
			->leftJoin('purchaseregister_item','purchaseregister_item.parent_id','=','purchaseregister.id')
			->where([
                ['status','1'],
                ['purchaseregister_item.item_id',$item],
				['company_id',$company]
                ])
            ->whereBetween('entry_date', array($from, $to))
			->first();
        return $data;
    }
    public function purchaseReturn($from,$to,$item){
        $company = Auth::user()->company_id;
        $data = DB::table('purchasereturn')
            ->select(DB::raw("SUM(purchasereturn_item.return_amount) as amount"),DB::raw("SUM(purchasereturn_item.return_quantity) as quantity"))
            ->leftJoin('purchasereturn_item','purchasereturn_item.parent_id','=','purchasereturn.id')
			->where([
                ['status','1'],
                ['purchasereturn_item.item_id',$item],
				['company_id',$company]
                ])
            ->whereBetween('debitnote_date', array($from, $to))
			->first();
		return $data;
    }
    public function sale($from,$to,$item){
        $company = Auth::user()->company_id;
        $data = DB::table('saleregister')
			->select(DB::raw("SUM(saleregister_item.amount) as amount"),DB::raw("SUM(saleregister_item.quantity) as quantity"))
			->leftJoin('saleregister_item','saleregister_item.parent_id','=','saleregister.id')
			->where([
                ['status','1'],
                ['saleregister_item.item_id',$item],
				['company_id',$company]
                ])
            ->whereBetween('entry_date', array($from, $to))
			->first();
        return $data;
    }
    public function saleReturn($from,$to,$item){
        $company = Auth::user()->company_id;
        $data = DB::table('salereturn')
            ->select(DB::raw("SUM(salereturn_item.return_amount) as amount"),DB::raw("SUM(salereturn_item.return_quantity) as quantity"))
            ->leftJoin('salereturn_item','salereturn_item.parent_id','=','salereturn.id')
			->where([
                ['status','1'],
                ['salereturn_item.item_id',$item],
				['company_id',$company]
                ])
            ->whereBetween('creditnote_date', array($from, $to))
			->first();
		return $data;
    }
    public function accountGroup($group_name){
        $company = Auth::user()->company_id;
        $data = DB::table('account_group')
			->select('id')
			->where([
                ['name',$group_name],
                ['status','1'],
				['company_id',$company]
                ])
			->first();
        return $data->id;
    }
    public function getAccounts($account_group_id){
        $company = Auth::user()->company_id;
        $data = DB::table('account')
			->select('id')
			->where([
                ['account_group_id',$account_group_id],
                ['status','1'],
				['company_id',$company]
                ])
			->get();
        return $data;
    }
    public function income($from,$to,$account_id){
        $company = Auth::user()->company_id;
        $data = DB::table('account_txn')
			->select('debit')
			->where([
                ['account_id',$account_id],
                ['status','1'],
				['company_id',$company]
                ])
            ->whereBetween('created_at', array($from, $to))
			->first();
        return $data->debit;
    }
    public function expense($from,$to,$account_id){
        $company = Auth::user()->company_id;
        $data = DB::table('account_txn')
			->select('credit')
			->where([
                ['account_id',$account_id],
                ['status','1'],
				['company_id',$company]
                ])
            ->whereBetween('created_at', array($from, $to))
			->first();
        return $data->credit;
    }
}
