<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function bank_list()
    {
        $company = Auth::user()->company_id;
        $bank = DB::table('bank')
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->get();
        return $bank;
    }
    public function bank_add($company, $bank_name, $sort_code, $bank_address, $account_no, $account_name)
    {
        $result = DB::transaction(function () use ($company, $bank_name, $sort_code, $bank_address, $account_no, $account_name) {
            $user_id = Auth::id();
            $account_group_id = null;
            $account_group = DB::table('account_group')
                ->select('id')
                ->where([
                    ['company_id', $company],
                    ['name', 'Creditors'],
                    ['type', 'CR'],
                    ['status', '1'],
                ])
                ->first();
            if ($account_group) {
                $account_group_id = $account_group->id;
            }
            $account_id = DB::table('account')->insertGetId(
                ['company_id' => $company, 'type' => 'CR', 'name' => $bank_name, 'account_group_id' => $account_group_id, 'created_at' => $this->date]
            );
            $bank_id = DB::table('bank')->insertGetId(
                ['company_id' => $company, 'bank_name' => $bank_name, 'sort_code' => $sort_code, 'bank_address' => $bank_address, 'account_no' => $account_no, 'account_name' => $account_name, 'account_id' => $account_id, 'created_at' => $this->date, 'created_by' => $user_id]
            );
            return $bank_id;
        });
        return $result;

    }
    public function bank_edit($id)
    {
        return DB::table('bank')->where('id', $id)->first();
    }
    public function bank_update($id, $company, $bank_name, $sort_code, $bank_address, $account_no, $account_name)
    {
        $user_id = Auth::id();
        return DB::table('bank')
            ->where('id', $id)
            ->update(['company_id' => $company, 'bank_name' => $bank_name, 'sort_code' => $sort_code, 'bank_address' => $bank_address, 'account_no' => $account_no, 'account_name' => $account_name, 'updated_at' => $this->date, 'updated_by' => $user_id]);
    }
    public function bank_delete($id)
    {
        $user_id = Auth::id();
        return DB::table('bank')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date, 'updated_by' => $user_id]);
    }
}
