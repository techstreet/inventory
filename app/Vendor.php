<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function vendor_list()
    {
        $company = Auth::user()->company_id;
        return DB::table('vendor')
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->get();
    }
    public function vendor_add($company, $name, $contact_name, $email, $address, $phone, $mobile)
    {
        $result = DB::transaction(function () use ($company, $name, $contact_name, $email, $address, $phone, $mobile) {
            $user_id = Auth::id();
            $account_group_id = null;
            $account_group = DB::table('account_group')
                ->select('id')
                ->where([
                    ['company_id', $company],
                    ['name', 'Creditors'],
                    ['type', 'CR'],
                    ['status', '1'],
                ])
                ->first();
            if ($account_group) {
                $account_group_id = $account_group->id;
            }
            $account_id = DB::table('account')->insertGetId(
                ['company_id' => $company, 'type' => 'CR', 'account_group_id' => $account_group_id, 'name' => $name, 'email' => $email, 'address' => $address, 'phone' => $phone, 'mobile' => $mobile, 'created_at' => $this->date]
            );
            $vendor_id = DB::table('vendor')->insertGetId(
                ['company_id' => $company, 'name' => $name, 'contact_name' => $contact_name, 'email' => $email, 'address' => $address, 'phone' => $phone, 'mobile' => $mobile, 'created_at' => $this->date, 'created_by' => $user_id]
            );
            return $vendor_id;
        });
        return $result;

    }
    public function vendor_edit($id)
    {
        return DB::table('vendor')->where('id', $id)->get();
    }
    public function vendor_update($id, $company, $name, $contact_name, $email, $address, $phone, $mobile)
    {
        $user_id = Auth::id();
        return DB::table('vendor')
            ->where('id', $id)
            ->update(['company_id' => $company, 'name' => $name, 'contact_name' => $contact_name, 'email' => $email, 'address' => $address, 'phone' => $phone, 'mobile' => $mobile, 'updated_at' => $this->date, 'updated_by' => $user_id]);
    }
    public function vendor_delete($id)
    {
        $user_id = Auth::id();
        return DB::table('vendor')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date, 'updated_by' => $user_id]);
    }
}
