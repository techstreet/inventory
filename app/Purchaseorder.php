<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Purchaseorder extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function purchaseorder_list()
    {
        $company = Auth::user()->company_id;
        return DB::table('purchaseorder')
            ->select('purchaseorder.*', 'vendor.name as vendor_name', 'vendor.address as vendor_address', 'vendor.phone as vendor_phone', 'vendor.mobile as vendor_mobile')
            ->where([
                ['purchaseorder.status', '1'],
                ['purchaseorder.company_id', $company],
            ])
            ->leftJoin('vendor', 'purchaseorder.vendor_id', '=', 'vendor.id')
            ->orderBy('purchaseorder.id', 'desc')
            ->get();
    }
    public function item_list($category_id)
    {
        return DB::table('item')->where('category_id', $category_id)->get();
    }
    public function category_list($id)
    {
        $item = DB::table('item')->where('id', $id)->get();
        foreach ($item as $value) {
            $company_id = $value->company_id;
        }
        $category = DB::table('category')->where('company_id', $company_id)->get();
        return $category;
    }
    public function category_id($item_id)
    {
        $item = DB::table('item')->where('id', $item_id)->get();
        foreach ($item as $value) {
            $category_id = $value->category_id;
        }
        return $category_id;
    }
    public function getPurchaseorderName($id)
    {
        $purchaseorder = DB::table('purchaseorder')->where('id', $id)->get();
        foreach ($purchaseorder as $value) {
            $name = $value->name;
        }
        return $name;
    }
    public function purchaseorder_add($company, $po_no, $entry_date, $vendor, $item, $remarks, $item_id, $unit_id, $quantity, $rate, $amount)
    {
        $result = DB::transaction(function () use ($company, $po_no, $entry_date, $vendor, $item, $remarks, $item_id, $unit_id, $quantity, $rate, $amount) {
            $user_id = Auth::id();
            $entry_date = date_format(date_create($entry_date), "Y-m-d");
            $sub_total = collect($amount)->sum();
            //$purchaseorder_no = 'PO_'.str_pad($po_no+1, 4, 0, STR_PAD_LEFT);
            $purchaseorder_no = $po_no;
            $purchaseorder_id = DB::table('purchaseorder')->insertGetId(
                ['company_id' => $company, 'purchaseorder_no' => $purchaseorder_no, 'entry_date' => $entry_date, 'vendor_id' => $vendor, 'sub_total' => $sub_total, 'total' => $sub_total, 'remarks' => $remarks, 'created_at' => $this->date, 'created_by' => $user_id]
            );
            if (count($item_id > 0)) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $quantity1 = $quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    if ($item_id1 != '') {
                        DB::table('purchaseorder_item')->insert(['parent_id' => $purchaseorder_id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'rate' => $rate1, 'amount' => $amount1]);
                    }
                }
            }
            return $purchaseorder_id;
        });
        return $result;
    }
    public function purchaseorder_edit($id)
    {
        return DB::table('purchaseorder')
            ->select('purchaseorder.*', 'vendor.name as vendor_name')
            ->where('purchaseorder.id', $id)
            ->leftJoin('vendor', 'purchaseorder.vendor_id', '=', 'vendor.id')
            ->get();
    }
//function updated by anurag company address
    public function purchaseorder_view($id)
    {
        return DB::table('purchaseorder')
            ->select('purchaseorder.*', 'vendor.name as vendor_name', 'vendor.mobile as vendor_mobile', 'company.name as company_name', 'users.address as address')
            ->where('purchaseorder.id', $id)
            ->leftJoin('vendor', 'purchaseorder.vendor_id', '=', 'vendor.id')
            ->leftJoin('company', 'purchaseorder.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->get();
    }
    public function purchaseorder_item($id)
    {
        return DB::table('purchaseorder')
            ->select('purchaseorder_item.*', 'item.name as item_name', 'item.batch_no as batch_no', 'item.strength', 'item.form_id', 'unit.name as unit_name', 'brand.name as brand_name', 'form.name as form_name')
            ->where([
                ['purchaseorder.id', $id],
                ['purchaseorder.status', '1'],
            ])
            ->leftJoin('purchaseorder_item', 'purchaseorder_item.parent_id', '=', 'purchaseorder.id')
            ->leftJoin('item', 'purchaseorder_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->get();
    }
    public function purchaseorder_update($id, $company, $entry_date, $vendor, $item, $remarks, $item_id, $unit_id, $quantity, $rate, $amount)
    {
        $result = DB::transaction(function () use ($id, $company, $entry_date, $vendor, $item, $remarks, $item_id, $unit_id, $quantity, $rate, $amount) {
            $user_id = Auth::id();
            $entry_date = date_format(date_create($entry_date), "Y-m-d");
            $sub_total = collect($amount)->sum();

            DB::table('purchaseorder')
                ->where('id', $id)
                ->update(['company_id' => $company, 'entry_date' => $entry_date, 'vendor_id' => $vendor, 'sub_total' => $sub_total, 'total' => $sub_total, 'remarks' => $remarks, 'updated_at' => $this->date, 'updated_by' => $user_id]);

            DB::table('purchaseorder_item')
                ->where('parent_id', $id)
                ->delete();

            if (count($item_id > 0)) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $quantity1 = $quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    if ($item_id1 != '') {
                        DB::table('purchaseorder_item')->insert(['parent_id' => $id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'rate' => $rate1, 'amount' => $amount1]);
                    }
                }
            }
            return true;
        });
        return $result;
    }
    public function purchaseorder_delete($id)
    {
        $user_id = Auth::id();
        return DB::table('purchaseorder')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date, 'updated_by' => $user_id]);
    }
    public function ajax($company_id)
    {
        if (!empty($company_id)) {
            $category = DB::table('category')->where([
                ['status', '1'],
                ['company_id', $company_id],
            ])->get();
            $category_count = $category->count();
            if ($category_count > 0) {
                ?><option value="">Select</option><?php
foreach ($category as $value) {?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
            } else {
                ?><option value="">Select</option><?php
}
        } else {
            ?><option value="">Select</option><?php
}
    }
    public function ajax2($category_id)
    {
        if (!empty($category_id)) {
            $item = DB::table('item')->where([
                ['status', '1'],
                ['category_id', $category_id],
            ])->get();
            $item_count = $item->count();
            if ($item_count > 0) {
                ?><option value="">Select</option><?php
foreach ($item as $value) {?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
            } else {
                ?><option value="">Select</option><?php
}
        } else {
            ?><option value="">Select</option><?php
}
    }
    public function additems($item_id)
    {
        $items = DB::table('item')
            ->select('item.*', 'unit.name as unit_name', 'form.name as form_name', 'brand.name as brand_name')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->where('item.id', $item_id)
            ->get();
        print_r(json_encode(array($items)));
    }
}
