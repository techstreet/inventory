<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Purchaseregister extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function purchaseregister_list()
    {
        $company = Auth::user()->company_id;
        return DB::table('purchaseregister')
            ->select('purchaseregister.*', 'vendor.name as vendor_name')
            ->where([
                ['purchaseregister.status', '1'],
                ['purchaseregister.company_id', $company],
            ])
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->orderBy('purchaseregister.id', 'desc')
            ->get();
    }
    public function item_list($category_id)
    {
        return DB::table('item')->where('category_id', $category_id)->get();
    }
    public function category_list($id)
    {
        $item = DB::table('item')->where('id', $id)->get();
        foreach ($item as $value) {
            $company_id = $value->company_id;
        }
        $category = DB::table('category')->where('company_id', $company_id)->get();
        return $category;
    }
    public function category_id($item_id)
    {
        $item = DB::table('item')->where('id', $item_id)->get();
        foreach ($item as $value) {
            $category_id = $value->category_id;
        }
        return $category_id;
    }
    public function getPurchaseregisterName($id)
    {
        $purchaseregister = DB::table('purchaseregister')->where('id', $id)->get();
        foreach ($purchaseregister as $value) {
            $name = $value->name;
        }
        return $name;
    }
    public function purchaseregister_addbyitem($company, $voucher_no, $entry_date, $bill_no, $bill_date, $vendor, $item, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $rate, $amount, $taxP, $taxA, $discount, $tax_percentage)
    {
        $result = DB::transaction(function () use ($company, $voucher_no, $entry_date, $bill_no, $bill_date, $vendor, $item, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $rate, $amount, $taxP, $taxA, $discount, $tax_percentage) {
            $user_id = Auth::id();
            if (!empty($entry_date)) {
                $entry_date = date_format(date_create($entry_date), "Y-m-d");
            }
            if (!empty($bill_date)) {
                $bill_date = date_format(date_create($bill_date), "Y-m-d");
            }
            $purchasevoucher_no = 'PV_' . str_pad($voucher_no + 1, 4, 0, STR_PAD_LEFT);
            $sub_total = collect($amount)->sum();

            $total = $sub_total - $discount;
            $tax = collect($taxA)->sum();
            $final_total = $sub_total - $discount + $tax;

            $purchaseregister_id = DB::table('purchaseregister')->insertGetId(
                ['company_id' => $company, 'purchasevoucher_no' => $purchasevoucher_no, 'entry_date' => $entry_date, 'bill_no' => $bill_no, 'bill_date' => $bill_date, 'vendor_id' => $vendor, 'sub_total' => $sub_total, 'discount' => $discount, 'tax' => $tax, 'total' => $final_total, 'remarks' => $remarks, 'created_at' => $this->date, 'created_by' => $user_id]
            );

            /*///////////////////Account Mange-Start///////////////////*/

            /*............................
            Account Manage-Purchase Voucher
            ..............................*/

            $max_counter = '1';
            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Purchase Voucher' and company_id=$company and status='1'")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }
            $voucher_no = 'PU' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;

            // 1st Transaction
            $account_id = getAcccountId('name', 'Purchase', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 2nd Transaction
            if($tax > 0){
                $account_id = getAcccountId('name', 'Tax', $company);
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $tax, 'created_at' => $this->date, 'status' => '1']
                );
            }
            // 3rd Transaction
            $account_id = getVendorAcccountId($vendor, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $sub_total+$tax, 'created_at' => $this->date, 'status' => '1']
            );
            // 4th & 5th Transaction
            if($discount > 0){
                // 4th Transaction
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $discount, 'created_at' => $this->date, 'status' => '1']
                );
                // 5th Transaction
                $account_id = getAcccountId('name', 'Discount', $company);
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $discount, 'created_at' => $this->date, 'status' => '1']
                );
            }
            
            // Update Voucher No in Purchase Register
            DB::table('purchaseregister')
                ->where('id', $purchaseregister_id)
                ->update(['voucher_no' => $voucher_no,'unique_no' => 'PV'.$purchaseregister_id]);

            /*.................................
            Account Manage - Receipt Voucher
            ...................................*/
            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Payment Voucher' and company_id=$company and status='1'")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }
            $voucher_no = 'PA' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;
            // 6th Transaction
            $account_id = getVendorAcccountId($vendor, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Payment Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $final_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 7th Transaction
            $account_id = getAcccountId('name', 'Cash', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Payment Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $final_total, 'created_at' => $this->date, 'status' => '1']
            );

            /*///////////////////Account Mange-End///////////////////*/

            if (count($item_id) > 0) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $barcode1 = $barcode[$i];
                    $expiry_date1 = $expiry_date[$i];
                    if (!empty($expiry_date1)) {
                        $expiry_date1 = date_format(date_create($expiry_date1), "Y-m-d");
                    }
                    $quantity1 = $quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    $taxP1 = $taxP[$i];
                    $taxA1 = $taxA[$i];
                    if ($item_id1 != '') {

                        if (empty($taxP1)) {
                            $taxP1 = 0;
                        }

                        DB::table('purchaseregister_item')->insert(['parent_id' => $purchaseregister_id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'rate' => $rate1, 'amount' => $amount1, 'tax_rate' => $taxP1, 'tax_amount' => $taxA1, 'barcode' => $barcode1, 'expiry_date' => $expiry_date1]);

                    }
                }
            }
            return $purchaseregister_id;
        });
        return $result;
    }
    public function purchaseregister_addbypo($company, $voucher_no, $entry_date, $bill_no, $bill_date, $vendor, $purchaseorder_no, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $rate, $amount, $taxP, $taxA, $discount, $tax_percentage)
    {
        $result = DB::transaction(function () use ($company, $voucher_no, $entry_date, $bill_no, $bill_date, $vendor, $purchaseorder_no, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $rate, $amount, $taxP, $taxA, $discount, $tax_percentage) {
            $user_id = Auth::id();
            $purchaseorder_no = strtoupper($purchaseorder_no);
            if (!empty($entry_date)) {
                $entry_date = date_format(date_create($entry_date), "Y-m-d");
            }
            if (!empty($bill_date)) {
                $bill_date = date_format(date_create($bill_date), "Y-m-d");
            }
            $purchasevoucher_no = 'PV_' . str_pad($voucher_no + 1, 4, 0, STR_PAD_LEFT);
            $sub_total = collect($amount)->sum();
            $total = $sub_total - $discount;
            $tax = collect($taxA)->sum();
            $final_total = $sub_total - $discount + $tax;

            $purchaseregister_id = DB::table('purchaseregister')->insertGetId(
                ['company_id' => $company, 'purchasevoucher_no' => $purchasevoucher_no, 'entry_date' => $entry_date, 'bill_no' => $bill_no, 'bill_date' => $bill_date, 'vendor_id' => $vendor, 'purchaseorder_no' => $purchaseorder_no, 'sub_total' => $sub_total, 'discount' => $discount, 'tax' => $tax, 'total' => $final_total, 'remarks' => $remarks, 'created_at' => $this->date, 'created_by' => $user_id]
            );

            /*///////////////////Account Mange-Start///////////////////*/

            /*............................
            Account Manage-Purchase Voucher
            ..............................*/

            $max_counter = '1';
            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Purchase Voucher' and company_id=$company and status='1'")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }
            $voucher_no = 'PU' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;

            // 1st Transaction
            $account_id = getAcccountId('name', 'Purchase', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 2nd Transaction
            if($tax > 0){
                $account_id = getAcccountId('name', 'Tax', $company);
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $tax, 'created_at' => $this->date, 'status' => '1']
                );
            }
            // 3rd Transaction
            $account_id = getVendorAcccountId($vendor, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $sub_total+$tax, 'created_at' => $this->date, 'status' => '1']
            );
            // 4th & 5th Transaction
            if($discount > 0){
                // 4th Transaction
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $discount, 'created_at' => $this->date, 'status' => '1']
                );
                // 5th Transaction
                $account_id = getAcccountId('name', 'Discount', $company);
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $discount, 'created_at' => $this->date, 'status' => '1']
                );
            }
            // Update Voucher No in Purchase Register
            DB::table('purchaseregister')
                ->where('id', $purchaseregister_id)
                ->update(['voucher_no' => $voucher_no,'unique_no' => 'PV'.$purchaseregister_id]);

            /*.................................
            Account Manage - Receipt Voucher
            ...................................*/
            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Payment Voucher' and company_id=$company and status='1'")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }
            $voucher_no = 'PA' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;
            // 6th Transaction
            $account_id = getVendorAcccountId($vendor, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Payment Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $final_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 7th Transaction
            $account_id = getAcccountId('name', 'Cash', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$purchaseregister_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Payment Voucher', 'account_id' => '10', 'account_type' => 'CR', 'credit' => $final_total, 'created_at' => $this->date, 'status' => '1']
            );

            /*///////////////////Account Mange-End///////////////////*/

            if (count($item_id) > 0) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $barcode1 = $barcode[$i];
                    $expiry_date1 = $expiry_date[$i];
                    if (!empty($expiry_date1)) {
                        $expiry_date1 = date_format(date_create($expiry_date1), "Y-m-d");
                    }
                    $quantity1 = $quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    $taxP1 = $taxP[$i];
                    $taxA1 = $taxA[$i];
                    if ($item_id1 != '') {

                        if (empty($taxP1)) {
                            $taxP1 = 0;
                        }

                        DB::table('purchaseregister_item')->insert(['parent_id' => $purchaseregister_id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'rate' => $rate1, 'amount' => $amount1, 'tax_rate' => $taxP1, 'tax_amount' => $taxA1, 'barcode' => $barcode1, 'expiry_date' => $expiry_date1]);

                    }
                }
            }
            return $purchaseregister_id;
        });
        return $result;
    }
    public function purchaseregister_edit($id)
    {
        return DB::table('purchaseregister')
            ->select('purchaseregister.*', 'vendor.name as vendor_name')
            ->where('purchaseregister.id', $id)
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->get();
    }
    public function purchaseregister_view($id)
    {
        return DB::table('purchaseregister')
            ->select('purchaseregister.*', 'vendor.name as vendor_name', 'vendor.mobile as vendor_mobile', 'company.name as company_name', 'users.address as address')
            ->where('purchaseregister.id', $id)
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->leftJoin('company', 'purchaseregister.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->get();
    }
    public function purchaseregister_item($id)
    {
        return DB::table('purchaseregister')
            ->select('purchaseregister_item.*', 'item.name as item_name', 'item.batch_no as batch_no', 'item.strength', 'unit.id as unit_id', 'unit.name as unit_name', 'brand.name as brand_name', 'brand.id as brand_id', 'form.id as form_id', 'form.name as form_name')
            ->where([
                ['purchaseregister.id', $id],
                ['purchaseregister.status', '1'],
            ])
            ->leftJoin('purchaseregister_item', 'purchaseregister_item.parent_id', '=', 'purchaseregister.id')
            ->leftJoin('item', 'purchaseregister_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->get();
    }
    public function purchaseregister_updatebyitem($id, $company, $entry_date, $bill_no, $bill_date, $vendor, $item, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $rate, $amount, $taxP, $taxA, $discount, $tax_percentage)
    {
        $result = DB::transaction(function () use ($id, $company, $entry_date, $bill_no, $bill_date, $vendor, $item, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $rate, $amount, $taxP, $taxA, $discount, $tax_percentage) {
            $user_id = Auth::id();
            if (!empty($entry_date)) {
                $entry_date = date_format(date_create($entry_date), "Y-m-d");
            }
            if (!empty($bill_date)) {
                $bill_date = date_format(date_create($bill_date), "Y-m-d");
            }
            $sub_total = collect($amount)->sum();

            $total = $sub_total - $discount;
            $tax = collect($taxA)->sum();
            $final_total = $sub_total - $discount + $tax;

            $purchaseregister_data = DB::table('purchaseregister')->where('id', $id)->first();
            $voucher_no = $purchaseregister_data->voucher_no;

            DB::table('purchaseregister')
                ->where('id', $id)
                ->update(['company_id' => $company, 'entry_date' => $entry_date, 'bill_no' => $bill_no, 'bill_date' => $bill_date, 'vendor_id' => $vendor, 'sub_total' => $sub_total, 'discount' => $discount, 'tax' => $tax, 'total' => $final_total, 'remarks' => $remarks, 'updated_at' => $this->date, 'updated_by' => $user_id]);

            DB::table('purchaseregister_item')
                ->where('parent_id', $id)
                ->delete();

            DB::table('account_txn')
                ->where([
                    ['unique_no', 'PV'.$id],
                    ['company_id', $company],
                ])
                ->delete();

            /*///////////////////Account Mange-Start///////////////////*/

            /*............................
            Account Manage-Purchase Voucher
            ..............................*/

            // 1st Transaction
            $account_id = getAcccountId('name', 'Purchase', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 2nd Transaction
            if($tax > 0){
                $account_id = getAcccountId('name', 'Tax', $company);
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $tax, 'created_at' => $this->date, 'status' => '1']
                );
            }
            // 3rd Transaction
            $account_id = getVendorAcccountId($vendor, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $sub_total+$tax, 'created_at' => $this->date, 'status' => '1']
            );
            // 4th & 5th Transaction
            if($discount > 0){
                // 4th Transaction
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $discount, 'created_at' => $this->date, 'status' => '1']
                );
                // 5th Transaction
                $account_id = getAcccountId('name', 'Discount', $company);
                DB::table('account_txn')->insert(
                    ['company_id' => $company, 'unique_no' => 'PV'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $discount, 'created_at' => $this->date, 'status' => '1']
                );
            }

            // Update Voucher No in Purchase Register
            DB::table('purchaseregister')
                ->where('id', $id)
                ->update(['voucher_no' => $voucher_no,'unique_no' => 'PV'.$id]);

            /*.................................
            Account Manage - Receipt Voucher
            ...................................*/
            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Payment Voucher' and company_id=$company")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }
            $voucher_no = 'PA' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;
            // 6th Transaction
            $account_id = getVendorAcccountId($vendor, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Payment Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $final_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 7th Transaction
            $account_id = getAcccountId('name', 'Cash', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PV'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Payment Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $final_total, 'created_at' => $this->date, 'status' => '1']
            );

            /*///////////////////Account Mange-End///////////////////*/

            if (count($item_id) > 0) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $barcode1 = $barcode[$i];
                    $expiry_date1 = $expiry_date[$i];
                    if (!empty($expiry_date1)) {
                        $expiry_date1 = date_format(date_create($expiry_date1), "Y-m-d");
                    }
                    $quantity1 = $quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    $taxP1 = $taxP[$i];
                    $taxA1 = $taxA[$i];
                    if ($item_id1 != '') {

                        if (empty($taxP1)) {
                            $taxP1 = 0;
                        }

                        DB::table('purchaseregister_item')->insert(['parent_id' => $id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'rate' => $rate1, 'amount' => $amount1, 'tax_rate' => $taxP1, 'tax_amount' => $taxA1, 'barcode' => $barcode1, 'expiry_date' => $expiry_date1]);
                    }
                }
            }
            return true;
        });

        return $result;
    }
    public function purchaseregister_delete($id)
    {
        $result = DB::transaction(function () use ($id) {
            $user_id = Auth::id();
            $company = Auth::user()->company_id;
            $purchaseregister_data = DB::table('purchaseregister')->where('id', $id)->first();
            $voucher_no = $purchaseregister_data->voucher_no;

            DB::table('purchaseregister')
                ->where('id', $id)
                ->update(['status' => '0', 'updated_at' => $this->date, 'updated_by' => $user_id]);

            DB::table('account_txn')
                ->where([
                    ['unique_no', 'PV'.$id],
                    ['company_id', $company],
                ])
                ->update(['status' => '0', 'updated_at' => $this->date]);

            return true;
        });
        return $result;
    }
    public function ajax($company_id)
    {
        if (!empty($company_id)) {
            $category = DB::table('category')->where([
                ['status', '1'],
                ['company_id', $company_id],
            ])->get();
            $category_count = $category->count();
            if ($category_count > 0) {
                ?><option value="">Select</option><?php
                foreach ($category as $value) {?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
            } else {
                ?><option value="">Select</option><?php
            }
        } else {
            ?><option value="">Select</option><?php
        }
    }
    public function ajax2($category_id)
    {
        if (!empty($category_id)) {
            $item = DB::table('item')->where([
                ['status', '1'],
                ['category_id', $category_id],
            ])->get();
            $item_count = $item->count();
            if ($item_count > 0) {
                ?><option value="">Select</option><?php
                foreach ($item as $value) {?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
            } else {
                ?><option value="">Select</option><?php
        }
        } else {
            ?><option value="">Select</option><?php
        }
    }
    public function additems($item_id)
    {
        $items = DB::table('item')
            ->select('item.*', 'unit.name as unit_name', 'form.name as form_name', 'brand.name as brand_name')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->where('item.id', $item_id)
            ->get();
        print_r(json_encode(array($items)));
    }
    public function additemsbypo($purchaseorder_no)
    {
        $company_id = Auth::user()->company_id;
        $items = DB::table('purchaseorder')
            ->select('purchaseorder_item.*', 'item.name as item_name', 'item.batch_no as batch_no', 'item.strength', 'unit.name as unit_name', 'form.id as form_id', 'form.name as form_name')
            ->leftJoin('purchaseorder_item', 'purchaseorder_item.parent_id', '=', 'purchaseorder.id')
            ->leftJoin('item', 'purchaseorder_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->where([
                ['purchaseorder.purchaseorder_no', $purchaseorder_no],
                ['purchaseorder.company_id', $company_id],
                ['purchaseorder.status', '1'],
            ])
            ->get();
        print_r(json_encode(array($items)));
    }
}
