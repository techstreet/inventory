<?php
use Carbon\Carbon;
if (! function_exists('getOpeningStock')) {
    function getOpeningStock($item_id,$barcode=NULL,$expiry_date=NULL){
		$param1 = array();
		$param2 = array();
		$param3 = array(
		    'item_id' => $item_id,
		    'status' => '1'
		);
		if(!empty($barcode)){
			$param1 = array(
			    'barcode' => $barcode,
			);
		}
		if(!empty($expiry_date)){
			$param2 = array(
			    'expiry_date' => $expiry_date,
			);
		}
		$search_param = array_merge($param1,$param2,$param3);
		
		$data = DB::table('openingstock')
			->select(DB::raw("SUM(quantity) as quantity"))
			->where($search_param)
            ->first();
                 
        $quantity = $data->quantity;
		if(empty($quantity)){
			$quantity = 0;
		}
		return $quantity;
	}
}
if (! function_exists('getOpeningStockByDate')) {
    function getOpeningStockByDate($item_id,$date){
		
		$data = DB::table('openingstock')
			->select(DB::raw("SUM(quantity) as quantity"))
			->where([
				['item_id',$item_id],
				['on_date','<=',$date],
				['status','1']
			])
			->first();
		if($data){
			$quantity = $data->quantity;
		}
		else{
			$quantity = 0;
		}
		return $quantity;
	}
}
if (! function_exists('getPurchaseStock')) {
    function getPurchaseStock($item_id,$barcode=NULL,$expiry_date=NULL){
		$param1 = array();
		$param2 = array();
		$param3 = array(
		    'purchaseregister_item.item_id' => $item_id,
		    'purchaseregister.status' => '1'
		);
		if(!empty($barcode)){
			$param1 = array(
			    'purchaseregister_item.barcode' => $barcode,
			);
		}
		if(!empty($expiry_date)){
			$param2 = array(
			    'purchaseregister_item.expiry_date' => $expiry_date,
			);
		}
		$search_param = array_merge($param1,$param2,$param3);
		$data = DB::table('purchaseregister')
			->select(DB::raw("SUM(purchaseregister_item.quantity) as quantity"))
			->leftJoin('purchaseregister_item','purchaseregister_item.parent_id','=','purchaseregister.id')
			->where($search_param)
            ->first();    
        $quantity = $data->quantity;
        if(empty($quantity)){
			$quantity = 0;
		}
		return $quantity;
	}
}
if (! function_exists('getPurchaseStockByDate')) {
    function getPurchaseStockByDate($item_id,$date){
		$data = DB::table('purchaseregister')
			->select(DB::raw("SUM(purchaseregister_item.quantity) as quantity"))
			->leftJoin('purchaseregister_item','purchaseregister_item.parent_id','=','purchaseregister.id')
			->where([
				['purchaseregister_item.item_id',$item_id],
				['purchaseregister.entry_date','<=',$date],
				['purchaseregister.status','1']
			])
			->first();
		if($data){
			$quantity = $data->quantity;
		}
		else{
			$quantity = 0;
		}
		return $quantity;
	}
}
if (! function_exists('getPurchaseReturnStock')) {
    function getPurchaseReturnStock($item_id,$barcode=NULL,$expiry_date=NULL){
		$param1 = array();
		$param2 = array();
		$param3 = array(
		    'purchasereturn_item.item_id' => $item_id,
		    'purchasereturn.status' => '1'
		);
		if(!empty($barcode)){
			$param1 = array(
			    'purchasereturn_item.barcode' => $barcode,
			);
		}
		if(!empty($expiry_date)){
			$param2 = array(
			    'purchasereturn_item.expiry_date' => $expiry_date,
			);
		}
		$search_param = array_merge($param1,$param2,$param3);
		$data = DB::table('purchasereturn')
			->select(DB::raw("SUM(purchasereturn_item.return_quantity) as quantity"))
			->leftJoin('purchasereturn_item','purchasereturn_item.parent_id','=','purchasereturn.id')
			->where($search_param)
            ->first();
                 
        $quantity = $data->quantity;
		if(empty($quantity)){
			$quantity = 0;
		}
		return $quantity;
	}
}
if (! function_exists('getPurchaseReturnStockByDate')) {
    function getPurchaseReturnStockByDate($item_id,$date){
		$data = DB::table('purchasereturn')
			->select(DB::raw("SUM(purchasereturn_item.return_quantity) as quantity"))
			->leftJoin('purchasereturn_item','purchasereturn_item.parent_id','=','purchasereturn.id')
			->where([
				['purchasereturn_item.item_id',$item_id],
				['purchasereturn.debitnote_date','<=',$date],
				['purchasereturn.status','1']
			])
            ->first();
                 
		if($data){
			$quantity = $data->quantity;
		}
		else{
			$quantity = 0;
		}
		return $quantity;
	}
}
if (! function_exists('getSaleStock')) {
    function getSaleStock($item_id,$barcode=NULL,$expiry_date=NULL,$invoice_no=NULL,$company_id=NULL){
		$param1 = array();
		$param2 = array();
		$param3 = array(
		    'saleregister_item.item_id' => $item_id,
		    'saleregister.status' => '1',
		    'saleregister.invoice_no' => $invoice_no,
		    'saleregister.company_id' => $company_id,	
		    );
		if(!empty($barcode)){
			$param1 = array(
			    'saleregister_item.barcode' => $barcode,
			);
		}
		if(!empty($expiry_date)){
			$param2 = array(
			    'saleregister_item.expiry_date' => $expiry_date,
			);
		}
		$search_param = array_merge($param1,$param2,$param3);
		$data = DB::table('saleregister')
			->select(DB::raw("SUM(saleregister_item.quantity) as quantity"))
			->leftJoin('saleregister_item','saleregister_item.parent_id','=','saleregister.id')
			->where($search_param)
            ->first();
                 
        $quantity = $data->quantity;
		if(empty($quantity)){
			$quantity = 0;
		}
		return $quantity;
	}
}


if (! function_exists('getTotalSales')) {
    function getTotalSales($item_id,$barcode=NULL,$expiry_date=NULL){
		$param1 = array();
		$param2 = array();
		$param3 = array(
		    'saleregister_item.item_id' => $item_id,
		    'saleregister.status' => '1',	
		    );
		if(!empty($barcode)){
			$param1 = array(
			    'saleregister_item.barcode' => $barcode,
			);
		}
		if(!empty($expiry_date)){
			$param2 = array(
			    'saleregister_item.expiry_date' => $expiry_date,
			);
		}
		$search_param = array_merge($param1,$param2,$param3);
		$data = DB::table('saleregister')
			->select(DB::raw("SUM(saleregister_item.quantity) as quantity"))
			->leftJoin('saleregister_item','saleregister_item.parent_id','=','saleregister.id')
			->where($search_param)
            ->first();
                 
        $quantity = $data->quantity;
		if(empty($quantity)){
			$quantity = 0;
		}
		return $quantity;
	}
}

if (! function_exists('getTotalSalesByDate')) {
    function getTotalSalesByDate($item_id,$date){
		$data = DB::table('saleregister')
			->select(DB::raw("SUM(saleregister_item.quantity) as quantity"))
			->leftJoin('saleregister_item','saleregister_item.parent_id','=','saleregister.id')
			->where([
				['saleregister_item.item_id',$item_id],
				['saleregister.entry_date','<=',$date],
				['saleregister.status','1']
			])
            ->first();
                 
		if($data){
			$quantity = $data->quantity;
		}
		else{
			$quantity = 0;
		}
		return $quantity;
	}
}


if (! function_exists('getSaleReturnStock')) {
    function getSaleReturnStock($item_id,$barcode=NULL,$expiry_date=NULL,$invoice_no=NULL,$company_id=NULL){
		$param1 = array();
		$param2 = array();
		$param3 = array(
		    'salereturn_item.item_id' => $item_id,
		    'salereturn.status' => '1',
		    'salereturn.invoice_no' => $invoice_no,
		    'salereturn.company_id' => $company_id,
		);
		if(!empty($barcode)){
			$param1 = array(
			    'salereturn_item.barcode' => $barcode,
			);
		}
		if(!empty($expiry_date)){
			$param2 = array(
			    'salereturn_item.expiry_date' => $expiry_date,
			);
		}
		$search_param = array_merge($param1,$param2,$param3);
		$data = DB::table('salereturn')
			->select(DB::raw("SUM(salereturn_item.return_quantity) as quantity"))
			->leftJoin('salereturn_item','salereturn_item.parent_id','=','salereturn.id')
			->where($search_param)
            ->first();
                 
        $quantity = $data->quantity;
		if(empty($quantity)){
			$quantity = 0;
		}
		return $quantity;
	}
}



if (! function_exists('getTotalSaleReturnStock')) {
    function getTotalSaleReturnStock($item_id,$barcode=NULL,$expiry_date=NULL){
		$param1 = array();
		$param2 = array();
		$param3 = array(
		    'salereturn_item.item_id' => $item_id,
		    'salereturn.status' => '1',
		);
		if(!empty($barcode)){
			$param1 = array(
			    'salereturn_item.barcode' => $barcode,
			);
		}
		if(!empty($expiry_date)){
			$param2 = array(
			    'salereturn_item.expiry_date' => $expiry_date,
			);
		}
		$search_param = array_merge($param1,$param2,$param3);
		$data = DB::table('salereturn')
			->select(DB::raw("SUM(salereturn_item.return_quantity) as quantity"))
			->leftJoin('salereturn_item','salereturn_item.parent_id','=','salereturn.id')
			->where($search_param)
            ->first();
                 
        $quantity = $data->quantity;
		if(empty($quantity)){
			$quantity = 0;
		}
		return $quantity;
	}
}

if (! function_exists('getTotalSaleReturnStockByDate')) {
    function getTotalSaleReturnStockByDate($item_id,$date){
		$data = DB::table('salereturn')
			->select(DB::raw("SUM(salereturn_item.return_quantity) as quantity"))
			->leftJoin('salereturn_item','salereturn_item.parent_id','=','salereturn.id')
			->where([
				['salereturn_item.item_id',$item_id],
				['salereturn.creditnote_date','<=',$date],
				['salereturn.status','1']
			])
            ->first();
                 
		if($data){
			$quantity = $data->quantity;
		}
		else{
			$quantity = 0;
		}
		return $quantity;
	}
}


if (! function_exists('getStock')) {
    function getStock($item_id,$barcode=NULL,$expiry_date=NULL){
		$opening_stock = getOpeningStock($item_id,$barcode=NULL,$expiry_date=NULL);
		$purchase = getPurchaseStock($item_id,$barcode=NULL,$expiry_date=NULL);
		$purchase_return = getPurchaseReturnStock($item_id,$barcode=NULL,$expiry_date=NULL);
		$sale = getTotalSales($item_id,$barcode=NULL,$expiry_date=NULL);
		$sale_return = getTotalSaleReturnStock($item_id,$barcode=NULL,$expiry_date=NULL);
		$stock = $opening_stock+$purchase+$sale_return-$purchase_return-$sale;
		return $stock;
	}
}
if (! function_exists('low_stock')) {
    function low_stock($company=NULL){
		if($company>0){
			$data = DB::table('item')
			->select('item.*','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
			->where('item.status','1')
			->leftJoin('category','category.id','=','item.category_id')
			->leftJoin('brand','brand.id','=','item.brand_id')
			->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
			->leftJoin('form','form.id','=','item.form_id')
			->where('item.company_id',$company)
            ->get();
		}
		else{
			$data = DB::table('item')
			->select('item.*','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
			->where('item.status','1')
			->leftJoin('category','category.id','=','item.category_id')
			->leftJoin('brand','brand.id','=','item.brand_id')
			->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
			->leftJoin('form','form.id','=','item.form_id')
            ->get();
		}
            
        $low_stock = array();
        $count = $data->count();
        if($count>0){
			foreach($data as $key=>$value){
				$in_stock = getStock($value->id);
				
	            $data[$key]->in_stock = $in_stock;
				if($value->notify_quantity >= $in_stock){
					$low_stock[] = $data[$key];
				}
			}
		}
		//dd($low_stock);
		return $low_stock;
	}
}
if (! function_exists('getPrice')) {
    function getPrice($item_id){
		$item = DB::table('item')
			->select('price')
			->where([
			['id',$item_id],
			['status','1']
			])
			->first();
			return $item->price;
	}
}
if (! function_exists('getUserName')) {
    function getUserName($user_id){
		$data = DB::table('users')
			->select('name')
			->where('id',$user_id)
            ->get();
        return $data[0]->name;
	}
}
if (! function_exists('record_exists')) {
    function record_exists($input,$column,$table,$company = NULL,$id = NULL){
		if(empty($company)){
			$record = DB::table($table)
			->where([
			['status','1'],
			['id','!=',$id],
			[$column,$input]
			])
            ->get();
		}
		else{
			$record = DB::table($table)
			->where([
			['status','1'],
			['id','!=',$id],
			[$column,$input],
			['company_id',$company]
			])
            ->get();
		}
        $count = $record->count();
        if($count>0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
if (! function_exists('post_api')) {
    function post_api($data){
		$data_string = json_encode($data);
		$ch = curl_init(config('app.apiurl'));                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                             
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
}
if (! function_exists('date_dfy')) {
    function date_dfy($date){
		if($date == '' || $date == '0000-00-00' || $date == '0000-00-00 00:00:00'){
			return '';
		}
		else{
			return date_format(date_create($date),"d-F-Y");
		}
	}
}
if (! function_exists('date_ymd')) {
    function date_ymd($date){
		if($date == '' || $date == '0000-00-00' || $date == '0000-00-00 00:00:00'){
			return '';
		}
		else{
			return date_format(date_create($date),"Y-m-d");
		}
	}
}
if (! function_exists('zero_empty')) {
    function zero_empty($data){
		if($data == '0'){
			return '';
		}
		else{
			return $data;
		}
	}
}
if (! function_exists('empty_zero')) {
    function empty_zero($data){
		if($data == ''){
			return 0;
		}
		else{
			return $data;
		}
	}
}
if (! function_exists('slash_decimal')) {
    function slash_decimal($data){
		$intdiscount = intval($data);
		$floatdiscount = floatval($data);
		if($intdiscount == $floatdiscount){
			return $intdiscount;
		}
		else{
			return $floatdiscount;
		}
	}
}
if (! function_exists('age')) {
    function age($dob){
    	return date_diff(date_create($dob), date_create('today'))->y;
	}
}
if (! function_exists('replace_blank')) {
    function replace_blank($data){
		if($data == ''){
			return 0;
		}
		else{
			return $data;
		}
	}
}
if (! function_exists('replace_blank_negative')) {
    function replace_blank_negative($data){
		if($data < 0 || $data == ''){
			return 0;
		}
		else{
			return $data;
		}
	}
}
if (! function_exists('openingstock_total')) {
    function openingstock_total($company=NULL){
		if(!empty($company)){
			$data = DB::table('openingstock')
			->select(DB::raw("SUM(amount) as amount"))
			->where([
				['status','1'],
				['company_id',$company]
				])
			->first();
			return $data->amount;
		}
		else{
			$data = DB::table('openingstock')
			->select(DB::raw("SUM(amount) as amount"))
			->where([
				['status','1'],
				])
			->first();
			return $data->amount;
		}
	}
}
if (! function_exists('purchase_total')) {
    function purchase_total($company=NULL){
		if(!empty($company)){
			$data = DB::table('purchaseregister')
			->select(DB::raw("SUM(total) as amount"),DB::raw("SUM(tax) as tax"))
			->where([
				['status','1'],
				['company_id',$company]
				])
			->first();
			return $data->amount-$data->tax;
		}
		else{
			$data = DB::table('purchaseregister')
			->select(DB::raw("SUM(total) as amount"),DB::raw("SUM(tax) as tax"))
			->where([
				['status','1'],
				])
			->first();
			return $data->amount-$data->tax;
		}
	}
}
if (! function_exists('purchasereturn_total')) {
    function purchasereturn_total($company=NULL){
		if(!empty($company)){
			$data = DB::table('purchasereturn')
			->select(DB::raw("SUM(total) as amount"),DB::raw("SUM(tax) as tax"))
			->where([
				['status','1'],
				['company_id',$company]
				])
			->first();
			return $data->amount-$data->tax;
		}
		else{
			$data = DB::table('purchasereturn')
			->select(DB::raw("SUM(total) as amount"),DB::raw("SUM(tax) as tax"))
			->where([
				['status','1'],
				])
			->first();
			return $data->amount-$data->tax;
		}
	}
}
if (! function_exists('sale_total')) {
    function sale_total($company=NULL){
		if(!empty($company)){
			$data = DB::table('saleregister')
			->select(DB::raw("SUM(total) as amount"),DB::raw("SUM(tax) as tax"))
			->where([
				['status','1'],
				['company_id',$company]
				])
			->first();
			return $data->amount-$data->tax;
		}
		else{
			$data = DB::table('saleregister')
			->select(DB::raw("SUM(total) as amount"),DB::raw("SUM(tax) as tax"))
			->where([
				['status','1'],
				])
			->first();
			return $data->amount-$data->tax;
		}
	}
}
if (! function_exists('salereturn_total')) {
    function salereturn_total($company=NULL){
		if(!empty($company)){
			$data = DB::table('salereturn')
			->select(DB::raw("SUM(total) as amount"),DB::raw("SUM(tax) as tax"))
			->where([
				['status','1'],
				['company_id',$company]
				])
			->first();
			return $data->amount-$data->tax;
		}
		else{
			$data = DB::table('salereturn')
			->select(DB::raw("SUM(total) as amount"),DB::raw("SUM(tax) as tax"))
			->where([
				['status','1'],
				])
			->first();
			return $data->amount-$data->tax;
		}
	}
}
if (! function_exists('is_decimal')) {
	function is_decimal( $value )
	{
		if ( strpos( $value, "." ) !== false ) {
			return true;
		}
		return false;
	}
}
if (! function_exists('getBalance')) {
    function getBalance($client_id,$company=NULL,$from_date=NULL,$to_date=NULL)
	{
		if(!empty($from_date) && !empty($to_date)){
			$from_date = Carbon::parse($from_date)->startOfDay();
			$to_date = Carbon::parse($to_date)->endOfDay();
		}

		if(!empty($company)){
			if(!empty($from_date) && !empty($from_date)){
				$res = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
					['client_id',$client_id],
					['company_id',$company],
					['status','1'],
					])
				->whereBetween('created_at', array($from_date, $to_date))
				->first();
			}
			else{
				$res = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
					['client_id',$client_id],
					['company_id',$company],
					['status','1'],
					])
				->first();
			}
		}
		else{
			if(!empty($from_date) && !empty($from_date)){
				$res = DB::table('client_register')
    			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
    			->where([
    				['client_id',$client_id],
    				['status','1'],
					])
				->whereBetween('created_at', array($from_date, $to_date))
    			->first();
			}
			else{
				$res = DB::table('client_register')
    			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
    			->where([
    				['client_id',$client_id],
    				['status','1'],
    				])
    			->first();
			}
		}
		if($res){
			$balance = $res->debit-$res->credit;
			if(is_decimal($balance)){
				return number_format($balance,2,'.','');
			}
			else{
				return $balance;
			}
		}
		else{
			return 0;
		}
	}
}

if (! function_exists('getCash')) {
    function getCash($client_id)
	{
		$cash_received = DB::table('client_register')
			->select(DB::raw("SUM(credit) as credit"))
			->where([
				['client_id',$client_id],
				['type','2'],
				['status','1'],
				])
			->first();

		$cash_returned = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"))
			->where([
				['client_id',$client_id],
				['type','4'],
				['status','1'],
				])
			->first();

		return $cash_received->credit - $cash_returned->debit;

	}
}
if (! function_exists('getCashReport')) {
    function getCashReport($client_id,$cash_from,$cash_to)
	{
		$cash_received = DB::table('client_register')
			->select(DB::raw("SUM(credit) as credit"))
			->where([
				['client_id',$client_id],
				['type','2'],
				['status','1'],
				])
			->whereBetween('created_at', array($cash_from, $cash_to))
			->first();

		$cash_returned = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"))
			->where([
				['client_id',$client_id],
				['type','4'],
				['status','1'],
				])
			->whereBetween('created_at', array($cash_from, $cash_to))
			->first();

		$balance = $cash_received->credit - $cash_returned->debit;
		if(is_decimal($balance)){
			return number_format($balance,2,'.','');
		}
		else{
			return $balance;
		}

	}
}
if (! function_exists('getDebtReport')) {
    function getDebtReport($client_id,$debt_from,$debt_to)
	{
		$res = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
			->where([
				['client_id',$client_id],
				['status','1'],
				])
			->whereBetween('created_at', array($debt_from, $debt_to))
			->first();
		if($res){
			return $res->debit-$res->credit;
		}
		else{
			return 0;
		}
	}
}
if (! function_exists('getSaleTypeId')) {
    function getSaleTypeId($invoice_no,$record_type,$company_id)
	{
		if($record_type == 1){
			$table_name = 'salereturn';
		}
		else{
			$table_name = 'saleregister';
		}
		$res = DB::table($table_name)
			->where([
				['invoice_no',$invoice_no],
				['company_id',$company_id],
				['status','1'],
				])
			->first();
		if($res){
		    return $res->id;
		}
		else{
		    $res = DB::table($table_name)
			->where([
				['invoice_no',$invoice_no],
				['company_id',$company_id],
				])
			->first();
			return $res->id;
		}
	}
}
if (! function_exists('getActualStock')) {
    function getActualStock($item_id,$date)
	{
		$res = DB::table('actualstock')
			->where([
				['item_id',$item_id],
				['entry_date',date_ymd($date)],
				['status','1'],
				])
			->first();
		if($res){
			return $res->actual_stock;
		}
		else{
			return '0';
		}
	}
}
if (! function_exists('getAcccountId')) {
    function getAcccountId($field_name,$field_value,$company)
	{
		$account = DB::table('account')
			->select('id')
			->where([
				[$field_name,$field_value],
				['company_id',$company],
				['status','1']
				])
			->first();
		return $account->id;
	}
}
if (! function_exists('getVendorAcccountId')) {
	function getVendorAcccountId($vendor_id,$company)
	{
		$vendor = DB::table('vendor')
			->select('account_id')
			->where([
				['id', $vendor_id],
				['company_id',$company],
				['status','1']
				])
			->first();
		return $vendor->account_id;
	}
}
if (! function_exists('getClientAcccountId')) {
	function getClientAcccountId($client_id,$company)
	{
		$client = DB::table('client')
			->select('account_id')
			->where([
				['id', $client_id],
				['company_id',$company],
				['status','1']
				])
			->first();
		return $client->account_id;
	}
}
?>