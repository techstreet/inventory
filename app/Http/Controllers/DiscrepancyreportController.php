<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Discrepancyreport;
use App\Form;
use App\Http\Controllers\Controller;
use App\Manufacturer;
use Illuminate\Http\Request;

class DiscrepancyreportController extends Controller
{
    public function __construct()
    {
        $this->discrepancyreport = new Discrepancyreport();
        $this->category = new Category();
        $this->form = new Form();
        $this->manufacturer = new Manufacturer();
        $this->brand = new Brand();
    }
    public function index()
    {
        $category = $this->category->category_list();
        $form = $this->form->form_list();
        $manufacturer = $this->manufacturer->manufacturer_list();
        $brand = $this->brand->brand_list();

        return view('discrepancyreport/list', ['category' => $category, 'form' => $form, 'manufacturer' => $manufacturer, 'brand' => $brand]);
    }
    public function search(Request $request)
    {
        $category_selected = $request->input('category');
        $form_selected = $request->input('form');
        $manufacturer_selected = $request->input('manufacturer');
        $brand_selected = $request->input('brand');
        $date = date_ymd($request->input('current_date'));

        $discrepancyreport = $this->discrepancyreport->discrepancyreport_list($category_selected, $form_selected, $manufacturer_selected, $brand_selected, $date);
        $count = $discrepancyreport->count();

        $category = $this->category->category_list();
        $form = $this->form->form_list();
        $manufacturer = $this->manufacturer->manufacturer_list();
        $brand = $this->brand->brand_list();

        return view('discrepancyreport/list', ['discrepancyreport' => $discrepancyreport, 'category' => $category, 'category_selected' => $category_selected, 'form' => $form, 'form_selected' => $form_selected, 'manufacturer' => $manufacturer, 'manufacturer_selected' => $manufacturer_selected, 'brand' => $brand, 'brand_selected' => $brand_selected, 'count' => $count]);
    }
}
