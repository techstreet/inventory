<?php

namespace App\Http\Controllers;

use App\Financialgroup;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class FinancialgroupController extends Controller
{
    public function __construct()
    {
        $this->financialgroup = new Financialgroup();
    }
    public function index()
    {
        $financialgroup = $this->financialgroup->financialgroup_list();
        $count = $financialgroup->count();
        return view('financial-group/list', ['financialgroup' => $financialgroup, 'count' => $count]);
    }
    public function add()
    {
        $parent = $this->financialgroup->financialgroup_list();
        return view('financial-group/add', ['parent' => $parent]);
    }
    public function save(Request $request)
    {
        $parent = $request->input('parent');
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $type = $request->input('type');
        $this->validate($request, [
            'name' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'financial_group', $company);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->financialgroup->financialgroup_add($parent, $company, $name, $type);
            if ($result) {
                $request->session()->flash('success', 'Record added successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        $financialgroup = $this->financialgroup->financialgroup_edit($id);
        $parent = $this->financialgroup->financialgroup_list();
        return view('financial-group/edit', ['financialgroup' => $financialgroup, 'parent' => $parent]);
    }
    public function update(Request $request, $id)
    {
        $parent = $request->input('parent');
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $type = $request->input('type');
        $this->validate($request, [
            'name' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'financial_group', $company, $id);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->financialgroup->financialgroup_update($id, $parent, $company, $name, $type);
            if ($result) {
                $request->session()->flash('success', 'Record updated successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->financialgroup->financialgroup_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
