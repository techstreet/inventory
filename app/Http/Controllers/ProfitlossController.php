<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Controllers\Controller;
use App\Item;
use App\Profitloss;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProfitlossController extends Controller
{
    public function __construct()
    {
        $this->profitloss = new Profitloss();
        $this->account = new Account();
        $this->item = new Item();
    }
    public function index()
    {
        return view('profit-loss/list');
    }
    public function search(Request $request)
    {
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $from = Carbon::parse($from_date)->startOfDay();
        $to = Carbon::parse($to_date)->endOfDay();

        $from_only_date = Carbon::parse($from_date)->toDateString();
        $to_only_date = Carbon::parse($to_date)->toDateString();

        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date' => 'required|date',
        ]);

        $item = $this->item->item_list();

        $total_opening_stock = 0;
        $total_purchase = 0;
        $total_sale = 0;
        $total_closing_stock = 0;

        foreach ($item as $key => $value) {
            $item_id = $value->id;
            // Opening Stock
            $opening_stock_data = $this->profitloss->openingStock($item_id);
            $opening_stock = $opening_stock_data->amount;
            $opening_stock_qty = $opening_stock_data->quantity;
            // Purchase
            $purchase_data = $this->profitloss->purchase($from_only_date, $to_only_date, $item_id);
            $purchase = $purchase_data->amount - $purchase_data->tax;
            $purchase_qty = $purchase_data->quantity;
            // Purchase Return
            $purchase_return_data = $this->profitloss->purchaseReturn($from_only_date, $to_only_date, $item_id);
            $purchase_return = $purchase_return_data->amount;
            $purchase_return_qty = $purchase_return_data->quantity;
            // Actual Purchase
            $actual_purchase = $purchase - $purchase_return;
            $actual_purchase_qty = $purchase_qty - $purchase_return_qty;
            // Sale
            $sale_data = $this->profitloss->sale($from_only_date, $to_only_date, $item_id);
            $sale = $sale_data->amount;
            $sale_qty = $sale_data->quantity;
            // Sale Return
            $sale_return_data = $this->profitloss->saleReturn($from_only_date, $to_only_date, $item_id);
            $sale_return = $sale_return_data->amount;
            $sale_return_qty = $sale_return_data->quantity;
            // Actual Sale
            $actual_sale = $sale - $sale_return;
            $actual_sale_qty = $sale_qty - $sale_return_qty;

            $average_cost = 0;
            if ($opening_stock > 0 || $actual_purchase > 0) {
                $average_cost = ($opening_stock + $actual_purchase) / ($opening_stock_qty + $actual_purchase_qty);
            }
            $closing_stock = ($opening_stock_qty + $actual_purchase_qty - $actual_sale_qty) * $average_cost;

            // Total
            $total_opening_stock = $total_opening_stock + $opening_stock;
            $total_purchase = $total_purchase + $actual_purchase;
            $total_sale = $total_sale + $actual_sale;
            $total_closing_stock = $total_closing_stock + $closing_stock;

            // echo $value->name."<br/>";
            // echo "Opening Stock: ".$opening_stock."<br/>";
            // echo "Opening Stock Quantity: ".$opening_stock_qty."<br/>";
            // echo "Purchase: ".$actual_purchase."<br/>";
            // echo "Purchase Quantity: ".$actual_purchase_qty."<br/>";
            // echo "Average Cost: ".$average_cost."<br/>";
            // echo "Sale: ".$actual_sale."<br/>";
            // echo "Sale Quantity: ".$actual_sale_qty."<br/>";
            // echo "Closing Stock: ".$closing_stock."<br/>";
            // echo "------------------------------------------"."<br/>";
        }

        $income_group = $this->profitloss->accountGroup('Income');
        $income_account = $this->profitloss->getAccounts($income_group);
        $income_account_count = $income_account->count();

        $expense_group = $this->profitloss->accountGroup('Expense');
        $expense_account = $this->profitloss->getAccounts($expense_group);
        $expense_account_count = $expense_account->count();

        $total_income = 0;
        $total_expense = 0;

        if ($income_account_count > 0) {
            foreach ($income_account as $key => $value) {
                $account_id = $value->id;
                $income = $this->profitloss->income($from, $to, $value->id);
                $total_income = $total_income + $income;
            }
        }

        if ($expense_account_count > 0) {
            foreach ($expense_account as $key => $value) {
                $account_id = $value->id;
                $expense = $this->profitloss->expense($from, $to, $value->id);
                $total_expense = $total_expense + $expense;
            }
        }

        $balance = ($total_opening_stock + $total_purchase + $total_expense) - ($total_sale + $total_closing_stock + $total_income);

        $gross_profit = 0;
        $gross_loss = 0;

        if ($balance > 0) {
            $gross_loss = $balance;
        }
        if ($balance < 0) {
            $gross_profit = $balance * -1;
        }

        return view('profit-loss/list', ['opening_stock' => $total_opening_stock, 'closing_stock' => $total_closing_stock, 'actual_purchase' => $total_purchase, 'actual_sale' => $total_sale, 'gross_profit' => $gross_profit, 'gross_loss' => $gross_loss, 'income' => $total_income, 'expense' => $total_expense]);
    }
}
