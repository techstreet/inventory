<?php

namespace App\Http\Controllers;

use App\Balancesheet;
use App\Account;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BalancesheetController extends Controller
{
    public function __construct()
    {
        $this->balancesheet = new Balancesheet();
        $this->account = new Account();
    }
    public function index()
    {
        return view('balance-sheet/list');
    }
    public function search(Request $request)
    {
        $to_date = $request->input('to_date');
        $to = Carbon::parse($to_date)->endOfDay();

        $this->validate($request, [
            'to_date' => 'required|date',
        ]);

        $company_id = Auth::user()->company_id;
        $accounts = $this->account->account_list();
        $liabilities = $this->balancesheet->GroupBalance('Liabilities',$to);
        $assets = $this->balancesheet->GroupBalance('Assets',$to);

        $liabilities_count = $liabilities->count();
        $assets_count = $assets->count();
        if($liabilities_count > $assets_count){
            $count = $liabilities_count;
        }
        else{
            $count = $assets_count;
        }

        return view('balance-sheet/list', ['accounts' => $accounts,'liabilities' => $liabilities,'assets' => $assets,'company_id' => $company_id,'to' => $to,'count' => $count]);
    }
}
