<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Controllers\Controller;
use App\Welcome;
use Auth;
use DB;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function __construct()
    {
        $this->dashboard = new Welcome();
        $this->company = new Company();
    }
    public function index()
    {
        $user = Auth::user();
        $user_id = $user->id;
        $user_name = $user->name;
        $user_group = $user->user_group;
        $company = $this->company->company_list();

        if ($user_group == '1') {
            $company_id = $company[0]->id;
        } else {
            $company_id = $user->company_id;
        }
        $sale = $this->dashboard->sale();
        $salereturn = $this->dashboard->salereturn();
        $cash = $this->dashboard->cash();
        $purchase = $this->dashboard->purchase();
        $purchasereturn = $this->dashboard->purchasereturn();
        $credit = $this->dashboard->credit();
        $credit_today = $this->dashboard->credit_today();
        $credit_thisweek = $this->dashboard->credit_thisweek();

        $sale_today = $this->dashboard->sale_today($company_id, '2');
        $sale_thisweek = $this->dashboard->sale_thisweek($company_id, '2');
        $sale_thismonth = $this->dashboard->sale_thismonth($company_id, '2');

        $salereturn_today = $this->dashboard->salereturn_today($company_id, '2');
        $salereturn_thisweek = $this->dashboard->salereturn_thisweek($company_id, '2');
        $salereturn_thismonth = $this->dashboard->salereturn_thismonth($company_id, '2');

        $cash_today = $this->dashboard->cash_today($company_id, '2');
        $cash_thisweek = $this->dashboard->cash_thisweek($company_id, '2');
        $cash_thismonth = $this->dashboard->cash_thismonth($company_id, '2');

        $purchase_today = $this->dashboard->purchase_today($company_id, '2');
        $purchase_thisweek = $this->dashboard->purchase_thisweek($company_id, '2');
        $purchase_thismonth = $this->dashboard->purchase_thismonth($company_id, '2');

        $purchasereturn_today = $this->dashboard->purchasereturn_today($company_id, '2');
        $purchasereturn_thisweek = $this->dashboard->purchasereturn_thisweek($company_id, '2');
        $purchasereturn_thismonth = $this->dashboard->purchasereturn_thismonth($company_id, '2');

        $credit_030 = $this->dashboard->credit_030($company_id, '2');
        $credit_3060 = $this->dashboard->credit_3060($company_id, '2');
        $credit_6090 = $this->dashboard->credit_6090($company_id, '2');
        $credit_90120 = $this->dashboard->credit_90120($company_id, '2');


        $remaining_stock = $this->dashboard->remaining_stock($company_id, '2');

        $latest_client = $this->dashboard->latest_client();
        $latestclient_count = $latest_client->count();

        $latest_vendors = $this->dashboard->latest_vendors();
        $latestvendors_count = $latest_vendors->count();

        return view('welcome', ['user_id' => $user_id, 'user_name' => $user_name, 'company' => $company, 'sale' => $sale, 'sale_today' => $sale_today, 'sale_thisweek' => $sale_thisweek, 'sale_thismonth' => $sale_thismonth, 'salereturn' => $salereturn, 'salereturn_today' => $salereturn_today, 'salereturn_thisweek' => $salereturn_thisweek, 'salereturn_thismonth' => $salereturn_thismonth, 'cash' => $cash, 'cash_today' => $cash_today, 'cash_thisweek' => $cash_thisweek, 'cash_thismonth' => $cash_thismonth, 'purchase' => $purchase, 'purchase_today' => $purchase_today, 'purchase_thisweek' => $purchase_thisweek, 'purchase_thismonth' => $purchase_thismonth, 'purchasereturn' => $purchasereturn, 'purchasereturn_today' => $purchasereturn_today, 'purchasereturn_thisweek' => $purchasereturn_thisweek, 'purchasereturn_thismonth' => $purchasereturn_thismonth, 'credit' => $credit, 'credit_today' => $credit_today, 'credit_thisweek' => $credit_thisweek, 'credit_030' => $credit_030, 'credit_3060' => $credit_3060, 'credit_6090' => $credit_6090, 'credit_90120' => $credit_90120, 'latest_client' => $latest_client, 'latestclient_count' => $latestclient_count, 'latest_vendors' => $latest_vendors, 'latestvendors_count' => $latestvendors_count, 'remaining_stock' => $remaining_stock]);
    }
    public function filter(Request $request)
    {
        $company_id = $request->input('company_id');

        $company = DB::table('company')
            ->where('id', $company_id)
            ->first();

        $company_name = $company->name;

        $sale_today = $this->dashboard->sale_today($company_id, '2');
        $sale_thisweek = $this->dashboard->sale_thisweek($company_id, '2');
        $sale_thismonth = $this->dashboard->sale_thismonth($company_id, '2');

        $salereturn_today = $this->dashboard->salereturn_today($company_id, '2');
        $salereturn_thisweek = $this->dashboard->salereturn_thisweek($company_id, '2');
        $salereturn_thismonth = $this->dashboard->salereturn_thismonth($company_id, '2');

        $cash_today = $this->dashboard->cash_today($company_id, '2');
        $cash_thisweek = $this->dashboard->cash_thisweek($company_id, '2');
        $cash_thismonth = $this->dashboard->cash_thismonth($company_id, '2');

        $purchase_today = $this->dashboard->purchase_today($company_id, '2');
        $purchase_thisweek = $this->dashboard->purchase_thisweek($company_id, '2');
        $purchase_thismonth = $this->dashboard->purchase_thismonth($company_id, '2');

        $purchasereturn_today = $this->dashboard->purchasereturn_today($company_id, '2');
        $purchasereturn_thisweek = $this->dashboard->purchasereturn_thisweek($company_id, '2');
        $purchasereturn_thismonth = $this->dashboard->purchasereturn_thismonth($company_id, '2');

        $credit_030 = $this->dashboard->credit_030($company_id, '2');
        $credit_3060 = $this->dashboard->credit_3060($company_id, '2');
        $credit_6090 = $this->dashboard->credit_6090($company_id, '2');
        $credit_90120 = $this->dashboard->credit_90120($company_id, '2');

        $remaining_stock = $this->dashboard->remaining_stock($company_id, '2');

        $data = array();

        $data['company_name'] = $company_name;

        $data['sale_today'] = replace_blank($sale_today);
        $data['sale_thisweek'] = replace_blank($sale_thisweek);
        $data['sale_thismonth'] = replace_blank($sale_thismonth);

        $data['salereturn_today'] = replace_blank($salereturn_today);
        $data['salereturn_thisweek'] = replace_blank($salereturn_thisweek);
        $data['salereturn_thismonth'] = replace_blank($salereturn_thismonth);

        $data['cash_today'] = replace_blank($cash_today);
        $data['cash_thisweek'] = replace_blank($cash_thisweek);
        $data['cash_thismonth'] = replace_blank($cash_thismonth);

        $data['purchase_today'] = replace_blank($purchase_today);
        $data['purchase_thisweek'] = replace_blank($purchase_thisweek);
        $data['purchase_thismonth'] = replace_blank($purchase_thismonth);

        $data['purchasereturn_today'] = replace_blank($purchasereturn_today);
        $data['purchasereturn_thisweek'] = replace_blank($purchasereturn_thisweek);
        $data['purchasereturn_thismonth'] = replace_blank($purchasereturn_thismonth);

        $data['credit_030'] = replace_blank_negative($credit_030);
        $data['credit_3060'] = replace_blank_negative($credit_3060);
        $data['credit_6090'] = replace_blank_negative($credit_6090);
        $data['credit_90120'] = replace_blank_negative($credit_90120);

        $data['remaining_stock'] = replace_blank(number_format($remaining_stock, 2, '.', ''));

        print_r(json_encode(array($data)));
    }
}
