<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->brand = new Brand();
    }
    public function index()
    {
        $brand = $this->brand->brand_list();
        $count = $brand->count();
        return view('brand/list', ['brand' => $brand, 'count' => $count]);
    }
    public function add()
    {
        return view('brand/add');
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $status = $request->input('status');
        $this->validate($request, [
            'name' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'brand', $company);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->brand->brand_add($company, $name, $status);
            if ($result) {
                $request->session()->flash('success', 'Record added successfully!');
            } else {
                $request->session()->flash('error', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        $brand = $this->brand->brand_edit($id);
        return view('brand/edit', ['brand' => $brand]);
    }
    public function update(Request $request, $id)
    {
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $status = $request->input('status');
        $this->validate($request, [
            'name' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'brand', $company, $id);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->brand->brand_update($id, $company, $name, $status);
            if ($result) {
                $request->session()->flash('success', 'Record updated successfully!');
            } else {
                $request->session()->flash('error', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->brand->brand_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('error', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
