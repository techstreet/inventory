<?php

namespace App\Http\Controllers;

use App\Category;
use App\Client;
use App\Http\Controllers\Controller;
use App\Salereturn;
use Auth;
use DB;
use Illuminate\Http\Request;

class SalereturnController extends Controller
{
    public function __construct()
    {
        $this->salereturn = new Salereturn();
        $this->category = new Category();
        $this->client = new Client();
    }
    public function index()
    {
        $salereturn = $this->salereturn->salereturn_list();
        $count = $salereturn->count();
        return view('salereturn/list', ['salereturn' => $salereturn, 'count' => $count]);
    }
    public function add()
    {
        $company_id = Auth::user()->company_id;
        $category = $this->category->category_list();
        $client = $this->client->client_list();
        $salereturn = DB::table('salereturn')->where('company_id', $company_id)->orderBy('id', 'desc')->first();
        $voucher_no = 0;
        if (!empty($salereturn)) {
            $last_voucher_no = $salereturn->creditnote_no;
            $arr = explode('_', $last_voucher_no);
            $voucher_no = $arr[1];
        }
        return view('salereturn/add', ['category' => $category, 'client' => $client, 'voucher_no' => $voucher_no]);
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $creditnote_no = $request->input('creditnote_no');
        $creditnote_date = $request->input('creditnote_date');
        $invoice_no = $request->input('invoice_no');
        $return_reason = $request->input('return_reason');
        $remarks = $request->input('remarks');

        $item_id = $request->input('item_id');
        $unit_id = $request->input('unit_id');
        $quantity = $request->input('quantity');
        $return_quantity = $request->input('return_quantity');
        $rate = $request->input('rate');
        $amount = $request->input('amount');
        $return_amount = $request->input('return_amount');

        $this->validate($request, [
            //'creditnote_no'=>'required',
            'creditnote_date' => 'required|date',
            'invoice_no' => 'required',
        ]);

        $result = $this->salereturn->salereturn_add($company, $creditnote_no, $creditnote_date, $invoice_no, $return_reason, $remarks, $item_id, $unit_id, $quantity, $return_quantity, $rate, $amount, $return_amount);
        if ($result) {
            $request->session()->flash('success', 'Record added successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }

    public function edit($id)
    {
        $salereturn = $this->salereturn->salereturn_edit($id);
        $salereturn_item = $this->salereturn->salereturn_item($id);
        $category = $this->category->category_list();
        $client = $this->client->client_list();
        return view('salereturn/edit', ['salereturn' => $salereturn, 'salereturn_item' => $salereturn_item, 'category' => $category, 'client' => $client]);
    }
    public function view($id)
    {
        $salereturn = $this->salereturn->salereturn_view($id);
        $salereturn_item = $this->salereturn->salereturn_item($id);
        return view('salereturn/view', ['salereturn' => $salereturn, 'salereturn_item' => $salereturn_item]);
    }
    public function update(Request $request, $id)
    {
        $company = Auth::user()->company_id;
        //$creditnote_no = $request->input('creditnote_no');
        $creditnote_date = $request->input('creditnote_date');
        $invoice_no = $request->input('invoice_no');
        $return_reason = $request->input('return_reason');
        $remarks = $request->input('remarks');

        $item_id = $request->input('item_id');
        $unit_id = $request->input('unit_id');
        $quantity = $request->input('quantity');
        $return_quantity = $request->input('return_quantity');
        $rate = $request->input('rate');
        $amount = $request->input('amount');
        $return_amount = $request->input('return_amount');

        $this->validate($request, [
            //'creditnote_no'=>'required',
            'creditnote_date' => 'required|date',
        ]);

        $result = $this->salereturn->salereturn_update($id, $company, $creditnote_date, $invoice_no, $return_reason, $remarks, $item_id, $unit_id, $quantity, $return_quantity, $rate, $amount, $return_amount);
        if ($result) {
            $request->session()->flash('success', 'Record updated successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->salereturn->salereturn_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function ajax(Request $request)
    {
        $company_id = $request->input('company_id');
        $this->salereturn->ajax($company_id);
    }
    public function ajax2(Request $request)
    {
        $category_id = $request->input('category_id');
        $this->salereturn->ajax2($category_id);
    }
    public function additems(Request $request)
    {
        $invoice_no = $request->input('invoice_no');
        $this->salereturn->additems($invoice_no);
    }
    public function autocomplete(Request $request)
    {
        $company_id = Auth::user()->company_id;
        $query = $request->get('query', '');
        $invoice = DB::table('saleregister')
            ->select('invoice_no as name')
            ->where([
                ['status', '1'],
                ['company_id', $company_id],
                ['invoice_no', 'LIKE', '%' . $query . '%'],
            ])
            ->get();
        return response()->json($invoice);
    }
}
