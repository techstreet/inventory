<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function index()
    {
        // Client
        // $client = DB::table('client')
        //     ->where([
        //     ['status','1'],
        //     ])
        //     ->get();
        // foreach($client as $value){
        //     $company_id = $value->company_id;
        //     $name = $value->name;
        //     $contact_name = $value->contact_name;
        //     $phone = $value->phone;
        //     $mobile = $value->mobile;
        //     $email = $value->email;
        //     $address = $value->address;
        //     $client_id = $value->id;

        //     // $account = DB::table('account')
        //     // ->where([
        //     // ['status','1'],
        //     // ['client_id',$client_id],
        //     // ])
        //     // ->first();

        //     // if(!$account){
        //     //     DB::table('account')->insert(
        //     //         ['company_id' => $company_id, 'account_group_id' => '1','name' => $name,'contact_name' => $contact_name,'type' => 'DR','phone' => $phone,'mobile' => $mobile,'email' => $email,'address' => $address,'client_id' => $client_id,'created_at' => $this->date]
        //     //     );
        //     // }
        //     $account_id = DB::table('account')->insertGetId(
        //         ['company_id' => $company_id, 'account_group_id' => '1','name' => $name,'type' => 'DR','phone' => $phone,'mobile' => $mobile,'email' => $email,'address' => $address,'created_at' => $this->date]
        //     );
        //     DB::table('client')
        //         ->where('id', $client_id)
        //         ->update(['account_id' => $account_id]);
        // }
        // Vendor
        // $vendor = DB::table('vendor')
        //     ->where([
        //     ['status','1'],
        //     ])
        //     ->get();
        // foreach($vendor as $value){
        //     $company_id = $value->company_id;
        //     $name = $value->name;
        //     $contact_name = $value->contact_name;
        //     $phone = $value->phone;
        //     $mobile = $value->mobile;
        //     $email = $value->email;
        //     $address = $value->address;
        //     $vendor_id = $value->id;

        //     $account_id = DB::table('account')->insertGetId(
        //         ['company_id' => $company_id, 'account_group_id' => '2', 'name' => $name, 'type' => 'CR','phone' => $phone,'mobile' => $mobile,'email' => $email,'address' => $address,'created_at' => $this->date]
        //     );
        //     DB::table('vendor')
        //     ->where('id', $vendor_id)
        //     ->update(['account_id' => $account_id]);
        // }
        echo "success";
    }
    public function autocomplete(Request $request)
    {
        $query = $request->get('query', '');
        $voucher = DB::table('purchaseregister')
            ->select('purchasevoucher_no as name')
            ->where([
                ['status', '1'],
                ['purchasevoucher_no', 'LIKE', '%' . $query . '%'],
            ])
            ->get();
        return response()->json($voucher);
    }
}
