<?php

namespace App\Http\Controllers;

use App\Debtreport;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DebtreportController extends Controller
{
    public function __construct()
    {
        $this->debtreport = new Debtreport();
    }
    public function index()
    {
        return view('debtreport/list');
    }
    public function search(Request $request)
    {
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $debt_from = Carbon::parse($from_date)->startOfDay();
        $debt_to = Carbon::parse($to_date)->endOfDay();

        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date' => 'required|date',
        ]);

        $company_id = Auth::user()->company_id;

        $debtreport = $this->debtreport->debtreport_list($debt_from, $debt_to);
        $count = $debtreport->count();
        return view('debtreport/list', ['company_id' => $company_id, 'debtreport' => $debtreport, 'count' => $count]);
    }
}
