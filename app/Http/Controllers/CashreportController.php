<?php

namespace App\Http\Controllers;

use App\Cashreport;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CashreportController extends Controller
{
    public function __construct()
    {
        $this->cashreport = new cashreport();
    }
    public function index()
    {
        return view('cashreport/list');
    }
    public function search(Request $request)
    {
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $cash_from = Carbon::parse($from_date)->startOfDay();
        $cash_to = Carbon::parse($to_date)->endOfDay();

        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date' => 'required|date',
        ]);

        $cashreport = $this->cashreport->cashreport_list($cash_from, $cash_to);
        $count = $cashreport->count();
        return view('cashreport/list', ['cashreport' => $cashreport, 'count' => $count, 'cash_from' => $cash_from, 'cash_to' => $cash_to]);
    }
}
