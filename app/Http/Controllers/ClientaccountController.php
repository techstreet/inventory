<?php

namespace App\Http\Controllers;

use App\Clientaccount;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ClientaccountController extends Controller
{
    public function __construct()
    {
        $this->clientaccount = new Clientaccount();
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function detail($id, Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');

        if (!empty($from) && !empty($to)) {
            $from = Carbon::parse($request->input('from'))->startOfDay();
            $to = Carbon::parse($request->input('to'))->endOfDay();
        }

        $account_id = $this->clientaccount->get_accountId($id);
        $clientaccount = $this->clientaccount->clientaccount_detail($from, $to, $account_id);

        $count = count($clientaccount);
        $client_name = $this->clientaccount->get_clientname($id);
        return view('clientaccount/detail', ['clientaccount' => $clientaccount, 'count' => $count, 'client_name' => $client_name]);
    }
    public function paymentAdd($id)
    {
        $company = Auth::user()->company_id;
        $account_id = $this->clientaccount->get_accountId($id);
        $outstanding_balance = $this->clientaccount->getBalance($account_id);
        $client_name = $this->clientaccount->get_clientname($id);
        $account_id = $this->clientaccount->get_accountId($id);

        $max_voucher = DB::select(
            DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Receipt Voucher' and company_id=$company")
        );
        if (count($max_voucher) > 0) {
            $max_counter = $max_voucher[0]->max_no + 1;
        }

        $voucher_no = 'RE' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;

        return view('clientaccount/receive', ['client_name' => $client_name, 'outstanding_balance' => $outstanding_balance,'voucher_no' => $voucher_no,'account_id' => $account_id]);
    }
    public function paymentSave(Request $request, $id)
    {
        $amount = $request->input('amount');
        $payment_date = $request->input('date');
        $particular = $request->input('remarks');
        $this->validate($request, [
            'amount' => 'required',
            'date' => 'required|date',
            'remarks' => 'required',
        ]);
        $result = $this->clientaccount->payment_add($id, $amount, $payment_date, $particular);
        if ($result) {
            $request->session()->flash('success', 'Payment received successfully!');
        } else {
            $request->session()->flash('error', 'Something went wrong!');
        }
        return redirect()->back();
    }

    public function delete(Request $request, $id)
    {
        $data = DB::table('client_register')
            ->where([
                ['id', $id],
            ])
            ->first();

        if ($data) {
            $invoice_no = $data->invoice_no;
            if (empty($invoice_no)) {
                $user_id = Auth::id();
                DB::table('client_register')
                    ->where('id', $id)
                    ->update(['status' => '0', 'updated_at' => $this->date, 'updated_by' => $user_id]);
            }
            $request->session()->flash('success', 'Record deleted successfully!');
            return redirect()->back();
        }

    }
}
