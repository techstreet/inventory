<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->client = new Client();
    }
    public function index()
    {
        $company_id = Auth::user()->company_id;
        $client = $this->client->client_list();
//        echo "<pre>";
        //        print_r($client);
        $count = $client->count();
        return view('client/list', ['company_id' => $company_id, 'client' => $client, 'count' => $count]);
    }
    public function add()
    {
        return view('client/add');
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $contact_name = $request->input('contact_name');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $mobile = $request->input('mobile');
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'mobile' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'client', $company);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->client->client_add($company, $name, $contact_name, $email, $address, $phone, $mobile);
            if ($result) {
                $request->session()->flash('success', 'Record added successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        $client = $this->client->client_edit($id);
        return view('client/edit', ['client' => $client]);
    }
    public function update(Request $request, $id)
    {
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $contact_name = $request->input('contact_name');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $mobile = $request->input('mobile');
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'mobile' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'client', $company, $id);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->client->client_update($id, $company, $name, $contact_name, $email, $address, $phone, $mobile);
            if ($result) {
                $request->session()->flash('success', 'Record updated successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->client->client_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
