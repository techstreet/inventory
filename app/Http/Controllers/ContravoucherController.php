<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Controllers\Controller;
use App\Contravoucher;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ContravoucherController extends Controller
{
    public function __construct()
    {
        $this->contravoucher = new Contravoucher();
        $this->account = new Account();
        $this->date = Carbon::now();
    }
    public function index()
    {
        $contravoucher = $this->contravoucher->contravoucher_list();
        $count = $contravoucher->count();
        return view('contra-voucher/list', ['contravoucher' => $contravoucher, 'count' => $count]);
    }
    public function add()
    {
        $account = $this->account->account_list();
        $company = Auth::user()->company_id;
        $max_voucher = DB::select(
            DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Contra Voucher' and company_id=$company")
        );
        if (count($max_voucher) > 0) {
            $max_counter = $max_voucher[0]->max_no + 1;
        }

        $voucher_no = 'CO' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;
        return view('contra-voucher/add', ['account' => $account, 'voucher_no' => $voucher_no]);
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $account_from = $request->input('account_from');
        $account_to = $request->input('account_to');
        $amount = $request->input('amount');
        $voucher_date = $request->input('voucher_date');
        $remarks = $request->input('remarks');
        $this->validate($request, [
            'account_from' => 'required',
            'account_to' => 'required',
            'amount' => 'required',
        ]);
        $result = $this->contravoucher->contravoucher_add($company, $account_from, $account_to, $amount, $voucher_date, $remarks);
        if ($result) {
            $request->session()->flash('success', 'Record added successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function cancel(Request $request, $id)
    {
        $result = $this->contravoucher->contravoucher_cancel($id);
        if ($result) {
            $request->session()->flash('success', 'Record cancelled successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->contravoucher->contravoucher_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
