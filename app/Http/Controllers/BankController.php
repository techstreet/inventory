<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public function __construct()
    {
        $this->bank = new Bank();
    }
    public function index()
    {
        $bank = $this->bank->bank_list();
        $count = $bank->count();
        return view('bank/list', ['bank' => $bank, 'count' => $count]);
    }
    public function add()
    {
        return view('bank/add');
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $bank_name = $request->input('bank_name');
        $sort_code = $request->input('sort_code');
        $bank_address = $request->input('bank_address');
        $account_no = $request->input('account_no');
        $account_name = $request->input('account_name');
        $this->validate($request, [
            'bank_name' => 'required',
            'sort_code' => 'required',
            'bank_address' => 'required',
            'account_no' => 'required',
            'account_name' => 'required',
        ]);
        $result = $this->bank->bank_add($company, $bank_name, $sort_code, $bank_address, $account_no, $account_name);
        if ($result) {
            $request->session()->flash('success', 'Record added successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        $bank = $this->bank->bank_edit($id);
        return view('bank/edit', ['bank' => $bank]);
    }
    public function update(Request $request, $id)
    {
        $company = Auth::user()->company_id;
        $bank_name = $request->input('bank_name');
        $sort_code = $request->input('sort_code');
        $bank_address = $request->input('bank_address');
        $account_no = $request->input('account_no');
        $account_name = $request->input('account_name');
        $this->validate($request, [
            'bank_name' => 'required',
            'sort_code' => 'required',
            'bank_address' => 'required',
            'account_no' => 'required',
            'account_name' => 'required',
        ]);
        $result = $this->bank->bank_update($id, $company, $bank_name, $sort_code, $bank_address, $account_no, $account_name);
        if ($result) {
            $request->session()->flash('success', 'Record updated successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->bank->bank_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
