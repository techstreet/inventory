<?php

namespace App\Http\Controllers;

use App\Accountgroup;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class AccountgroupController extends Controller
{
    public function __construct()
    {
        $this->accountgroup = new Accountgroup();
    }
    public function index()
    {
        $accountgroup = $this->accountgroup->accountgroup_list();
        $count = $accountgroup->count();
        return view('account-group/list', ['accountgroup' => $accountgroup, 'count' => $count]);
    }
    public function add()
    {
        $parent = $this->accountgroup->accountgroup_list();
        return view('account-group/add', ['parent' => $parent]);
    }
    public function save(Request $request)
    {
        $parent = $request->input('parent');
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $type = $request->input('type');
        $this->validate($request, [
            'name' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'account_group', $company);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->accountgroup->accountgroup_add($parent, $company, $name, $type);
            if ($result) {
                $request->session()->flash('success', 'Record added successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        $accountgroup = $this->accountgroup->accountgroup_edit($id);
        $parent = $this->accountgroup->accountgroup_list();
        return view('account-group/edit', ['accountgroup' => $accountgroup, 'parent' => $parent]);
    }
    public function update(Request $request, $id)
    {
        $parent = $request->input('parent');
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $type = $request->input('type');
        $this->validate($request, [
            'name' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'account_group', $company, $id);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->accountgroup->accountgroup_update($id, $parent, $company, $name, $type);
            if ($result) {
                $request->session()->flash('success', 'Record updated successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->accountgroup->accountgroup_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
