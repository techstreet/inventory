<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Vendor;
use Auth;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->vendor = new Vendor();
    }
    public function index()
    {
        $vendor = $this->vendor->vendor_list();
        $count = $vendor->count();
        return view('vendor/list', ['vendor' => $vendor, 'count' => $count]);
    }
    public function add()
    {
        return view('vendor/add');
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $contact_name = $request->input('contact_name');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $mobile = $request->input('mobile');
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'mobile' => 'required',
        ];
        $customRules = [
            'name.required' => 'The vendor name field can not be blank.',
        ];
        $this->validate($request, $rules, $customRules);
        $record_exists = record_exists($name, 'name', 'vendor', $company);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->vendor->vendor_add($company, $name, $contact_name, $email, $address, $phone, $mobile);
            if ($result) {
                $request->session()->flash('success', 'Record added successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        $vendor = $this->vendor->vendor_edit($id);
        return view('vendor/edit', ['vendor' => $vendor]);
    }
    public function update(Request $request, $id)
    {
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $contact_name = $request->input('contact_name');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $mobile = $request->input('mobile');
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'mobile' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'vendor', $company, $id);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->vendor->vendor_update($id, $company, $name, $contact_name, $email, $address, $phone, $mobile);
            if ($result) {
                $request->session()->flash('success', 'Record updated successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->vendor->vendor_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
