<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Controllers\Controller;
use App\Journalvoucher;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class JournalvoucherController extends Controller
{
    public function __construct()
    {
        $this->journalvoucher = new Journalvoucher();
        $this->account = new Account();
        $this->date = Carbon::now();
    }
    public function index()
    {
        $journalvoucher = $this->journalvoucher->journalvoucher_list();
        $count = $journalvoucher->count();
        return view('journal-voucher/list', ['journalvoucher' => $journalvoucher, 'count' => $count]);
    }
    public function add()
    {
        $account = $this->account->account_list();
        $company = Auth::user()->company_id;
        $max_voucher = DB::select(
            DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Journal Voucher' and company_id=$company")
        );
        if (count($max_voucher) > 0) {
            $max_counter = $max_voucher[0]->max_no + 1;
        }

        $voucher_no = 'JO' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;
        return view('journal-voucher/add', ['account' => $account, 'voucher_no' => $voucher_no]);
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $voucher_date = $request->input('voucher_date');
        $account_id = $request->input('account_id');
        $debit = $request->input('debit');
        $credit = $request->input('credit');
        $amount = $request->input('total');
        $remarks = $request->input('remarks');
        $this->validate($request, [
            'voucher_date' => 'required|date',
            'amount' => 'required',
        ]);
        $result = $this->journalvoucher->journalvoucher_add($company, $voucher_date, $account_id, $debit, $credit, $amount, $remarks);
        if ($result) {
            $request->session()->flash('success', 'Record added successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function cancel(Request $request, $id)
    {
        $result = $this->journalvoucher->journalvoucher_cancel($id);
        if ($result) {
            $request->session()->flash('success', 'Record cancelled successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->journalvoucher->journalvoucher_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
