<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;

class NotificationController extends Controller
{
    public function __construct()
    {
    }
    public function index()
    {
        $company = Auth::user()->company_id;
        $notification = low_stock($company);
        $count = count($notification);
        return view('notification/list', ['notification' => $notification, 'count' => $count]);
    }
}
