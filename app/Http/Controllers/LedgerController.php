<?php

namespace App\Http\Controllers;

use App\Ledger;
use App\Account;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LedgerController extends Controller
{
    public function __construct()
    {
        $this->ledger = new Ledger();
        $this->account = new Account();
    }
    public function index()
    {
        $accounts = $this->account->account_list();
        $account_id = '';
        return view('ledger/list', ['accounts' => $accounts,'account_id' => $account_id]);
    }
    public function search(Request $request)
    {
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $from = Carbon::parse($from_date)->startOfDay();
        $to = Carbon::parse($to_date)->endOfDay();

        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date' => 'required|date',
        ]);

        $company_id = Auth::user()->company_id;
        $accounts = $this->account->account_list();
        $account_id = $request->input('account');

        $debit = $this->ledger->debit($from, $to, $account_id);
        $credit = $this->ledger->credit($from, $to, $account_id);

        $debit_sum = $this->ledger->debit_sum($from, $to, $account_id);
        $credit_sum = $this->ledger->credit_sum($from, $to, $account_id);

        // print_r($debit_sum);
        // die;

        $debit_count = count($debit);
        $credit_count = count($credit);
        if($debit_count > $credit_count){
            $count = $debit_count;
        }
        else{
            $count = $credit_count;
        }
        $current_date = Carbon::now();
        return view('ledger/list', ['accounts' => $accounts,'account_id' => $account_id,'current_date' => $current_date,'company_id' => $company_id, 'debit' => $debit, 'credit' => $credit, 'debit_sum' => $debit_sum, 'credit_sum' => $credit_sum, 'count' => $count]);
    }
}
