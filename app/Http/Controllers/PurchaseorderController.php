<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Http\Controllers\Controller;
use App\Purchaseorder;
use App\Vendor;
use Auth;
use DB;
use Illuminate\Http\Request;

class PurchaseorderController extends Controller
{
    public function __construct()
    {
        $this->purchaseorder = new Purchaseorder();
        $this->category = new Category();
        $this->brand = new Brand();
        $this->vendor = new Vendor();
    }
    public function index()
    {
        $purchaseorder = $this->purchaseorder->purchaseorder_list();
        $count = $purchaseorder->count();
        return view('purchaseorder/list', ['purchaseorder' => $purchaseorder, 'count' => $count]);
    }
    public function add()
    {
        $company_id = Auth::user()->company_id;
        $category = $this->category->category_list();
        $brand = $this->brand->brand_list();
        $vendor = $this->vendor->vendor_list();
        $purchaseorder = DB::table('purchaseorder')->where('company_id', $company_id)->orderBy('id', 'desc')->first();
        $po_no = 0;
        if (!empty($purchaseorder)) {
            $last_po_no = $purchaseorder->purchaseorder_no;
            $arr = explode('_', $last_po_no);
            $po_no = $arr[1];
        }
        return view('purchaseorder/add', ['category' => $category, 'brand' => $brand, 'vendor' => $vendor, 'po_no' => $po_no]);
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $entry_date = $request->input('entry_date');
        $vendor = $request->input('vendor');
        $category = $request->input('category');
        $item = $request->input('item');
        $remarks = $request->input('remarks');

        $item_id = $request->input('item_id');
        $unit_id = $request->input('unit_id');
        $quantity = $request->input('quantity');
        $rate = $request->input('rate');
        $amount = $request->input('amount');
        $po_no = $request->input('po_no');

        $this->validate($request, [
            'vendor' => 'required',
            'entry_date' => 'required|date',
        ]);

        if (count($item_id) == 0) {
            $request->session()->flash('error', 'No items added yet!');
            return redirect()->back();
        }
        $result = $this->purchaseorder->purchaseorder_add($company, $po_no, $entry_date, $vendor, $item, $remarks, $item_id, $unit_id, $quantity, $rate, $amount);
        if ($result) {
            $request->session()->flash('success', 'Record added successfully!');
        } else {
            $request->session()->flash('error', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        $purchaseorder = $this->purchaseorder->purchaseorder_edit($id);
        $purchaseorder_item = $this->purchaseorder->purchaseorder_item($id);
        $category = $this->category->category_list();
        $brand = $this->brand->brand_list();
        $vendor = $this->vendor->vendor_list();
        return view('purchaseorder/edit', ['purchaseorder' => $purchaseorder, 'purchaseorder_item' => $purchaseorder_item, 'category' => $category, 'brand' => $brand, 'vendor' => $vendor]);
    }
    public function view($id)
    {
        $purchaseorder = $this->purchaseorder->purchaseorder_view($id);
        $purchaseorder_item = $this->purchaseorder->purchaseorder_item($id);
        return view('purchaseorder/view', ['purchaseorder' => $purchaseorder, 'purchaseorder_item' => $purchaseorder_item]);
    }
    public function update(Request $request, $id)
    {
        $company = Auth::user()->company_id;
        $entry_date = $request->input('entry_date');
        $vendor = $request->input('vendor');
        $category = $request->input('category');
        $item = $request->input('item');
        $remarks = $request->input('remarks');

        $item_id = $request->input('item_id');
        $unit_id = $request->input('unit_id');
        $quantity = $request->input('quantity');
        $rate = $request->input('rate');
        $amount = $request->input('amount');

        $this->validate($request, [
            'vendor' => 'required',
            'entry_date' => 'required|date',
        ]);

        if (count($item_id) == 0) {
            $request->session()->flash('error', 'No items added yet!');
            return redirect()->back();
        }

        $result = $this->purchaseorder->purchaseorder_update($id, $company, $entry_date, $vendor, $item, $remarks, $item_id, $unit_id, $quantity, $rate, $amount);
        if ($result) {
            $request->session()->flash('success', 'Record updated successfully!');
        } else {
            $request->session()->flash('error', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->purchaseorder->purchaseorder_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('error', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function ajax(Request $request)
    {
        $company_id = $request->input('company_id');
        $this->purchaseorder->ajax($company_id);
    }
    public function ajax2(Request $request)
    {
        $category_id = $request->input('category_id');
        $this->purchaseorder->ajax2($category_id);
    }
    public function additems(Request $request)
    {
        $item_id = $request->input('item_id');
        $this->purchaseorder->additems($item_id);
    }
}
