<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Client;
use App\Custom;
use App\Http\Controllers\Controller;
use App\Saleregister;
use Auth;
use DB;
use Illuminate\Http\Request;

class SaleregisterController extends Controller
{
    public function __construct()
    {
        $this->saleregister = new Saleregister();
        $this->category = new Category();
        $this->brand = new Brand();
        $this->client = new Client();
        $this->custom = new Custom();
    }
    public function index()
    {
        $saleregister = $this->saleregister->saleregister_list();
        $count = $saleregister->count();
        return view('saleregister/list', ['saleregister' => $saleregister, 'count' => $count]);
    }
    public function add()
    {
        $company_id = Auth::user()->company_id;
        $category = $this->category->category_list();
        $brand = $this->brand->brand_list();
        $client = $this->client->client_list();
        $saleregister = DB::table('saleregister')->where('company_id', $company_id)->orderBy('invoice_no', 'desc')->first();
        $invoice_no = 0;
        if (!empty($saleregister)) {
            $last_invoice_no = $saleregister->invoice_no;
            $arr = explode('_', $last_invoice_no);
            $invoice_no = $arr[1];
        }
        $tax_percentage = $this->custom->overall_tax();
        return view('saleregister/add', ['category' => $category, 'brand' => $brand, 'client' => $client, 'invoice_no' => $invoice_no, 'tax_percentage' => $tax_percentage]);
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $entry_date = $request->input('entry_date');
        $client = $request->input('client');
        $sale_type = $request->input('sale_type');
        $category = $request->input('category');
        $item = $request->input('item');
        $ref = $request->input('ref');
        $remarks = $request->input('remarks');
        $discount = $request->input('discount');

        $item_id = $request->input('item_id');
        $unit_id = $request->input('unit_id');
        $quantity = $request->input('quantity');
        $rate = $request->input('rate');
        $amount = $request->input('amount');
        $barcode = $request->input('barcode');
        $expiry_date = $request->input('expiry_date');
        $invoice_no = $request->input('invoice_no');

        $this->validate($request, [
            'client' => 'required',
            'sale_type' => 'required',
            'entry_date' => 'required|date',
        ]);

        $tax_percentage = $this->custom->overall_tax();

        $result = $this->saleregister->saleregister_add($company, $invoice_no, $entry_date, $client, $sale_type, $item, $ref, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $rate, $amount, $discount, $tax_percentage);
        if ($result) {
            $request->session()->flash('success', 'Record added successfully!');
            return redirect()->action(
                'SaleregisterController@view', ['id' => $result]
            );
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
            return redirect()->back();
        }
    }
    public function edit($id)
    {
        $saleregister = $this->saleregister->saleregister_edit($id);
        $saleregister_item = $this->saleregister->saleregister_item($id);
        $category = $this->category->category_list();
        $brand = $this->brand->brand_list();
        $client = $this->client->client_list();
        $tax_percentage = $this->custom->overall_tax();
        return view('saleregister/edit', ['saleregister' => $saleregister, 'saleregister_item' => $saleregister_item, 'category' => $category, 'brand' => $brand, 'client' => $client, 'tax_percentage' => $tax_percentage]);
    }
    public function view($id)
    {
        $saleregister = $this->saleregister->saleregister_view($id);
        $saleregister_item = $this->saleregister->saleregister_item($id);
        return view('saleregister/view', ['saleregister' => $saleregister, 'saleregister_item' => $saleregister_item]);
    }
    public function update(Request $request, $id)
    {
        $company = Auth::user()->company_id;
        $entry_date = $request->input('entry_date');
        $client = $request->input('client');
        $sale_type = $request->input('sale_type');
        $category = $request->input('category');
        $item = $request->input('item');
        $ref = $request->input('ref');
        $remarks = $request->input('remarks');
        $discount = $request->input('discount');

        $item_id = $request->input('item_id');
        $unit_id = $request->input('unit_id');
        $quantity = $request->input('quantity');
        $rate = $request->input('rate');
        $amount = $request->input('amount');
        $barcode = $request->input('barcode');
        $expiry_date = $request->input('expiry_date');

        $this->validate($request, [
            'client' => 'required',
            'sale_type' => 'required',
            'entry_date' => 'required|date',
        ]);

        $tax_percentage = $this->custom->overall_tax();

        $result = $this->saleregister->saleregister_update($id, $company, $entry_date, $client, $sale_type, $item, $ref, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $rate, $amount, $discount, $tax_percentage);
        if ($result) {
            $request->session()->flash('success', 'Record updated successfully!');
            return redirect()->action(
                'SaleregisterController@view', ['id' => $id]
            );
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
            return redirect()->back();
        }
    }
    public function delete(Request $request, $id)
    {
        $result = $this->saleregister->saleregister_delete($id);
        if ($result == 'success') {
            $request->session()->flash('success', 'Record deleted successfully!');
        } elseif ($result == 'warning') {
            $request->session()->flash('warning', 'Please delete sales return of the invoice first!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function ajax(Request $request)
    {
        $company_id = $request->input('company_id');
        $this->saleregister->ajax($company_id);
    }
    public function ajax2(Request $request)
    {
        $category_id = $request->input('category_id');
        $this->saleregister->ajax2($category_id);
    }
    public function additems(Request $request)
    {
        $item_id = $request->input('item_id');
        //$this->saleregister->additems($item_id);
        $this->saleregister->itemsearch($item_id);
    }
    public function barcodesearch(Request $request)
    {
        $barcode = $request->input('barcode');
        $this->saleregister->barcodesearch($barcode);
    }
}
