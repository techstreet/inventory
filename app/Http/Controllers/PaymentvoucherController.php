<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Controllers\Controller;
use App\Paymentvoucher;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class PaymentvoucherController extends Controller
{
    public function __construct()
    {
        $this->paymentvoucher = new Paymentvoucher();
        $this->account = new Account();
        $this->date = Carbon::now();
    }
    public function index()
    {
        $paymentvoucher = $this->paymentvoucher->paymentvoucher_list();
        $count = $paymentvoucher->count();
        return view('payment-voucher/list', ['paymentvoucher' => $paymentvoucher, 'count' => $count]);
    }
    public function add()
    {
        $account = $this->account->account_list();
        $company = Auth::user()->company_id;
        $max_voucher = DB::select(
            DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Payment Voucher' and company_id=$company")
        );
        if (count($max_voucher) > 0) {
            $max_counter = $max_voucher[0]->max_no + 1;
        }

        $voucher_no = 'PA' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;
        return view('payment-voucher/add', ['account' => $account, 'voucher_no' => $voucher_no]);
    }
    public function save(Request $request)
    {
        $company = Auth::user()->company_id;
        $account_id = $request->input('account');
        $amount = $request->input('amount');
        $mode = $request->input('mode');
        $cheque_no = $request->input('cheque_no');
        $cheque_date = $request->input('cheque_date');
        $voucher_date = $request->input('voucher_date');
        $bank_name = $request->input('bank_name');
        $remarks = $request->input('remarks');
        $this->validate($request, [
            'account' => 'required',
            'amount' => 'required',
            'mode' => 'required',
        ]);
        $result = $this->paymentvoucher->paymentvoucher_add($company, $account_id, $amount, $mode, $cheque_no, $cheque_date, $voucher_date, $bank_name, $remarks);
        if ($result) {
            $request->session()->flash('success', 'Record added successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function cancel(Request $request, $id)
    {
        $result = $this->paymentvoucher->paymentvoucher_cancel($id);
        if ($result) {
            $request->session()->flash('success', 'Record cancelled successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->paymentvoucher->paymentvoucher_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
