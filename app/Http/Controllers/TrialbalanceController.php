<?php

namespace App\Http\Controllers;

use App\Trialbalance;
use App\Account;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrialbalanceController extends Controller
{
    public function __construct()
    {
        $this->trialbalance = new Trialbalance();
        $this->account = new Account();
    }
    public function index()
    {
        return view('trial-balance/list');
    }
    public function search(Request $request)
    {
        $to_date = $request->input('to_date');
        $to = Carbon::parse($to_date)->endOfDay();

        $this->validate($request, [
            'to_date' => 'required|date',
        ]);

        $company_id = Auth::user()->company_id;
        $accounts = $this->account->account_list();
        
        return view('trial-balance/list', ['accounts' => $accounts,'company_id' => $company_id,'to' => $to]);
    }
}
