<?php

namespace App\Http\Controllers;

use App\Account;
use App\Accountgroup;
use App\Financialgroup;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->account = new Account();
        $this->accountgroup = new Accountgroup();
        $this->financialgroup = new Financialgroup();
    }
    public function index()
    {
        $account = $this->account->account_list();
        $count = $account->count();
        return view('account/list', ['account' => $account, 'count' => $count]);
    }
    public function add()
    {
        $parent = $this->account->account_list();
        $account_group = $this->accountgroup->accountgroup_list();
        $financial_group = $this->financialgroup->financialgroup_list();
        return view('account/add', ['parent' => $parent, 'account_group' => $account_group, 'financial_group' => $financial_group]);
    }
    public function save(Request $request)
    {
        $parent = $request->input('parent');
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $type = $request->input('type');
        $account_group_id = $request->input('account_group_id');
        $financial_group_id = $request->input('financial_group_id');
        $opening_balance = $request->input('opening_balance');
        $restricted = $request->input('restricted');
        if (empty($restricted)) {
            $restricted = 0;
        }
        $gst_no = $request->input('gst_no');
        $taxid_no = $request->input('taxid_no');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $city = $request->input('city');
        $state = $request->input('state');
        $country = $request->input('country');
        $address = $request->input('address');
        $this->validate($request, [
            'name' => 'required',
            'account_group_id' => 'required',
            'financial_group_id' => 'required',
            'type' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'account', $company);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->account->account_add($parent, $company, $name, $type, $account_group_id, $financial_group_id, $opening_balance, $restricted, $gst_no, $taxid_no, $phone, $email, $city, $state, $country, $address);
            if ($result) {
                $request->session()->flash('success', 'Record added successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function edit($id)
    {
        $account = $this->account->account_edit($id);
        $parent = $this->account->account_list();
        $account_group = $this->accountgroup->accountgroup_list();
        $financial_group = $this->financialgroup->financialgroup_list();
        return view('account/edit', ['account' => $account, 'parent' => $parent, 'account_group' => $account_group, 'financial_group' => $financial_group]);
    }
    public function update(Request $request, $id)
    {
        $parent = $request->input('parent');
        $company = Auth::user()->company_id;
        $name = $request->input('name');
        $type = $request->input('type');
        $account_group_id = $request->input('account_group_id');
        $financial_group_id = $request->input('financial_group_id');
        $opening_balance = $request->input('opening_balance');
        $restricted = $request->input('restricted');
        if (empty($restricted)) {
            $restricted = 0;
        }
        $gst_no = $request->input('gst_no');
        $taxid_no = $request->input('taxid_no');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $city = $request->input('city');
        $state = $request->input('state');
        $country = $request->input('country');
        $address = $request->input('address');
        $this->validate($request, [
            'name' => 'required',
            'account_group_id' => 'required',
            'financial_group_id' => 'required',
            'type' => 'required',
        ]);
        $record_exists = record_exists($name, 'name', 'account', $company, $id);
        if ($record_exists) {
            $request->session()->flash('warning', 'Record already exists!');
        } else {
            $result = $this->account->account_update($id, $parent, $company, $name, $type, $account_group_id, $financial_group_id, $opening_balance, $restricted, $gst_no, $taxid_no, $phone, $email, $city, $state, $country, $address);
            if ($result) {
                $request->session()->flash('success', 'Record updated successfully!');
            } else {
                $request->session()->flash('failed', 'Something went wrong!');
            }
        }
        return redirect()->back();
    }
    public function delete(Request $request, $id)
    {
        $result = $this->account->account_delete($id);
        if ($result) {
            $request->session()->flash('success', 'Record deleted successfully!');
        } else {
            $request->session()->flash('failed', 'Something went wrong!');
        }
        return redirect()->back();
    }
}
