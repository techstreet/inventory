<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Purchasereturn extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function purchasereturn_list()
    {
        $company = Auth::user()->company_id;
        return DB::table('purchasereturn')
            ->select('purchasereturn.*', 'vendor.name as vendor_name')
            ->where([
                ['purchasereturn.status', '1'],
                ['purchasereturn.company_id', $company],
                ['purchaseregister.company_id', $company],
            ])
            ->leftJoin('purchaseregister', 'purchaseregister.purchasevoucher_no', '=', 'purchasereturn.purchasevoucher_no')
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->orderBy('purchasereturn.id', 'desc')
            ->get();
    }
    public function item_list($category_id)
    {
        return DB::table('item')->where('category_id', $category_id)->get();
    }
    public function category_list($id)
    {
        $item = DB::table('item')->where('id', $id)->get();
        foreach ($item as $value) {
            $company_id = $value->company_id;
        }
        $category = DB::table('category')->where('company_id', $company_id)->get();
        return $category;
    }
    public function category_id($item_id)
    {
        $item = DB::table('item')->where('id', $item_id)->get();
        foreach ($item as $value) {
            $category_id = $value->category_id;
        }
        return $category_id;
    }
    public function getPurchasereturnName($id)
    {
        $purchasereturn = DB::table('purchasereturn')->where('id', $id)->get();
        foreach ($purchasereturn as $value) {
            $name = $value->name;
        }
        return $name;
    }
    public function purchasereturn_add($company, $debitnote_no, $debitnote_date, $purchasevoucher_no, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $return_quantity, $rate, $amount, $return_amount)
    {
        $result = DB::transaction(function () use ($company, $debitnote_no, $debitnote_date, $purchasevoucher_no, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $return_quantity, $rate, $amount, $return_amount) {
            $user_id = Auth::id();
            if (!empty($debitnote_date)) {
                $debitnote_date = date_format(date_create($debitnote_date), "Y-m-d");
            }
            $sub_total = collect($return_amount)->sum();
            $purchasereturn_id = DB::table('purchasereturn')->insertGetId(
                ['company_id' => $company, 'debitnote_no' => $debitnote_no, 'debitnote_date' => $debitnote_date, 'purchasevoucher_no' => $purchasevoucher_no, 'remarks' => $remarks, 'sub_total' => $sub_total, 'total' => $sub_total, 'created_at' => $this->date, 'created_by' => $user_id]
            );

            $purchaseregister = DB::table('purchaseregister')
                ->where([
                    ['purchasevoucher_no', $purchasevoucher_no],
                    ['company_id', $company],
                ])
                ->first();
            $vendor = $purchaseregister->vendor_id;

            /*///////////////////Account Mange-Start///////////////////*/

            /*............................
            Account Manage-Purchase Return
            ..............................*/

            $max_counter = '1';
            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Purchase Return Voucher' and company_id=$company and status='1'")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }
            $voucher_no = 'PR' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;

            // 1st Transaction
            $account_id = getVendorAcccountId($vendor, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PR'.$purchasereturn_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Return Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 2nd Transaction
            $account_id = getAcccountId('name', 'Purchase Return', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PR'.$purchasereturn_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Return Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // Update Voucher No in Purchase Return
            DB::table('purchasereturn')
                ->where('id', $purchasereturn_id)
                ->update(['voucher_no' => $voucher_no,'unique_no' => 'PR'.$purchasereturn_id]);

            /*///////////////////Account Mange-End///////////////////*/

            if (count($item_id) > 0) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $barcode1 = $barcode[$i];
                    $expiry_date1 = $expiry_date[$i];
                    if (!empty($expiry_date1)) {
                        $expiry_date1 = date_format(date_create($expiry_date1), "Y-m-d");
                    }
                    $quantity1 = $quantity[$i];
                    $return_quantity1 = $return_quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    $return_amount1 = $return_amount[$i];
                    if ($item_id1 != '') {
                        DB::table('purchasereturn_item')->insert(['parent_id' => $purchasereturn_id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'return_quantity' => $return_quantity1, 'rate' => $rate1, 'amount' => $amount1, 'return_amount' => $return_amount1, 'barcode' => $barcode1, 'expiry_date' => $expiry_date1]);
                    }
                }
            }
            return true;
        });
        return $result;
    }
    public function purchasereturn_edit($id)
    {
        return DB::table('purchasereturn')
            ->select('purchasereturn.*', 'vendor.name as vendor_name')
            ->where('purchasereturn.id', $id)
            ->leftJoin('purchaseregister', 'purchaseregister.purchasevoucher_no', '=', 'purchasereturn.purchasevoucher_no')
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->get();
    }
    public function purchasereturn_view($id)
    {
        return DB::table('purchasereturn')
            ->select('purchasereturn.*', 'vendor.name as vendor_name', 'vendor.mobile as vendor_mobile', 'company.name as company_name', 'users.address as address')
            ->where('purchasereturn.id', $id)
            ->leftJoin('purchaseregister', 'purchaseregister.purchasevoucher_no', '=', 'purchasereturn.purchasevoucher_no')
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->leftJoin('company', 'purchaseregister.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->get();
    }
    public function purchasereturn_item($id)
    {
        return DB::table('purchasereturn')
            ->select('purchasereturn_item.*', 'item.name as item_name', 'item.batch_no as batch_no', 'item.strength', 'item.form_id', 'unit.name as unit_name', 'form.name as form_name')
            ->where([
                ['purchasereturn.id', $id],
                ['purchasereturn.status', '1'],
            ])
            ->leftJoin('purchasereturn_item', 'purchasereturn_item.parent_id', '=', 'purchasereturn.id')
            ->leftJoin('item', 'purchasereturn_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->get();
    }
    public function purchasereturn_update($id, $company, $debitnote_date, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $return_quantity, $rate, $amount, $return_amount)
    {
        $result = DB::transaction(function () use ($id, $company, $debitnote_date, $remarks, $item_id, $unit_id, $barcode, $expiry_date, $quantity, $return_quantity, $rate, $amount, $return_amount) {
            $user_id = Auth::id();
            if (!empty($debitnote_date)) {
                $debitnote_date = date_format(date_create($debitnote_date), "Y-m-d");
            }
            $sub_total = collect($return_amount)->sum();

            $purchasereturn = DB::table('purchasereturn')->where('id', $id)->first();
            $purchasevoucher_no = $purchasereturn->purchasevoucher_no;
            $voucher_no = $purchasereturn->voucher_no;

            $purchaseregister = DB::table('purchaseregister')
                ->where([
                    ['purchasevoucher_no', $purchasevoucher_no],
                    ['company_id', $company],
                ])
                ->first();

            $vendor = $purchaseregister->vendor_id;

            DB::table('purchasereturn')
                ->where('id', $id)
                ->update(['company_id' => $company, 'debitnote_date' => $debitnote_date, 'remarks' => $remarks, 'sub_total' => $sub_total, 'total' => $sub_total, 'updated_at' => $this->date, 'updated_by' => $user_id]);

            DB::table('purchasereturn_item')
                ->where('parent_id', $id)
                ->delete();

            DB::table('account_txn')
                ->where([
                    ['unique_no', 'PR'.$id],
                    ['company_id', $company],
                ])
                ->delete();

            /*///////////////////Account Mange-Start///////////////////*/

            /*............................
            Account Manage-Purchase Return
            ..............................*/

            // 1st Transaction
            $account_id = getVendorAcccountId($vendor, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PR'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Return Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 2nd Transaction
            $account_id = getAcccountId('name', 'Purchase Return', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'PR'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Purchase Return Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // Update Voucher No in Purchase Return
            DB::table('purchasereturn')
                ->where('id', $id)
                ->update(['voucher_no' => $voucher_no,'unique_no' => 'PR'.$id]);

            /*///////////////////Account Mange-End///////////////////*/

            if (count($item_id) > 0) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $barcode1 = $barcode[$i];
                    $expiry_date1 = $expiry_date[$i];
                    if (!empty($expiry_date1)) {
                        $expiry_date1 = date_format(date_create($expiry_date1), "Y-m-d");
                    }
                    $quantity1 = $quantity[$i];
                    $return_quantity1 = $return_quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    $return_amount1 = $return_amount[$i];
                    if ($item_id1 != '') {
                        DB::table('purchasereturn_item')->insert(['parent_id' => $id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'return_quantity' => $return_quantity1, 'rate' => $rate1, 'amount' => $amount1, 'return_amount' => $return_amount1, 'barcode' => $barcode1, 'expiry_date' => $expiry_date1]);
                    }
                }
            }
            return true;
        });
        return $result;
    }
    public function purchasereturn_delete($id)
    {
        $result = DB::transaction(function () use ($id) {
            $user_id = Auth::id();
            $company = Auth::user()->company_id;

            $purchasereturn_data = DB::table('purchasereturn')
                ->where([
                    ['id', $id],
                ])
                ->first();

            $voucher_no = $purchasereturn_data->voucher_no;

            DB::table('purchasereturn')
                ->where('id', $id)
                ->update(['status' => '0', 'updated_at' => $this->date, 'updated_by' => $user_id]);

            DB::table('account_txn')
                ->where([
                    ['unique_no', 'PR'.$id],
                    ['company_id', $company],
                ])
                ->update(['status' => '0', 'updated_at' => $this->date]);

            return true;
        });
        return $result;
    }
    public function ajax($company_id)
    {
        if (!empty($company_id)) {
            $category = DB::table('category')->where([
                ['status', '1'],
                ['company_id', $company_id],
            ])->get();
            $category_count = $category->count();
            if ($category_count > 0) {
                ?><option value="">Select</option><?php
                foreach ($category as $value) {?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
            } else {
                ?><option value="">Select</option><?php
            }
        } else {
            ?><option value="">Select</option><?php
        }
    }
    public function ajax2($category_id)
    {
        if (!empty($category_id)) {
            $item = DB::table('item')->where([
                ['status', '1'],
                ['category_id', $category_id],
            ])->get();
            $item_count = $item->count();
            if ($item_count > 0) {
                ?><option value="">Select</option><?php
                foreach ($item as $value) {?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
            } else {
                ?><option value="">Select</option><?php
        }
        } else {
            ?><option value="">Select</option><?php
        }
    }
    public function additems($purchasevoucher_no)
    {
        $company_id = Auth::user()->company_id;
        $items = DB::table('purchaseregister')
            ->select('purchaseregister_item.*', 'item.name as item_name', 'item.batch_no as batch_no', 'item.strength', 'item.form_id', 'unit.name as unit_name', 'form.name as form_name', 'vendor.mobile as vendor_mobile', 'vendor.name as vendor_name')
            ->leftJoin('purchaseregister_item', 'purchaseregister_item.parent_id', '=', 'purchaseregister.id')
            ->leftJoin('item', 'purchaseregister_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->leftJoin('vendor', 'vendor.id', '=', 'purchaseregister.vendor_id')
            ->where([
                ['purchaseregister.purchasevoucher_no', $purchasevoucher_no],
                ['purchaseregister.company_id', $company_id],
                ['purchaseregister.status', '1'],
            ])
            ->get();
        foreach ($items as $key => $value) {
            $items[$key]->expiry_date = date_dfy($value->expiry_date);
        }
        print_r(json_encode(array($items)));
    }

}
