<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Balancesheet extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function getBalance($account_id,$to)
    {
        $company = Auth::user()->company_id;
        $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1' AND `created_at` <= '$to') AND `account_id` <> '$account_id'");
        
        if ($data[0]->balance <= 0) {
            return $data[0]->balance*-1;
        } else {
            return 0;
        }
    }
    public function GroupBalance($group_name,$to)
    {
        $company = Auth::user()->company_id;
        $account_group = DB::table('account_group')
			->select('id')
			->where([
                ['status','1'],
                ['name',$group_name],
				['company_id',$company]
				])
            ->first();
        $account_group_id = $account_group->id;
        $account = DB::table('account')
			->where([
                ['account.status','1'],
                ['account.company_id',$company],
                ['account.account_group_id',$account_group_id]
                ])
            ->get();
        foreach($account as $key=>$value){
            $account_id = $value->id;
            $account[$key]->balance = $this->getBalance($account_id,$to);
            if($account[$key]->balance == 0){
                unset($account[$key]);
            }
        }
        return $account;
    }
}
