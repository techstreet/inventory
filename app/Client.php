<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public static function getBalance($account_id)
    {
        $company = Auth::user()->company_id;
        $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id'");

        if ($data[0]->balance < 0) {
            return $data[0]->balance*-1;
        }
         else {
            return 0;
        }
    }
    public function client_list()
    {
        $company = Auth::user()->company_id;

        return DB::table('client')
            ->select(DB::raw('client.*'))
            ->where([
                ['client.status', '1'],
                ['client.company_id', $company],
            ])
            ->groupBy('client.id')
            ->orderBy('client.id', 'DESC')
            ->get();

        // return DB::table('client')
        //     ->select(DB::raw('client.*,sum(client_register.debit) as debit,sum(client_register.credit) as credit'))
        //     ->where([
        //         ['client.status', '1'],
        //         ['client.company_id', $company],
        //     ])
        //     ->leftJoin('client_register', 'client_register.client_id', '=', 'client.id')
        //     ->groupBy('client.id')
        //     ->orderBy('client.id', 'DESC')
        //     ->get();
    }
    public function client_add($company, $name, $contact_name, $email, $address, $phone, $mobile)
    {
        $result = DB::transaction(function () use ($company, $name, $contact_name, $email, $address, $phone, $mobile) {
            $user_id = Auth::id();
            $account_group_id = null;
            $account_group = DB::table('account_group')
                ->select('id')
                ->where([
                    ['company_id', $company],
                    ['name', 'Debtors'],
                    ['type', 'DR'],
                    ['status', '1'],
                ])
                ->first();
            if ($account_group) {
                $account_group_id = $account_group->id;
            }
            $account_id = DB::table('account')->insertGetId(
                ['company_id' => $company, 'type' => 'DR', 'account_group_id' => $account_group_id, 'name' => $name, 'email' => $email, 'address' => $address, 'phone' => $phone, 'mobile' => $mobile, 'created_at' => $this->date]
            );
            $client_id = DB::table('client')->insertGetId(
                ['company_id' => $company, 'name' => $name, 'contact_name' => $contact_name, 'email' => $email, 'address' => $address, 'phone' => $phone, 'mobile' => $mobile, 'created_at' => $this->date, 'created_by' => $user_id]
            );
            return $client_id;
        });
        return $result;
    }
    public function client_edit($id)
    {
        return DB::table('client')->where('id', $id)->get();
    }
    public function client_update($id, $company, $name, $contact_name, $email, $address, $phone, $mobile)
    {
        $user_id = Auth::id();
        return DB::table('client')
            ->where('id', $id)
            ->update(['company_id' => $company, 'name' => $name, 'contact_name' => $contact_name, 'email' => $email, 'address' => $address, 'phone' => $phone, 'mobile' => $mobile, 'updated_at' => $this->date, 'updated_by' => $user_id]);
    }
    public function client_delete($id)
    {
        $user_id = Auth::id();
        return DB::table('client')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date, 'updated_by' => $user_id]);
    }
}
