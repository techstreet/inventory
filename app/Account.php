<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now();
    }
    protected function has_parent($id)
    {
        $abc = '';
        $data = DB::table('account')->where('id', $id)->get();
        $p_id = $data[0]->parent_id;
        $p_name = $data[0]->name;
        if ($p_id) {
            $child = new self($p_id);
            return $child->has_parent($p_id) . ' >> ' . $p_name;
        } else {
            return $p_name;
        }

    }
    public function account_list()
    {
        $company = Auth::user()->company_id;
        $account = DB::table('account')
            ->select('account.*', 'account_group.name as account_group_name', 'financial_group.name as financial_group_name')
            ->leftJoin('account_group', 'account_group.id', '=', 'account.account_group_id')
            ->leftJoin('financial_group', 'financial_group.id', '=', 'account.financial_group_id')
            ->where([
                ['account.status', '1'],
                ['account.company_id', $company],
            ])
            ->orderBy('account.id','asc')
            ->get();
        // foreach ($account as $key => $value) {
        //     $account[$key]->slug = $this->has_parent($value->id);
        // }
        return $account;
    }
    public function account_add($parent, $company, $name, $type, $account_group_id, $financial_group_id, $opening_balance, $restricted, $gst_no, $taxid_no, $phone, $email, $city, $state, $country, $address)
    {
        $user_id = Auth::id();
        return DB::table('account')->insert(
            ['parent_id' => $parent, 'company_id' => $company, 'name' => $name, 'type' => $type, 'account_group_id' => $account_group_id, 'financial_group_id' => $financial_group_id, 'opening_balance' => $opening_balance, 'restricted' => $restricted, 'gst_no' => $gst_no, 'taxid_no' => $taxid_no, 'phone' => $phone, 'email' => $email, 'city' => $city, 'state' => $state, 'country' => $country, 'address' => $address, 'created_at' => $this->date]
        );
    }
    public function account_edit($id)
    {
        return DB::table('account')->where('id', $id)->first();
    }
    public function account_update($id, $parent, $company, $name, $type, $account_group_id, $financial_group_id, $opening_balance, $restricted, $gst_no, $taxid_no, $phone, $email, $city, $state, $country, $address)
    {
        $user_id = Auth::id();
        return DB::table('account')
            ->where('id', $id)
            ->update(['parent_id' => $parent, 'company_id' => $company, 'name' => $name, 'type' => $type, 'account_group_id' => $account_group_id, 'financial_group_id' => $financial_group_id, 'opening_balance' => $opening_balance, 'restricted' => $restricted, 'gst_no' => $gst_no, 'taxid_no' => $taxid_no, 'phone' => $phone, 'email' => $email, 'city' => $city, 'state' => $state, 'country' => $country, 'address' => $address, 'updated_at' => $this->date]);
    }
    public function account_delete($id)
    {
        $user_id = Auth::id();
        return DB::table('account')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date]);
    }
}
