<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Clientaccount extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function getBalance($account_id)
    {
        $company = Auth::user()->company_id;
        $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id'");
        
        if ($data[0]->balance <= 0) {
            return $data[0]->balance*-1;
        } else {
            return 0;
        }
    }
    public static function canDelete($voucher_no)
    {
        $company_id = Auth::user()->company_id;
        $receipt_voucher = DB::table('receipt_voucher')
            ->where([
                ['voucher_no', $voucher_no],
                ['company_id', $company_id],
                ['status', '1'],
            ])
            ->first();
        if($receipt_voucher){
            return $receipt_voucher->id;
        }
        else{
            return false;
        }
    }
    public function get_accountId($id)
    {
        $client = DB::table('client')
            ->where('id', $id)
            ->first();
        return $client->account_id;
    }
    public function clientaccount_detail($from, $to, $account_id)
    {
        $company = Auth::user()->company_id;
        if(!empty($from) && !empty($to)){
            return DB::select("SELECT account_txn.*,account.name FROM `account_txn` LEFT JOIN `account` ON account_txn.account_id = account.id WHERE account_txn.`voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id') AND account_txn.`account_id` <> '$account_id' AND account_txn.`company_id` = '$company' AND account_txn.`status` = '1' AND account_txn.`created_at` BETWEEN '$from' AND '$to'");
        }
        else{
            return DB::select("SELECT account_txn.*,account.name FROM `account_txn` LEFT JOIN `account` ON account_txn.account_id = account.id WHERE account_txn.`voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id') AND account_txn.`account_id` <> '$account_id' AND account_txn.`company_id` = '$company' AND account_txn.`status` = '1'");
        }
    }
    public function get_clientname($id)
    {
        $client = DB::table('client')
            ->where('id', $id)
            ->first();
        return $client->name;
    }
}
