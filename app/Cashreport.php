<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Cashreport extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function cashreport_list($cash_from, $cash_to)
    {
        $company = Auth::user()->company_id;

        $account = DB::table('client')
            ->select('account_id', 'id as customer_id', 'name as customer_name', 'contact_name', 'email', 'phone', 'mobile', 'address')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
        foreach ($account as $key => $value) {
            $account_id = $value->account_id;

            $data = DB::select("SELECT sum(`debit`) as `debit` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$cash_from' AND '$cash_to'");

            $account[$key]->amount = $data[0]->debit;
        }
        return $account;
    }
}
