<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Contravoucher extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now();
    }
    public function contravoucher_list()
    {
        $company = Auth::user()->company_id;
        $contravoucher = DB::table('contra_voucher')
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->orderBy('id','desc')
            ->get();
        return $contravoucher;
    }
    public function contravoucher_add($company, $account_from, $account_to, $amount, $voucher_date, $remarks)
    {
        $result = DB::transaction(function () use ($company, $account_from, $account_to, $amount, $voucher_date, $remarks) {

            if (empty($voucher_date)) {
                $voucher_date = null;
            } else {
                $voucher_date = Carbon::parse($voucher_date)->format('Y-m-d');
            }

            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Contra Voucher' and company_id=$company")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }

            $voucher_no = 'CO' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;

            DB::table('contra_voucher')->insert(
                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_date' => $voucher_date, 'amount' => $amount, 'remarks' => $remarks, 'created_at' => $this->date, 'status' => '1']
            );

            DB::table('account_txn')->insert(
                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_type' => 'Contra Voucher', 'account_id' => $account_from, 'account_type' => 'CR', 'credit' => $amount, 'created_at' => $this->date, 'remarks' => $remarks, 'status' => '1']
            );

            DB::table('account_txn')->insert(
                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_type' => 'Contra Voucher', 'account_id' => $account_to, 'account_type' => 'DR', 'debit' => $amount, 'created_at' => $this->date, 'remarks' => $remarks, 'status' => '1']
            );

            return true;
        });
        return $result;
    }
    public function contravoucher_cancel($id)
    {
        return DB::table('contra_voucher')
            ->where('id', $id)
            ->update(['is_active' => '0', 'updated_at' => $this->date]);
    }
    public function contravoucher_delete($id)
    {
        return DB::table('contra_voucher')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date]);
    }
}
