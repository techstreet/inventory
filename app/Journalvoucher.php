<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Journalvoucher extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now();
    }
    public function journalvoucher_list()
    {
        $company = Auth::user()->company_id;
        $journalvoucher = DB::table('journal_voucher')
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->orderBy('id','desc')
            ->get();
        return $journalvoucher;
    }
    public function journalvoucher_add($company, $voucher_date, $account_id, $debit, $credit, $amount, $remarks)
    {
        $result = DB::transaction(function () use ($company, $voucher_date, $account_id, $debit, $credit, $amount, $remarks) {

            if (empty($voucher_date)) {
                $voucher_date = null;
            } else {
                $voucher_date = Carbon::parse($voucher_date)->format('Y-m-d');
            }

            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Journal Voucher' and company_id=$company")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }

            $voucher_no = 'JO' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;

            DB::table('journal_voucher')->insert(
                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_date' => $voucher_date, 'amount' => $amount, 'remarks' => $remarks, 'created_at' => $this->date, 'status' => '1']
            );

            if (count($account_id) > 0) {
                for ($i = 0; $i < count($account_id); ++$i) {
                    $account_id1 = $account_id[$i];
                    $debit1 = $debit[$i];
                    $credit1 = $credit[$i];
                    if ($account_id1 != '') {
                        if($debit1 > 0){
                            DB::table('account_txn')->insert(
                                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_type' => 'Journal Voucher', 'account_id' => $account_id1, 'account_type' => 'DR', 'debit' => $debit1, 'created_at' => $this->date, 'remarks' => $remarks, 'status' => '1']
                            );
                        }
                        else{
                            DB::table('account_txn')->insert(
                                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_type' => 'Journal Voucher', 'account_id' => $account_id1, 'account_type' => 'CR', 'credit' => $credit1, 'created_at' => $this->date, 'remarks' => $remarks, 'status' => '1']
                            );
                        }
                    }
                }
            }
            
            return true;
        });
        return $result;
    }
    public function journalvoucher_cancel($id)
    {
        return DB::table('journal_voucher')
            ->where('id', $id)
            ->update(['is_active' => '0', 'updated_at' => $this->date]);
    }
    public function journalvoucher_delete($id)
    {
        return DB::table('journal_voucher')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date]);
    }
}
