<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Receiptvoucher extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now();
    }
    public function receiptvoucher_list()
    {
        $company = Auth::user()->company_id;
        $receiptvoucher = DB::table('receipt_voucher')
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->orderBy('id','desc')
            ->get();
        return $receiptvoucher;
    }
    public function receiptvoucher_add($company, $account_id, $amount, $mode, $cheque_no, $cheque_date, $voucher_date, $bank_name, $remarks)
    {
        $result = DB::transaction(function () use ($company, $account_id, $amount, $mode, $cheque_no, $cheque_date, $voucher_date, $bank_name, $remarks) {

            if (empty($cheque_date)) {
                $cheque_date = null;
            } else {
                $cheque_date = Carbon::parse($cheque_date)->format('Y-m-d');
            }

            if (empty($voucher_date)) {
                $voucher_date = null;
            } else {
                $voucher_date = Carbon::parse($voucher_date)->format('Y-m-d');
            }

            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Receipt Voucher' and company_id=$company")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }
            $voucher_no = 'RE' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;

            DB::table('receipt_voucher')->insert(
                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_date' => $voucher_date, 'amount' => $amount, 'mode' => $mode, 'cheque_no' => $cheque_no, 'cheque_date' => $cheque_date, 'bank_name' => $bank_name, 'remarks' => $remarks, 'created_at' => $this->date, 'status' => '1']
            );

            DB::table('account_txn')->insert(
                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_type' => 'Receipt Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $amount, 'created_at' => $this->date, 'remarks' => $remarks, 'status' => '1']
            );

            $account_id = getAcccountId('name', $mode, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'voucher_no' => $voucher_no, 'voucher_type' => 'Receipt Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $amount, 'created_at' => $this->date, 'remarks' => $remarks, 'status' => '1']
            );
            return true;
        });
        return $result;
    }
    public function receiptvoucher_cancel($id)
    {
        return DB::table('receipt_voucher')
            ->where('id', $id)
            ->update(['is_active' => '0', 'updated_at' => $this->date]);
    }
    public function receiptvoucher_delete($id)
    {
        return DB::table('receipt_voucher')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date]);
    }
}
