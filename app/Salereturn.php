<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Salereturn extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function salereturn_list()
    {
        $company = Auth::user()->company_id;
        return DB::table('salereturn')
            ->select('salereturn.*', 'client.name as client_name')
            ->where([
                ['salereturn.status', '1'],
                ['salereturn.company_id', $company],
                ['saleregister.company_id', $company],
            ])
            ->leftJoin('saleregister', 'saleregister.invoice_no', '=', 'salereturn.invoice_no')
            ->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
            ->orderBy('salereturn.id', 'desc')
            ->get();
    }
    public function item_list($category_id)
    {
        return DB::table('item')->where('category_id', $category_id)->get();
    }
    public function category_list($id)
    {
        $item = DB::table('item')->where('id', $id)->get();
        foreach ($item as $value) {
            $company_id = $value->company_id;
        }
        $category = DB::table('category')->where('company_id', $company_id)->get();
        return $category;
    }
    public function category_id($item_id)
    {
        $item = DB::table('item')->where('id', $item_id)->get();
        foreach ($item as $value) {
            $category_id = $value->category_id;
        }
        return $category_id;
    }
    public function getSalereturnName($id)
    {
        $salereturn = DB::table('salereturn')->where('id', $id)->get();
        foreach ($salereturn as $value) {
            $name = $value->name;
        }
        return $name;
    }
    public function salereturn_add($company, $creditnote_no, $creditnote_date, $invoice_no, $return_reason, $remarks, $item_id, $unit_id, $quantity, $return_quantity, $rate, $amount, $return_amount)
    {
        $result = DB::transaction(function () use ($company, $creditnote_no, $creditnote_date, $invoice_no, $return_reason, $remarks, $item_id, $unit_id, $quantity, $return_quantity, $rate, $amount, $return_amount) {
            $user_id = Auth::id();
            if (!empty($creditnote_date)) {
                $creditnote_date = date_format(date_create($creditnote_date), "Y-m-d");
            }
            $sub_total = collect($return_amount)->sum();
            $salereturn_id = DB::table('salereturn')->insertGetId(
                ['company_id' => $company, 'creditnote_no' => $creditnote_no, 'creditnote_date' => $creditnote_date, 'invoice_no' => $invoice_no, 'return_reason' => $return_reason, 'remarks' => $remarks, 'sub_total' => $sub_total, 'total' => $sub_total, 'created_at' => $this->date, 'created_by' => $user_id]
            );

            if (count($item_id) > 0) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $quantity1 = $quantity[$i];
                    $return_quantity1 = $return_quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    $return_amount1 = $return_amount[$i];
                    if ($item_id1 != '') {
                        DB::table('salereturn_item')->insert(['parent_id' => $salereturn_id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'return_quantity' => $return_quantity1, 'rate' => $rate1, 'amount' => $amount1, 'return_amount' => $return_amount1]);
                    }
                }
            }

            $saleregister = DB::table('saleregister')
                ->where([
                    ['invoice_no', $invoice_no],
                    ['company_id', $company],
                ])
                ->first();
            $client = $saleregister->client_id;

            /*///////////////////Account Mange-Start///////////////////*/

            /*............................
            Account Manage-Sale Return
            ..............................*/

            $max_counter = '1';
            $max_voucher = DB::select(
                DB::raw("SELECT Max(cast(substring(voucher_no,instr(voucher_no,'/')+1,length(voucher_no)-instr(voucher_no,'/')) as UNSIGNED)) as max_no FROM `account_txn` where voucher_type='Sale Return Voucher' and company_id=$company and status='1'")
            );
            if (count($max_voucher) > 0) {
                $max_counter = $max_voucher[0]->max_no + 1;
            }
            $voucher_no = 'SR' . date_format(date_create($this->date), "dmY") . '/' . $max_counter;

            // 1st Transaction
            $account_id = getAcccountId('name', 'Sale Return', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'SR'.$salereturn_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Sale Return Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 2nd Transaction
            $account_id = getClientAcccountId($client, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'SR'.$salereturn_id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Sale Return Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // Update Voucher No in Sale Return
            DB::table('salereturn')
                ->where('id', $salereturn_id)
                ->update(['voucher_no' => $voucher_no, 'unique_no' => 'SR'.$salereturn_id]);

            /*///////////////////Account Mange-End///////////////////*/

            return $salereturn_id;
        });
        return $result;
    }
    public function salereturn_edit($id)
    {
        return DB::table('salereturn')
            ->select('salereturn.*', 'saleregister.client_id', 'client.name as client_name', 'client.mobile as client_mobile')
            ->where('salereturn.id', $id)
            ->leftJoin('saleregister', 'saleregister.invoice_no', '=', 'salereturn.invoice_no')
            ->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
            ->get();
    }
    public function salereturn_view($id)
    {
        return DB::table('salereturn')
            ->select('salereturn.*', 'client.name as client_name', 'client.mobile as client_mobile', 'company.name as company_name', 'users.address as address')
            ->where('salereturn.id', $id)
            ->leftJoin('saleregister', 'saleregister.invoice_no', '=', 'salereturn.invoice_no')
            ->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
            ->leftJoin('company', 'salereturn.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->get();
    }
    public function salereturn_item($id)
    {
        return DB::table('salereturn')
            ->select('salereturn_item.*', 'item.name as item_name', 'item.batch_no as batch_no', 'item.strength', 'unit.name as unit_name', 'form.id as form_id', 'form.name as form_name')
            ->where([
                ['salereturn.id', $id],
                ['salereturn.status', '1'],
            ])
            ->leftJoin('salereturn_item', 'salereturn_item.parent_id', '=', 'salereturn.id')
            ->leftJoin('item', 'salereturn_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->get();
    }
    public function salereturn_update($id, $company, $creditnote_date, $invoice_no, $return_reason, $remarks, $item_id, $unit_id, $quantity, $return_quantity, $rate, $amount, $return_amount)
    {
        $result = DB::transaction(function () use ($id, $company, $creditnote_date, $invoice_no, $return_reason, $remarks, $item_id, $unit_id, $quantity, $return_quantity, $rate, $amount, $return_amount) {
            $user_id = Auth::id();
            if (!empty($creditnote_date)) {
                $creditnote_date = date_format(date_create($creditnote_date), "Y-m-d");
            }
            $sub_total = collect($return_amount)->sum();

            $salereturn = DB::table('salereturn')->where('id', $id)->first();
            $invoice_no = $salereturn->invoice_no;
            $voucher_no = $salereturn->voucher_no;

            $saleregister = DB::table('saleregister')
                ->where([
                    ['invoice_no', $invoice_no],
                    ['company_id', $company],
                ])
                ->first();
            $client = $saleregister->client_id;

            DB::table('salereturn')
                ->where('id', $id)
                ->update(['company_id' => $company, 'creditnote_date' => $creditnote_date, 'return_reason' => $return_reason, 'remarks' => $remarks, 'sub_total' => $sub_total, 'total' => $sub_total, 'updated_at' => $this->date, 'updated_by' => $user_id]);

            DB::table('salereturn_item')
                ->where('parent_id', $id)
                ->delete();

            DB::table('account_txn')
                ->where([
                    ['unique_no', 'SR'.$id],
                    ['company_id', $company],
                ])
                ->delete();

            /*///////////////////Account Mange-Start///////////////////*/

            /*............................
            Account Manage-Sale Return
            ..............................*/

            // 1st Transaction
            $account_id = getAcccountId('name', 'Sale Return', $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'SR'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Sale Return Voucher', 'account_id' => $account_id, 'account_type' => 'DR', 'debit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // 2nd Transaction
            $account_id = getClientAcccountId($client, $company);
            DB::table('account_txn')->insert(
                ['company_id' => $company, 'unique_no' => 'SR'.$id, 'voucher_no' => $voucher_no, 'voucher_type' => 'Sale Return Voucher', 'account_id' => $account_id, 'account_type' => 'CR', 'credit' => $sub_total, 'created_at' => $this->date, 'status' => '1']
            );
            // Update Voucher No in Sale Return
            DB::table('salereturn')
                ->where('id', $id)
                ->update(['voucher_no' => $voucher_no, 'unique_no' => 'SR'.$id]);

            /*///////////////////Account Mange-End///////////////////*/

            if (count($item_id) > 0) {
                for ($i = 0; $i < count($item_id); ++$i) {
                    $item_id1 = $item_id[$i];
                    $unit_id1 = $unit_id[$i];
                    $quantity1 = $quantity[$i];
                    $return_quantity1 = $return_quantity[$i];
                    $rate1 = $rate[$i];
                    $amount1 = $amount[$i];
                    $return_amount1 = $return_amount[$i];
                    if ($item_id1 != '') {
                        DB::table('salereturn_item')->insert(['parent_id' => $id, 'item_id' => $item_id1, 'quantity' => $quantity1, 'return_quantity' => $return_quantity1, 'rate' => $rate1, 'amount' => $amount1, 'return_amount' => $return_amount1]);
                    }
                }
            }

            return true;
        });
        return $result;
    }
    public function salereturn_delete($id)
    {
        $result = DB::transaction(function () use ($id) {
            $user_id = Auth::id();

            DB::table('salereturn')
                ->where('id', $id)
                ->update(['status' => '0', 'updated_at' => $this->date, 'updated_by' => $user_id]);

            $company = Auth::user()->company_id;

            DB::table('account_txn')
                ->where([
                    ['unique_no', 'SR'.$id],
                    ['company_id', $company],
                ])
                ->update(['status' => '0', 'updated_at' => $this->date]);

            return true;
        });
        return $result;
    }
    public function ajax($company_id)
    {
        if (!empty($company_id)) {
            $category = DB::table('category')->where([
                ['status', '1'],
                ['company_id', $company_id],
            ])->get();
            $category_count = $category->count();
            if ($category_count > 0) {
                ?><option value="">Select</option><?php
                foreach ($category as $value) {?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
            } else {
                ?><option value="">Select</option><?php
            }
        } else {
            ?><option value="">Select</option><?php
        }
    }
    public function ajax2($category_id)
    {
        if (!empty($category_id)) {
            $item = DB::table('item')->where([
                ['status', '1'],
                ['category_id', $category_id],
            ])->get();
            $item_count = $item->count();
            if ($item_count > 0) {
                ?><option value="">Select</option><?php
                foreach ($item as $value) {?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
            } else {
                ?><option value="">Select</option><?php
            }
        } else {
            ?><option value="">Select</option><?php
        }
    }
    public function additems($invoice_no)
    {
        $company_id = Auth::user()->company_id;
        $items = DB::table('saleregister')
            ->select('saleregister_item.*', 'item.name as item_name', 'item.batch_no as batch_no', 'item.strength', 'unit.name as unit_name', 'form.id as form_id', 'form.name as form_name', 'client.mobile as client_mobile', 'client.name as client_name')
            ->leftJoin('saleregister_item', 'saleregister_item.parent_id', '=', 'saleregister.id')
            ->leftJoin('item', 'saleregister_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->leftJoin('client', 'client.id', '=', 'saleregister.client_id')
            ->where([
                ['saleregister.invoice_no', $invoice_no],
                ['saleregister.company_id', $company_id],
                ['saleregister.status', '1'],
            ])
            ->get();
        foreach ($items as $key => $value) {
            $items[$key]->quantity = getSaleStock($value->item_id, '', '', $invoice_no, $company_id) - getSaleReturnStock($value->item_id, '', '', $invoice_no, $company_id);
        }
        print_r(json_encode(array($items)));
    }

}
