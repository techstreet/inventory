<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Debtreport extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function debtreport_list($debt_from, $debt_to)
    {
        $company = Auth::user()->company_id;

        $account = DB::table('client')
            ->select('account_id', 'id as customer_id', 'name as customer_name', 'contact_name', 'email', 'phone', 'mobile', 'address')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
        foreach ($account as $key => $value) {
            $account_id = $value->account_id;

            $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$debt_from' AND '$debt_to'");

            if ($data[0]->balance < 0) {
                $balance = $data[0]->balance * -1;
            } else {
                $balance = 0;
            }
            $account[$key]->balance = $balance;
        }
        return $account;
    }
}
