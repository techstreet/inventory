<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Welcome extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function sale()
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;
        if ($user_group == '1') {
            $result = DB::table('saleregister')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->first();
            return $result->total;
        }
        $result = DB::table('saleregister')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->first();
        return $result->total;
    }
    public function sale_today($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;
        $today = Carbon::today();

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('saleregister')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereDate('entry_date', $today)
                ->first();
            return $result->total;
        }

        $result = DB::table('saleregister')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereDate('entry_date', $today)
            ->first();

        return $result->total;

    }
    public function sale_thisweek($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;
        $monday = Carbon::now()->startOfWeek();
        $sunday = Carbon::now()->endOfWeek();

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('saleregister')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereBetween('entry_date', [$monday, $sunday])
                ->first();
            return $result->total;
        }
        $result = DB::table('saleregister')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereBetween('entry_date', [$monday, $sunday])
            ->first();
        return $result->total;
    }

    public function sale_thismonth($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('saleregister')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereBetween('entry_date', [$start, $end])
                ->first();
            return $result->total;
        }
        $result = DB::table('saleregister')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereBetween('entry_date', [$start, $end])
            ->first();
        return $result->total;
    }

    public function salereturn($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('salereturn')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->first();
            return $result->total;
        }
        $result = DB::table('salereturn')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->first();
        return $result->total;
    }
    public function salereturn_today($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $today = Carbon::today();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('salereturn')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereDate('creditnote_date', $today)
                ->first();
            return $result->total;
        }
        $result = DB::table('salereturn')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereDate('creditnote_date', $today)
            ->first();
        return $result->total;
    }
    public function salereturn_thisweek($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $monday = Carbon::now()->startOfWeek();
        $sunday = Carbon::now()->endOfWeek();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('salereturn')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereBetween('creditnote_date', [$monday, $sunday])
                ->first();
            return $result->total;
        }
        $result = DB::table('salereturn')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereBetween('creditnote_date', [$monday, $sunday])
            ->first();
        return $result->total;
    }

    public function salereturn_thismonth($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('salereturn')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereBetween('creditnote_date', [$start, $end])
                ->first();
            return $result->total;
        }
        $result = DB::table('salereturn')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereBetween('creditnote_date', [$start, $end])
            ->first();
        return $result->total;
    }

    public function cash($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $account = DB::table('account')
            ->select('id')
            ->where([
                ['name', 'Cash'],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->id;
                $cash_received = DB::table('account_txn')
                    ->select(DB::raw("SUM(debit) as debit"))
                    ->where([
                        ['account_id', $account_id],
                        ['account_type', 'DR'],
                        ['status', '1'],
                    ])
                    ->first();
                $cash = $cash+$cash_received->debit;
            }
            return $cash;
        } else {
            $account = DB::table('account')
                ->select('id')
                ->where([
                    ['name', 'Cash'],
                    ['company_id', $company],
                    ['status', '1'],
                ])
                ->first();
            $account_id = $account->id;
            $cash_received = DB::table('account_txn')
                ->select(DB::raw("SUM(debit) as debit"))
                ->where([
                    ['account_id', $account_id],
                    ['account_type', 'DR'],
                    ['company_id', $company],
                    ['status', '1'],
                ])
                ->first();
            return $cash_received->debit;
        }
    }
    public function cash_today($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $today = Carbon::today();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $account = DB::table('account')
            ->select('id')
            ->where([
                ['name', 'Cash'],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->id;
                $cash_received = DB::table('account_txn')
                    ->select(DB::raw("SUM(debit) as debit"))
                    ->where([
                        ['account_id', $account_id],
                        ['account_type', 'DR'],
                        ['status', '1'],
                    ])
                    ->whereDate('account_txn.created_at', $today)
                    ->first();
                $cash = $cash+$cash_received->debit;
            }
            return $cash;
        } else {
            $account = DB::table('account')
                ->select('id')
                ->where([
                    ['name', 'Cash'],
                    ['company_id', $company],
                    ['status', '1'],
                ])
                ->first();
            $account_id = $account->id;
            $cash_received = DB::table('account_txn')
                ->select(DB::raw("SUM(debit) as debit"))
                ->where([
                    ['account_id', $account_id],
                    ['account_type', 'DR'],
                    ['company_id', $company],
                    ['status', '1'],
                ])
                ->whereDate('account_txn.created_at', $today)
                ->first();
            return $cash_received->debit;
        }
    }
    public function cash_thisweek($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $monday = Carbon::now()->startOfWeek();
        $sunday = Carbon::now()->endOfWeek();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $account = DB::table('account')
            ->select('id')
            ->where([
                ['name', 'Cash'],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->id;
                $cash_received = DB::table('account_txn')
                    ->select(DB::raw("SUM(debit) as debit"))
                    ->where([
                        ['account_id', $account_id],
                        ['account_type', 'DR'],
                        ['status', '1'],
                    ])
                    ->whereBetween('account_txn.created_at', [$monday, $sunday])
                    ->first();
                $cash = $cash+$cash_received->debit;
            }
            return $cash;
        } else {
            $account = DB::table('account')
                ->select('id')
                ->where([
                    ['name', 'Cash'],
                    ['company_id', $company],
                    ['status', '1'],
                ])
                ->first();
            $account_id = $account->id;
            $cash_received = DB::table('account_txn')
                ->select(DB::raw("SUM(debit) as debit"))
                ->where([
                    ['account_id', $account_id],
                    ['account_type', 'DR'],
                    ['company_id', $company],
                    ['status', '1'],
                ])
                ->whereBetween('account_txn.created_at', [$monday, $sunday])
                ->first();
            return $cash_received->debit;
        }
    }

    public function cash_thismonth($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $account = DB::table('account')
            ->select('id')
            ->where([
                ['name', 'Cash'],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->id;
                $cash_received = DB::table('account_txn')
                    ->select(DB::raw("SUM(debit) as debit"))
                    ->where([
                        ['account_id', $account_id],
                        ['account_type', 'DR'],
                        ['status', '1'],
                    ])
                    ->whereBetween('account_txn.created_at', [$start, $end])
                    ->first();
                $cash = $cash+$cash_received->debit;
            }
            return $cash;
        } else {
            $account = DB::table('account')
                ->select('id')
                ->where([
                    ['name', 'Cash'],
                    ['company_id', $company],
                    ['status', '1'],
                ])
                ->first();
            $account_id = $account->id;
            $cash_received = DB::table('account_txn')
                ->select(DB::raw("SUM(debit) as debit"))
                ->where([
                    ['account_id', $account_id],
                    ['account_type', 'DR'],
                    ['company_id', $company],
                    ['status', '1'],
                ])
                ->whereBetween('account_txn.created_at', [$start, $end])
                ->first();
            return $cash_received->debit;
        }
    }

    public function purchase($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('purchaseregister')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->first();
            return $result->total;
        }
        $result = DB::table('purchaseregister')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->first();
        return $result->total;
    }
    public function purchase_today($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $today = Carbon::today();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('purchaseregister')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereDate('entry_date', $today)
                ->first();
            return $result->total;
        }
        $result = DB::table('purchaseregister')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereDate('entry_date', $today)
            ->first();
        return $result->total;
    }
    public function purchase_thisweek($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $monday = Carbon::now()->startOfWeek();
        $sunday = Carbon::now()->endOfWeek();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('purchaseregister')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereBetween('entry_date', [$monday, $sunday])
                ->first();
            return $result->total;
        }
        $result = DB::table('purchaseregister')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereBetween('entry_date', [$monday, $sunday])
            ->first();
        return $result->total;
    }

    public function purchase_thismonth($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('purchaseregister')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereBetween('entry_date', [$start, $end])
                ->first();
            return $result->total;
        }
        $result = DB::table('purchaseregister')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereBetween('entry_date', [$start, $end])
            ->first();
        return $result->total;
    }

    public function purchasereturn($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('purchasereturn')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->first();
            return $result->total;
        }
        $result = DB::table('purchasereturn')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->first();
        return $result->total;
    }
    public function purchasereturn_today($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $today = Carbon::today();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('purchasereturn')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereDate('debitnote_date', $today)
                ->first();
            return $result->total;
        }
        $result = DB::table('purchasereturn')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereDate('debitnote_date', $today)
            ->first();
        return $result->total;
    }
    public function purchasereturn_thisweek($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $monday = Carbon::now()->startOfWeek();
        $sunday = Carbon::now()->endOfWeek();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('purchasereturn')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereBetween('debitnote_date', [$monday, $sunday])
                ->first();
            return $result->total;
        }
        $result = DB::table('purchasereturn')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereBetween('debitnote_date', [$monday, $sunday])
            ->first();
        return $result->total;
    }

    public function purchasereturn_thismonth($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $result = DB::table('purchasereturn')
                ->select(DB::raw("SUM(total) as total"))
                ->where([
                    ['status', '1'],
                ])
                ->whereBetween('debitnote_date', [$start, $end])
                ->first();
            return $result->total;
        }
        $result = DB::table('purchasereturn')
            ->select(DB::raw("SUM(total) as total"))
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->whereBetween('debitnote_date', [$start, $end])
            ->first();
        return $result->total;
    }

    public function credit()
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;
        if ($user_group == '1') {
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `status` = '1') AND `account_id` <> '$account_id'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
        else{
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
    }
    public function credit_today()
    {
        $company = Auth::user()->company_id;
        $today = Carbon::today();
        $user_group = Auth::user()->user_group;
        if ($user_group == '1') {
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` = '$today'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
        else{
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` = '$today'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
    }
    public function credit_thisweek()
    {
        $company = Auth::user()->company_id;
        $monday = Carbon::now()->startOfWeek();
        $sunday = Carbon::now()->endOfWeek();
        $user_group = Auth::user()->user_group;
        if ($user_group == '1') {
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$monday' AND '$sunday'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
        else{
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$monday' AND '$sunday'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
    }

    public function credit_030($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $start = Carbon::now()->subMonths(1)->startOfDay();
        $end = Carbon::today()->endOfDay();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$start' AND '$end'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
        else{
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$start' AND '$end'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
    }

    public function credit_3060($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $start = Carbon::now()->subMonths(2)->startOfDay();
        $end = Carbon::now()->subMonths(1)->endOfDay();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$start' AND '$end'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
        else{
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$start' AND '$end'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
    }

    public function credit_6090($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $start = Carbon::now()->subMonths(3)->startOfDay();
        $end = Carbon::now()->subMonths(2)->endOfDay();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$start' AND '$end'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
        else{
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` BETWEEN '$start' AND '$end'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
    }

    public function credit_90120($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $date = Carbon::now()->subMonths(3)->endOfDay();
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` <= '$date'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
        else{
            $account = DB::table('client')
            ->select('account_id')
            ->where([
                ['company_id', $company],
                ['status', '1'],
            ])
            ->get();
            $cash = 0;
            foreach($account as $key=>$value){
                $account_id = $value->account_id;

                $data = DB::select("SELECT sum(`debit`-`credit`) as `balance` FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id' AND `company_id` = '$company' AND `status` = '1') AND `account_id` <> '$account_id' AND `created_at` <= '$date'");

                if ($data[0]->balance < 0) {
                    $balance = $data[0]->balance*-1;
                }
                else {
                    $balance = 0;
                }
                $cash = $cash+$balance;
            }
            return $cash;
        }
    }

    public function latest_client()
    {
        $company = Auth::user()->company_id;
        return DB::table('client')
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->orderBy('client.id', 'desc')
            ->limit(10)
            ->get();
    }
    public function latest_vendors()
    {
        $company = Auth::user()->company_id;
        return DB::table('vendor')
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();
    }

    public function remaining_stock($company_id = null, $u_group = null)
    {
        $company = Auth::user()->company_id;
        $user_group = Auth::user()->user_group;

        if (!empty($company_id)) {
            $company = $company_id;
        }

        if (!empty($u_group)) {
            $user_group = $u_group;
        }

        if ($user_group == '1') {
            $items = DB::table('item')
                ->where([
                    ['status', '1'],
                ])
                ->get();
        } else {
            $items = DB::table('item')
                ->where([
                    ['status', '1'],
                    ['company_id', $company],
                ])
                ->get();
        }
        $opening_stock = 0;
        $purchase = 0;
        $purchase_return = 0;
        $purchase_total = 0;
        $sale = 0;
        $sale_return = 0;
        $sale_total = 0;
        $closing_stock = 0;
        $unit_price = 0;
        $stock_value = 0;
        foreach ($items as $value) {
            $opening_stock += getOpeningStock($value->id);
            $purchase += getPurchaseStock($value->id);
            $purchase_return += getPurchaseReturnStock($value->id);
            $purchase_total += getPurchaseStock($value->id) - getPurchaseReturnStock($value->id);
            $sale += getTotalSales($value->id);
            $sale_return += getTotalSaleReturnStock($value->id);
            $sale_total += getTotalSales($value->id) - getTotalSaleReturnStock($value->id);
            $closing_stock += $opening_stock + $purchase_total - $sale_total;
            $unit_price += getPrice($value->id);
            $stock_value += getStock($value->id) * getPrice($value->id);
        }
        return $stock_value;
    }

}
