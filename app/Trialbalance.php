<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Trialbalance extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
}
