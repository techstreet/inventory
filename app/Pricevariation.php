<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Pricevariation extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }
    public function pricevariation_list($item_id, $from_date, $to_date)
    {
        $company = Auth::user()->company_id;
        $from_date = date_format(date_create($from_date), "Y-m-d");
        $to_date = date_format(date_create($to_date), "Y-m-d");

        return DB::table('purchaseregister')
            ->select('purchaseregister.*', 'purchaseregister_item.rate', 'item.name', 'item.currency', 'category.name as category_name', 'brand.name as brand_name', 'manufacturer.name as manufacturer_name', 'form.name as form_name', 'company.name as company_name', 'users.address as address')
            ->leftJoin('company', 'purchaseregister.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->groupby('id')
            ->leftJoin('purchaseregister_item', 'purchaseregister_item.parent_id', '=', 'purchaseregister.id')
            ->leftJoin('item', 'item.id', '=', 'purchaseregister_item.item_id')
            ->leftJoin('category', 'category.id', '=', 'item.category_id')
            ->leftJoin('brand', 'brand.id', '=', 'item.brand_id')
            ->leftJoin('manufacturer', 'manufacturer.id', '=', 'item.manufacturer_id')
            ->leftJoin('form', 'form.id', '=', 'item.form_id')
            ->where([
                ['purchaseregister_item.item_id', $item_id],
                ['purchaseregister.company_id', $company],
                ['purchaseregister.status', '1'],
            ])
            ->whereBetween('purchaseregister.entry_date', [$from_date, $to_date])
            ->orderBy('purchaseregister.id', 'desc')
            ->get();
    }
}
