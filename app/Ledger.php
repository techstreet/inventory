<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Ledger extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now('Asia/Kolkata');
    }

    public function debit($from, $to, $account_id)
    {
        $company = Auth::user()->company_id;

        return DB::select("SELECT account_txn.*,account.name FROM `account_txn` LEFT JOIN `account` ON account_txn.account_id = account.id WHERE account_txn.`voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id') AND account_txn.`account_id` <> '$account_id' AND account_txn.`account_type` = 'DR' AND account_txn.`debit` > 0 AND account_txn.`company_id` = '$company' AND account_txn.`status` = '1' AND account_txn.`created_at` BETWEEN '$from' AND '$to'");
    }

    public function credit($from, $to, $account_id)
    {
        $company = Auth::user()->company_id;

        return DB::select("SELECT account_txn.*,account.name FROM `account_txn` LEFT JOIN `account` ON account_txn.account_id = account.id WHERE account_txn.`voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id') AND account_txn.`account_id` <> '$account_id' AND account_txn.`account_type` = 'CR' AND account_txn.`credit` > 0 AND account_txn.`company_id` = '$company' AND account_txn.`status` = '1' AND account_txn.`created_at` BETWEEN '$from' AND '$to'");
    }

    public function debit_sum($from, $to, $account_id)
    {
        $company = Auth::user()->company_id;

        $data = DB::select("SELECT sum(`debit`) as total FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id') AND `account_id` <> '$account_id' AND `account_type` = 'DR' AND `debit` > 0 AND `company_id` = '$company' AND `status` = '1' AND `created_at` BETWEEN '$from' AND '$to'");

        return $data[0]->total;
    }

    public function credit_sum($from, $to, $account_id)
    {
        $company = Auth::user()->company_id;

        $data = DB::select("SELECT sum(`credit`) as total FROM `account_txn` WHERE `voucher_no` IN (SELECT `voucher_no` FROM `account_txn`  WHERE `account_id` = '$account_id') AND `account_id` <> '$account_id' AND `account_type` = 'CR' AND `credit` > 0 AND `company_id` = '$company' AND `status` = '1' AND `created_at` BETWEEN '$from' AND '$to'");

        return $data[0]->total;
    }
}
