<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Accountgroup extends Model
{
    public function __construct()
    {
        $this->date = Carbon::now();
    }
    protected function has_parent($id)
    {
        $abc = '';
        $data = DB::table('account_group')->where('id', $id)->get();
        $p_id = $data[0]->parent_id;
        $p_name = $data[0]->name;
        if ($p_id) {
            $child = new self($p_id);
            return $child->has_parent($p_id) . ' >> ' . $p_name;
        } else {
            return $p_name;
        }

    }
    public function accountgroup_list()
    {
        $company = Auth::user()->company_id;
        $accountgroup = DB::table('account_group')
            ->where([
                ['status', '1'],
                ['company_id', $company],
            ])
            ->get();
        foreach ($accountgroup as $key => $value) {
            $accountgroup[$key]->slug = $this->has_parent($value->id);
        }
        return $accountgroup;
    }
    public function accountgroup_add($parent, $company, $name, $type)
    {
        $user_id = Auth::id();
        return DB::table('account_group')->insert(
            ['parent_id' => $parent, 'company_id' => $company, 'name' => $name, 'type' => $type, 'created_at' => $this->date]
        );
    }
    public function accountgroup_edit($id)
    {
        return DB::table('account_group')->where('id', $id)->get();
    }
    public function accountgroup_update($id, $parent, $company, $name, $type)
    {
        $user_id = Auth::id();
        return DB::table('account_group')
            ->where('id', $id)
            ->update(['parent_id' => $parent, 'company_id' => $company, 'name' => $name, 'type' => $type, 'updated_at' => $this->date]);
    }
    public function accountgroup_delete($id)
    {
        $user_id = Auth::id();
        return DB::table('account_group')
            ->where('id', $id)
            ->update(['status' => '0', 'updated_at' => $this->date]);
    }
}
