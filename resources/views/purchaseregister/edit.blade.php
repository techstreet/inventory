@extends('layouts.website')
@section('content')
<?php
foreach($purchaseregister as $value){
	$purchasevoucher_no = $value->purchasevoucher_no;
	$purchaseorder_no = $value->purchaseorder_no;
	$vendor_id = $value->vendor_id;
	$vendor_name = $value->vendor_name;
	$remarks = $value->remarks;
	$entry_date = $value->entry_date;
	$bill_no = $value->bill_no;
	$bill_date = $value->bill_date;
	$sub_total = $value->sub_total;
	$discount = $value->discount;
	$tax = $value->tax;
	$total = $value->total;
}
?>
<div class="pageheader">
		<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="media-body">
    	<h4 class="test">Purchase Register</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{ url('/purchaseregister') }}">Purchase Register</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
	  <div class="col-md-12" id="flashmessage">
	  	@include('flashmessage')
	  </div>
	</div>
  	@if(empty($purchaseorder_no))
  		@include('purchaseregister/edit_byitem')
  	@else
  		@include('purchaseregister/edit_byitem')
  	@endif
</div>
</div>
@endsection