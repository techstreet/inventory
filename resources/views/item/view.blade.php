@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="media-body">
		<h4>Items</h4>
		<ul class="breadcrumb">
            <li><a href="{{url('')}}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Masters</li>
            <li><a href="{{url('/item')}}">Items</a></li>
            <li>View</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		  <table class="table">
            <thead>
              <tr>
                <th class="left-align" colspan="4">Item Details</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Generic Name:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->name }}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Category:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->slug }}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Price:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->price }}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Currency:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->currency }}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Strength:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->strength }}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Unit:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->unit }}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Brand:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->brand }}</td>
                <!--<td style="padding-top: 15px" class="left-align bt-none"><b>Manufacturer:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->manufacturer }}</td>-->
                <td style="padding-top: 15px" class="left-align bt-none"><b>Form:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->form }}</td>
              </tr>
              <tr>
                <!--<td style="padding-top: 15px" class="left-align bt-none"><b>Form:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->form }}</td>-->
                <td style="padding-top: 15px" class="left-align bt-none"><b>Pack Size:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->pack_size }}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Notify Quantity:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->notify_quantity }}</td>
              </tr>
              <!--<tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Notify Quantity:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->notify_quantity }}</td>
              </tr>-->
            </tbody>
	    </table> 
      	</div>
	</div>
</div>
</div>
@endsection