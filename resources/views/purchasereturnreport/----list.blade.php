@extends('layouts.website')
@section('content')
<?php
$from_date = '';
$to_date = '';
if($_POST && $_POST>0){
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
}
?>
<script>
$(function(){
$(".basic-select").select2();
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('.input-group.date').datepicker({
    calendarWeeks: true,
    todayHighlight: true,
    autoclose: true,
    format: "dd-MM-yyyy",
    //startDate: today
});  
});
function printPage() {
    document.title = "Purchase Return Report";
    window.print();
}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
            <i class="fa fa-file-text-o"></i>
        </div>
		<div class="media-body">
		<h4>Purchase Return Report</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Reports</li>
		    <li>Purchase Return Report</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
		<div class="col-md-12">
			<form class="form-inline" method="post" action="">
			{{ csrf_field() }}
				<div class="form-group" style="margin-right: 0">
					<div class="input-group date">
						<input type="text" name="from_date" class="form-control" value="{{$from_date}}" placeholder="From Date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="To Date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
				<button type="submit" name="submit" class="btn btn-info">Find</button>
			</form>
		</div>
	</div>
	<?php if($_POST && $_POST > 0){ ?>     
    <div class="row" style="margin-top: 10px">
	    <div class="col-md-12">
<h4 class="center onlyprint"><u>{{$purchasereturnreport[0]->company_name}}</u></h4><!--function updated by anurag company address-->
             <h5 class="center onlyprint">{{$purchasereturnreport[0]->address}}</h5><!--function updated by anurag company address-->
	    @include('flashmessage')
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Date</th>
		        <th>Voucher No</th>
		        <th>Debit Note No</th>
		        <th>Supplier Name</th>
		        <th>Sub-Total</th>
		        <th>Discount</th>
		        <th>Tax</th>
		        <th>Total</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($purchasereturnreport as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ date_dfy($value->created_at) }}</td>
	            <td>{{ $value->purchasevoucher_no }}</td>
	            <td>{{ $value->debitnote_no }}</td>
	            <td>{{ $value->vendor_name }}</td>
	            <td>{{ $value->sub_total }}</td>
	            <td>{{ $value->discount }}</td>
	            <td>{{ $value->tax }}</td>
	            <td>{{ $value->total }}</td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
@endsection