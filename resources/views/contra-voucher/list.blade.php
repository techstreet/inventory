@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<a href="{{ url('/contra-voucher/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Create Contra Voucher</button></a>
		</div>
		<div class="media-body">
		<h4>Contra Voucher</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Account Register</li>
		    <li>Contra Voucher</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Voucher Number</th>
						<th>Voucher Date</th>
		        <th>Amount</th>
						<th>Remarks</th>
		        <th width="50px" style="min-width: 50px;">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($paymentvoucher as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->voucher_no }}</td>
	            <td>{{ date_dfy($value->voucher_date) }}</td>
							<td>{{ $value->amount }}</td>
							<td>{{ $value->remarks }}</td>
	            <td>
	            <a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/contra-voucher/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-times"></i></a>
							</td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    });
});
</script>
@endsection