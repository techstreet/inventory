@extends('layouts.website') @section('content')
<?php
use Carbon\Carbon;
$from_date = '';
$to_date = '';
if($_POST && $_POST>0){
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
}
?>
<script>
	$(function () {

		var debit_total = 0;
		var credit_total = 0;

		$('.debit-total').each(function(){
			var amount = $(this).text();
			if(amount == ''){
				amount = 0;
			}
			debit_total += parseFloat(amount);
		});
		$('.credit-total').each(function(){
			var amount = $(this).text();
			if(amount == ''){
				amount = 0;
			}
			credit_total += parseFloat(amount);
		});

		$('.debit-total-sum').text(debit_total);
		$('.credit-total-sum').text(credit_total);

		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.input-group.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
	});
	function printPage() {
		document.title = "Profit & Loss";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-file-text-o"></i>
		</div>
		<div class="media-body">
			<h4>Profit & Loss</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>Account Register</li>
				<li>Profit & Loss</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12">
			<h4 class="center onlyprint"><u>{{Auth::user()->name}}</u></h4>
			<h5 class="center onlyprint">{{Auth::user()->address}}</h4>
				@include('flashmessage')
				<form class="form-inline" method="post" action="">
					{{ csrf_field() }}
					<div class="input-group date">
						<input type="text" name="from_date" class="form-control" value="{{$from_date}}" placeholder="From Date" required="" autocomplete="off">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
					</div>
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="To Date" required="" autocomplete="off">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
					</div>
					<button type="submit" name="submit" class="btn btn-info">Find</button>
				</form>
			</div>
		</div>
		<?php if($_POST && $_POST > 0){ ?>
		<div class="row" style="margin-top: 10px">
			<div class="col-md-8">
				@include('flashmessage')
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable">
						<thead>
							<tr>
								<th colspan="4">For the period from {{$from_date}} to {{$to_date}}</th>
							</tr>
							<tr>
								<th colspan="2">Debit</th>
								<th colspan="2">Credit</th>
							</tr>
							<tr>
								<th>Particulars</th>
								<th>Amount</th>
								<th>Particular</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>To Opening Stock</th>
								<th class="debit-total">{{$opening_stock}}</th>
								<th>By Sale</th>
								<th class="credit-total">{{$actual_sale}}</th>
							</tr>
							<tr>
								<th>To Purchase</th>
								<th class="debit-total">{{$actual_purchase}}</th>
								<th>By Closing Stock</th>
								<th class="credit-total">{{$closing_stock}}</th>
							</tr>
							<tr>
								<th>Expense</th>
								<th>{{$expense}}</th>
								<th>Income</th>
								<th>{{$income}}</th>
							</tr>
							@if($gross_profit > 0)
							<tr>
								<th>To Gross Profit c/d</th>
								<th class="debit-total">{{$gross_profit}}</th>
								<th colspan="2"></th>
							</tr>
							@endif
							@if($gross_loss > 0)
							<tr>
								<th colspan="2"></th>
								<th>By Gross Loss c/d</th>
								<th class="credit-total">{{$gross_loss}}</th>
							</tr>
							@endif
							<!-- Total -->
							<tr style="background: beige">
								<th style="text-align:right">Total: </th>
								<th>{{$actual_purchase+$opening_stock+$expense+$gross_profit}}</th>
								<th style="text-align:right">Total: </th>
								<th>{{$actual_sale+$closing_stock+$income+$gross_loss}}</th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
@endsection