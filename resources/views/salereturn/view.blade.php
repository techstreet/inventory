@extends('layouts.website')
@section('content')
<script>
	function printPage() {
	    document.title = "Sale Return";
	    window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
            <i class="fa fa-exchange"></i>
        </div>
		<div class="media-body">
		<h4>Sale Return</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li><a href="{{ url('/salereturn') }}">Sale Return</a></li>
		    <li>View</li>
		    <li>{{Request::segment(3)}}</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-8">
		  <?php
		  foreach ($salereturn as $value){
		  	$company_name = $value->company_name;
		  	$invoice_no = $value->invoice_no;
		  	$creditnote_no = $value->creditnote_no;
		  	$creditnote_date = $value->creditnote_date;
		  	$client_name = $value->client_name;
		  	$remarks = $value->remarks;
		  	$client_mobile = $value->client_mobile;
		  	$return_reason = $value->return_reason;
		  	$sub_total = $value->sub_total;
		  	$by = $value->created_by;
		  }
		  ?>
		  <h4 class="center onlyprint"><u>{{$company_name}}</u></h4>
<h5 class="center onlyprint">{{$salereturn[0]->address}}</h4><!--function updated by anurag company address-->
		  <table class="table">
            <thead>
              <tr>
                <th class="left-align" colspan="4">Order Details</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Credit Note No:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$creditnote_no}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Credit Note Date:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{date_dfy($creditnote_date)}}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Invoice No:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$invoice_no}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Client:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$client_name}}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Mobile:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$client_mobile}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Return Reason:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$return_reason}}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Sales Person:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{getUserName($by)}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Remarks:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$remarks}}</td>
              </tr>
            </tbody>
	    </table> 
      	</div>
	</div>
	<div class="row">
	  <div class="col-md-8">
		<div class="table-responsive">
	          <table class="table table-bordered mb30">
	            <tbody>
	              <tr>
	                <td width="50px" class="left-align"><b>S.No</b></td>
	                <td class="left-align"><b>Name</b></td>
	                <td class="left-align"><b>Strength</b></td>
	                <td class="left-align"><b>Unit</b></td>
	                <td class="left-align"><b>Form</b></td>
	                <td class="left-align"><b>Quantity</b></td>
	                <td class="left-align"><b>Return Quantity</b></td>
	                <td class="left-align"><b>Cost</b></td>
	                <td class="left-align"><b>Return Total</b></td>
	              </tr>
	              <?php $i=1; foreach ($salereturn_item as $value){ ?>
	              <tr>
	                <td class="left-align">{{ $i }}</td>
	                <td class="left-align">{{ $value->item_name }}</td>
	                <td class="left-align">{{ $value->strength }}</td>
	                <td class="left-align">{{ $value->unit_name }}</td>
	                <td class="left-align">{{ $value->form_name }}</td>
	                <td class="left-align">{{ $value->quantity }}</td>
	                <td class="left-align">{{ $value->return_quantity }}</td>
	                <td class="left-align">{{ $value->rate }}</td>
	                <td class="left-align">{{ $value->return_amount }}</td>
	              </tr>
	              <?php $i++; }?>
	              <tr>
					<td colspan="8" class="right-align">Sub-Total:</td>
					<td class="left-align"><b>{{$sub_total}}</b></td>
	              </tr>
	            </tbody>
	          </table>
          </div>
	  </div>
	</div>
</div>
</div>
@endsection