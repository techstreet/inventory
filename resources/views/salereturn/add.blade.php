@extends('layouts.website')
@section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$entry_date = date("d-F-Y");
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> 
<script src="{{ url('/js/mycustom.js') }}"></script>
<script type='text/javascript'>
$(function(){
	// Select 2 Dropdown initialization
	$("#item").select2();
	// Upper case
	$('#invoice_no').on('keyup change', function(e){
		e.preventDefault();
		var text = $(this).val();
		$('#invoice_no').val(text.toUpperCase());
	});
	// Fill by P.O
	$(".search").click(function(){
		var invoice_no = $('#invoice_no').val();
		if(invoice_no != ''){
			$.ajax({
				type: "GET",
				url: "{{url('/salereturn/additems')}}",
				data:'invoice_no='+invoice_no,
				success: function(data){
					var obj = JSON.parse(data);
					if(obj[0].length >0){
						$('.field_wrapper').html('');
						$.each(obj[0], function (index, value) {
						  var item_id = obj[0][index].item_id;
						  var item_name = obj[0][index].item_name;
						  var batch_no = obj[0][index].batch_no;
						  if(batch_no === null){
							batch_no = '';
						  }
						  var strength = obj[0][0].strength;
						  var form_id = obj[0][0].form_id;
						  var form_name = obj[0][0].form_name;
						  var unit_id = obj[0][index].unit_id;
						  var unit_name = obj[0][index].unit_name;
						  var quantity = obj[0][index].quantity;
						  var rate = obj[0][index].rate;
						  var amount = obj[0][index].amount;
						  
						  $('.field_wrapper').append('<tr><td class="center"><span>'+item_name+'</span><input type="hidden" name="item_id[]" value="'+item_id+'"></td><td class="center"><span>'+batch_no+'</span></td><td class="center"><span>'+strength+'</span><input type="hidden" name="strength[]" class="strength" value="'+strength+'"></td><td class="center"><span>'+unit_name+'</span><input type="hidden" name="unit_id[]" value="'+unit_id+'"></td><td class="center"><span>'+form_name+'</span><input type="hidden" name="form_id[]" class="form_id" value="'+form_id+'"></td><td class="center"><span>'+quantity+'</span><input type="hidden" name="quantity[]" value="'+quantity+'"></td><td class="center return_qty" style="max-width: 50px"><input class="form-control return_qtyVal" type="number" name="return_quantity[]" min="1"></td><td class="center rate"><span>'+rate+'</span><input class="rateVal" type="hidden" name="rate[]" value="'+rate+'"></td><td class="center amount"><span class="amountText">'+amount+'</span><input class="amountVal" type="hidden" name="amount[]" value="'+amount+'"><input class="return_amountVal" type="hidden" name="return_amount[]" value="0"></td></tr>');
						});
						$('.field_wrapper').append('<tr><td colspan="8" class="right-align">Return Amount:</td><td class="center-align subtotalText"><b>0.00</b></td></tr>');
						$('#client').val(obj[0][0].client_name);
					}
					else{
						$('.field_wrapper').html('');
						$(".nav-tabs").before('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> P.O Number not found!</div>');
						setTimeout(function(){ $('.alert.alert-danger').fadeOut(300)}, 3000);
					}
				}
			});
		}
	});
	// Input Type Date
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.input-group.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	//Item Table
	var return_quantity = 0;
	var rate = 0;
	$('.field_wrapper').on('keyup change', '.return_qtyVal', function(e){
		e.preventDefault();
		return_quantity = $(this).val();
		rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
		$(this).closest('tr').children('td.amount').children('input.return_amountVal').val(parseFloat(return_quantity*rate).toFixed(2));
		var sub_total = 0;
	    $('.return_amountVal').each(function () {
	    	sub_total = sub_total + parseFloat($(this).val());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	});
});
</script>
<div class="pageheader">
		<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-exchange"></i>
        </div>
        <div class="media-body">
    	<h4 class="test">Sale Return</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{ url('/salereturn') }}">Sale Return</a></li>
            <li>Add</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
	  <div class="col-md-10" id="flashmessage">
	  	@include('flashmessage')
	  </div>
	</div>
  	<div class="row">
  		<div class="col-md-10">
  			<form method="post" action="{{ url('salereturn/save') }}">
		  	{{ csrf_field() }}
		  	<div class="row">
		  		<div class="col-md-6">
		  			<div class="form-group">
					  <label>Credit Note No:</label>
					  <input type="text" class="form-control" value="{{'CN_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}" disabled="">
					  <input type="hidden" name="creditnote_no" id="creditnote_no" value="{{'CN_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}">
					</div>
		  		</div>
		  		<div class="col-md-6">
		  			<div class="form-group">
					  <label>*Credit Note Date:</label>
					  <div class="input-group date">
						<input type="text" name="creditnote_date" id="creditnote_date" value="{{$entry_date}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					  </div>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-6">
					<div class="form-group">
				      <label>*Invoice Number:</label>
					  <div class="input-group">
						<input type="text" class="form-control" name="invoice_no" id="invoice_no" value="{{ old('invoice_no') }}" required="" autocomplete="off"><span class="input-group-addon search"><i class="fa fa-search"></i></span>
					  </div>
				    </div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					  <label>Client Name:</label>
					  <input type="text" class="form-control" name="client" id="client" disabled="">
					</div>
				</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="table-responsive">
						<table class="table table-bordered" id="itemTable2">
							<tr>
				                <th>Name</th>
								<th>Batch No</th>
				                <th>Strength</th>
				                <th>Unit</th>
				                <th>Form</th>
				                <th>Quantity</th>
				                <th>Return Quantity</th>
				                <th>Cost</th>
				                <th>Total</th>
				            </tr>
							<tbody class="field_wrapper">
								<tr>
									<td colspan="8" class="right-align">Return Amount:</td>
									<td class="center-align subtotalText"><b>0.00</b></td>
					            </tr>
							</tbody>
						</table>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="form-group">
					  <label>Reason For Return:</label>
					  <textarea class="form-control" placeholder="Enter reason" name="return_reason" id="return_reason">{{ old('return_reason') }}</textarea>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="form-group">
					  <label>Remarks:</label>
					  <input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{ old('remarks') }}">
					</div>
		  		</div>
		  	</div>
			<button type="submit" class="btn btn-primary">Save</button>
			</form>
  		</div>
  	</div>
</div>
</div>
<script type="text/javascript">
    var url = "{{ url('/salereturn/autocomplete') }}";
    $('#invoice_no').typeahead({
        source:  function (query, process) {
        return $.get(url, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
@endsection