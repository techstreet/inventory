@extends('layouts.website')
@section('content')
<?php
$from_date = '';
$to_date = '';
if($_POST && $_POST>0){
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
}
?>
<script>
$(function(){
$(".basic-select").select2();
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('.input-group.date').datepicker({
    calendarWeeks: true,
    todayHighlight: true,
    autoclose: true,
    format: "dd-MM-yyyy",
    //startDate: today
});  
});
function printPage(){
	window.print();
}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-file-text-o"></i>
        </div>
		<div class="media-body">
		<h4>Stock By Form</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Stock By Form</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
		<div class="col-md-12">
			<form class="form-inline" method="post" action="">
			{{ csrf_field() }}
				<div class="form-group" style="margin-right: 0">
					<div class="input-group">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="form" id="form" placeholder="Select Form">
						<option value="">Select Form</option>
						@foreach ($form as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $form_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
					</div>
					<div class="input-group date">
						<input type="text" name="from_date" class="form-control" value="{{$from_date}}" placeholder="From Date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="To Date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
				<button type="submit" name="submit" class="btn btn-info">Find</button>
			</form>
		</div>
	</div>
	<?php if($_POST && $_POST > 0){ ?>     
    <div class="row" style="margin-top: 10px">
	    <div class="col-md-12">
	    @include('flashmessage')
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Item</th>
		        <th>Open Stock</th>
		        <th>Purchase</th>
		        <th>Sale</th>
		        <th>Close Stock</th>
		        @if(Auth::user()->user_group !='3')
		        <th>Stock Value</th>
		        @endif
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($stockform as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->item_name }}</td>
	            <td>{{ $value->opening_stock }}</td>
	            <td>{{ $value->purchase_quantity }}</td>
	            <td>{{ $value->sale_quantity }}</td>
	            <td>{{ $value->closing_stock }}</td>
	            @if(Auth::user()->user_group !='3')
	            <td>{{number_format($value->closing_stock*$value->price, 2)}}</td>
	            @endif
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
@endsection