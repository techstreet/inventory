@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="media-body">
			<h4>Journal Voucher</h4>
			<ul class="breadcrumb">
				<li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
				<li>Accounts</li>
				<li><a href="{{ url('/journal-voucher') }}">Journal Voucher</a></li>
				<li>add</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			@include('flashmessage')
			<form method="post" action="{{ url('journal-voucher/save') }}">
				{{ csrf_field() }}
				<div class="col-md-9">
					<div class="panel panel-default">
						<div class="panel-body">
							<h5 style="position: absolute;top: -18px;font-weight: bold;background: #fff;padding: 0 5px;">General Info</h5>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Voucher No:</label>
										<input type="text" class="form-control" value="{{$voucher_no}}" disabled="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Voucher Date:</label>
										<div class="input-group date">
											<input type="text" name="voucher_date" id="voucher_date" class="form-control" placeholder="Enter Voucher Date" value="{{Carbon\Carbon::now()->format('d-F-Y')}}"><span
											class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Type:</label>
										<select class="form-control" name="type" id="type">
											<option>Select</option>
											<option value="debit">Debit</option>
											<option value="credit">Credit</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Account:</label>
										<select class="form-control" name="account" id="account">
											<option>Select</option>
											@foreach ($account as $value)
											@if (old('parent') == $value->id)
											<option value="{{ $value->id.'|'.$value->name }}" selected>{{ $value->name }}</option>
											@else
											<option value="{{ $value->id.'|'.$value->name }}">{{ $value->name }}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Amount:</label>
										<input type="number" name="amount" value="0" id="amount" min="0" step="0.01" class="form-control">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top:24px">
										<button onClick="addItems();" type="button" class="btn btn-default">Add</button>
									</div>
								</div>
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered" id="itemTable">
											<tbody class="field_wrapper">
												<tr>
													<th>Account</th>
													<th>Debit</th>
													<th>Credit</th>
													<th>Action</th>
												</tr>
											</tbody>
											<tbody>
												<th style="text-align:right">Total:</th>
												<th id="total_debit">0</th>
												<th id="total_credit">0</th>
												<th><input type="hidden" name="total" id="total" /></th>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Remarks:</label>
										<textarea class="form-control" placeholder="Enter Remarks" name="remarks" id="remarks"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
		<button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
		</form>
	</div>
</div>
<script>
	$(function () {
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.input-group.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
		var total_debit = $('#total_debit').text();
		var total_credit = $('#total_credit').text();

		if(total_debit == total_credit && total_debit > 0 && total_credit > 0){
			$("#submitBtn").prop('disabled', false);
		}
		else{
			$("#submitBtn").prop('disabled', true);
		}
		$('.field_wrapper').on('click', '.remove_button', function(e){
			e.preventDefault();
			$(this).closest("tr").remove();
			// Total Debit
			var total_debit = 0;
			$('.debitText').each(function () {
				total_debit = total_debit + parseFloat($(this).text());
			});
			$('#total_debit').text(total_debit);
			$('#total').val(total_debit);
			// Total Credit
			var total_credit = 0;
			$('.creditText').each(function () {
				total_credit = total_credit + parseFloat($(this).text());
			});
			$('#total_credit').text(total_credit);
			// Enable Disable Submit Button
			var total_debit = $('#total_debit').text();
			var total_credit = $('#total_credit').text();
			if(total_debit == total_credit && total_debit > 0 && total_credit > 0){
				$("#submitBtn").prop('disabled', false);
			}
			else{
				$("#submitBtn").prop('disabled', true);
			}
		});
	});
</script>
<script>
	var debit_count = 0;
	var credit_count = 0;
	function addItems(){
		var type = $('#type').val();
		var amount = $('#amount').val();
		var account = $('#account').val().split('|');
		var account_name = account[1];
		var account_id = account[0];

		if(type == 'debit'){
			debit_count = debit_count+1;
			var fieldHTML = '<tr>'+
			'<td>'+account_name+'<input type="hidden" name="account_id[]" value="'+account_id+'"/></td>'+
			'<td><span class="debitText">'+amount+'</span><input type="hidden" name="debit[]" value="'+amount+'"/></td>'+
			'<td><input type="hidden" name="credit[]" value="0"/></td>'+
			'<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td>'+
			'<tr>';
			if(debit_count > 1 && credit_count > 1){
				alert('Not allowed');
			}
			else{
				$('.field_wrapper').append(fieldHTML);
				var total_debit = 0;
				$('.debitText').each(function () {
					total_debit = total_debit + parseFloat($(this).text());
				});
				$('#total_debit').text(total_debit);
				$('#total').val(total_debit);
			}
		}
		if(type == 'credit'){
			credit_count = credit_count+1;
			var fieldHTML = '<tr>'+
			'<td>'+account_name+'<input type="hidden" name="account_id[]" value="'+account_id+'"/></td>'+
			'<td><input type="hidden" name="debit[]" value="0"/></td>'+
			'<td><span class="creditText">'+amount+'</span><input type="hidden" name="credit[]" value="'+amount+'"/></td>'+
			'<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td>'+
			'<tr>';
			if(debit_count > 1 && credit_count > 1){
				alert('Not allowed');
			}
			else{
				$('.field_wrapper').append(fieldHTML);
				var total_credit = 0;
				$('.creditText').each(function () {
					total_credit = total_credit + parseFloat($(this).text());
				});
				$('#total_credit').text(total_credit);
			}
		}
		var total_debit = $('#total_debit').text();
		var total_credit = $('#total_credit').text();

		if(total_debit == total_credit && total_debit > 0 && total_credit > 0){
			$("#submitBtn").prop('disabled', false);
		}
		else{
			$("#submitBtn").prop('disabled', true);
		}
	}
</script>
@endsection
