@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="row">
	    <div class="col-md-10 col-xs-10">
		<div class="media">
			<div class="pageicon pull-left">
				<i class="fa fa-home"></i>
			</div>
			<div class="media-body">
			<h4>Dashboard</h4>
			<ul class="breadcrumb">
				<li><a href="{{ url('') }}"><i class="glyphicon glyphicon-home"></i></a></li>
				<li>Dashboard</li>
			</ul>
			</div>
		</div><!-- media -->
		</div>
	    <div class="col-md-2 col-xs-2">
		@if(session('superadmin') == '1' && Auth::user()->user_group!='1')
			<div class="superadmin" style="float: right;margin-right: 2px;"><a class="btn btn-primary" href="{{url('/administrators/login/1')}}"><i class="fa fa-user"></i> Super Admin</a></div>
		@endif
		</div>
    
	</div>
</div><!-- pageheader -->
<div class="contentpanel">
<div id="page-wrapper">
    @if(Auth::user()->user_group=='1')
    <div class="panel panel-default" style="border:none">
        <div class="panel-body">
            <div class="pull-right">
                <select class="form-control" onchange="dashboardFilter(this.value)">
                    @foreach ($company as $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    @endif
    @if(Auth::user()->user_group == '1' || Auth::user()->user_group == '2' || Auth::user()->user_group == '4')
    <div class="row row-stat">
        <div class="col-md-4">
            <div class="panel panel-primary-head noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0px 0px 15px" class="fa fa-gift"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0px 0px 15px" class="fa fa-gift"></i></div>
                        <h1 class="md-title" style="font-size:20px">Sale</h1>
                        <!-- <h1 class="mt5">{{replace_blank($sale)}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin" id="sale_today">{{replace_blank($sale_today)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin" id="sale_thisweek">{{replace_blank($sale_thisweek)}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="">
                            <h5 class="md-title nomargin">This Month</h5>
                            <h4 class="nomargin" id="sale_thismonth">{{replace_blank($sale_thismonth)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-custom-head1 noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0 0 15px" class="fa fa-minus-circle"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0 0 15px" class="fa fa-minus-circle"></i></div>
                    <h1 class="md-title" style="font-size:20px">Sale Return</h1>
                        <!-- <h1 class="mt5">{{slash_decimal(replace_blank($salereturn))}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin" id="salereturn_today">{{slash_decimal(replace_blank($salereturn_today))}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin" id="salereturn_thisweek">{{slash_decimal(replace_blank($salereturn_thisweek))}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">This Month</h5>
                            <h4 class="nomargin" id="salereturn_thismonth">{{slash_decimal(replace_blank($salereturn_thismonth))}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-custom-head2 noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0 0 12px" class="fa fa-money"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0 0 12px" class="fa fa-money"></i></div>
                        <h1 class="md-title" style="font-size:20px">Cash Received</h1>
                        <!-- <h1 class="mt5">{{replace_blank($cash)}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin" id="cash_today">{{replace_blank($cash_today)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin" id="cash_thisweek">{{replace_blank($cash_thisweek)}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">This Month</h5>
                            <h4 class="nomargin" id="cash_thismonth">{{replace_blank($cash_thismonth)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-stat">
        <div class="col-md-4">
            <div class="panel panel-primary-head noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0px 0px 15px" class="fa fa-shopping-cart"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0px 0px 15px" class="fa fa-shopping-cart"></i></div>
                        <h1 class="md-title" style="font-size:20px">Purchase</h1>
                        <!-- <h1 class="mt5">{{replace_blank($purchase-$purchasereturn)}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <!-- <h4 class="nomargin">{{replace_blank($purchase_today-$purchasereturn_today)}}</h4> -->
                            <h4 class="nomargin" id="purchase_today">{{replace_blank($purchase_today)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <!-- <h4 class="nomargin">{{replace_blank($purchase_thisweek-$purchasereturn_thisweek)}}</h4> -->
                            <h4 class="nomargin" id="purchase_thisweek">{{replace_blank($purchase_thisweek)}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">This Month</h5>
                            <h4 class="nomargin" id="purchase_thismonth">{{replace_blank($purchase_thismonth)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-custom-head1 noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0 0 15px" class="fa fa-minus-circle"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0 0 15px" class="fa fa-minus-circle"></i></div>
                        <h1 class="md-title" style="font-size:20px">Purchase Return</h1>
                        <!-- <h1 class="mt5">{{slash_decimal(replace_blank($purchasereturn))}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin" id="purchasereturn_today">{{slash_decimal(replace_blank($purchasereturn_today))}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin" id="purchasereturn_thisweek">{{slash_decimal(replace_blank($purchasereturn_thisweek))}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">This Month</h5>
                            <h4 class="nomargin" id="purchasereturn_thismonth">{{replace_blank($purchasereturn_thismonth)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-custom-head2 noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0 0 12px" class="fa fa-money"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0 0 12px" class="fa fa-money"></i></div>
                        <h1 class="md-title" style="font-size:20px">Receivables</h1>
                        <!-- <h1 class="mt5">{{replace_blank_negative($credit)}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <!-- <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin">{{replace_blank_negative($credit_today)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin">{{replace_blank_negative($credit_thisweek)}}</h4>
                        </div> -->
                        <div class="pull-left">
                            <h5 class="md-title nomargin">0-30 Days</h5>
                            <h4 class="nomargin" id="credit_030">{{replace_blank_negative($credit_030)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">30-60 Days</h5>
                            <h4 class="nomargin" id="credit_3060">{{replace_blank_negative($credit_3060)}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">60-90 Days</h5>
                            <h4 class="nomargin" id="credit_6090">{{replace_blank_negative($credit_6090)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">90-120 Days</h5>
                            <h4 class="nomargin" id="credit_90120">{{replace_blank_negative($credit_90120)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="row row-stat">
        <div class="col-md-4">
            <div class="panel panel-primary-head noborder">
                <div class="panel-heading noborder">
                    <div class="panel-icon"><i style="padding: 12px 0px 0px 12px" class="fa fa-money"></i></div>
                    <div class="media-body">
											<!--Remaining Stock Value -->
                        <h5 class="md-title nomargin">COST of stock remaining</h5>
                        <h1 class="mt5" id="remaining_stock">{{replace_blank($remaining_stock)}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(Auth::user()->user_group == '2')
    <!--<div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
			    <div class="panel-heading">Latest Customers</div>
			    <div class="panel-body table-responsive">
			    	@if($latestclient_count>0)
			    	<table class="table table-bordered" style="border: 1px solid #ddd">
					    <thead>
					      <tr>
					        <th>Name</th>
					        <th>Address</th>
					        <th>Phone</th>
					        <th>Mobile</th>
					      </tr>
					    </thead>
    					<tbody>
    					  @foreach ($latest_client as $value)
					      <tr>
					        <td>{{$value->name}}</td>
					        <td>{{$value->address}}</td>
					        <td>{{$value->phone}}</td>
					        <td>{{$value->mobile}}</td>
					      </tr>
					      @endforeach
					    </tbody>
 					 </table>
 					 @else
 					 <p>No results found!</p>
 					 @endif
			    </div>
			</div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-info">
			    <div class="panel-heading">Latest Suppliers</div>
			    <div class="panel-body table-responsive">
			    	@if($latestvendors_count>0)
			    	<table class="table table-bordered" style="border: 1px solid #ddd">
					    <thead>
					      <tr>
					        <th>Name</th>
					        <th>Address</th>
					        <th>Phone</th>
					        <th>Mobile</th>
					      </tr>
					    </thead>
    					<tbody>
    					  @foreach ($latest_vendors as $value)
					      <tr>
					        <td>{{$value->name}}</td>
					        <td>{{$value->address}}</td>
					        <td>{{$value->phone}}</td>
					        <td>{{$value->mobile}}</td>
					      </tr>
					      @endforeach
					    </tbody>
 					 </table>
 					 @else
 					 <p>No results found!</p>
 					 @endif
			    </div>
			</div>
        </div>
    </div>-->
	@endif
	@if(Auth::user()->user_group == '3')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary-head noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0px 0px 15px" class="fa fa-gift"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0px 0px 15px" class="fa fa-gift"></i></div>
                        <h1 class="md-title" style="font-size:20px">Sale</h1>
                        <!-- <h1 class="mt5">{{replace_blank($sale)}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin">{{replace_blank($sale_today)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin">{{replace_blank($sale_thisweek)}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="">
                            <h5 class="md-title nomargin">This Month</h5>
                            <h4 class="nomargin">{{replace_blank($sale_thismonth)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-custom-head1 noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0 0 15px" class="fa fa-minus-circle"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0 0 15px" class="fa fa-minus-circle"></i></div>
                    <h1 class="md-title" style="font-size:20px">Sale Return</h1>
                        <!-- <h1 class="mt5">{{slash_decimal(replace_blank($salereturn))}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin">{{slash_decimal(replace_blank($salereturn_today))}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin">{{slash_decimal(replace_blank($salereturn_thisweek))}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">This Month</h5>
                            <h4 class="nomargin">{{slash_decimal(replace_blank($salereturn_thismonth))}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-stat">
    	<div class="col-md-6">
        <div class="panel panel-custom-head2 noborder">
            <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0 0 12px" class="fa fa-money"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0 0 12px" class="fa fa-money"></i></div>
                        <h1 class="md-title" style="font-size:20px">Cash Received</h1>
                        <!-- <h1 class="mt5">{{replace_blank($cash)}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin">{{replace_blank($cash_today)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin">{{replace_blank($cash_thisweek)}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">This Month</h5>
                            <h4 class="nomargin">{{replace_blank($cash_thismonth)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    	<div class="col-md-6">
            <div class="panel panel-custom-head2 noborder">
                <div class="panel-heading noborder">
                    <!-- <div class="panel-icon"><i style="padding: 12px 0 0 12px" class="fa fa-money"></i></div> -->
                    <div class="media-body">
                    <div class="panel-icon"><i style="padding: 12px 0 0 12px" class="fa fa-money"></i></div>
                        <h1 class="md-title" style="font-size:20px">Receivables</h1>
                        <!-- <h1 class="mt5">{{replace_blank_negative($credit)}}</h1> -->
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <!-- <div class="pull-left">
                            <h5 class="md-title nomargin">Today</h5>
                            <h4 class="nomargin">{{replace_blank_negative($credit_today)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">This Week</h5>
                            <h4 class="nomargin">{{replace_blank_negative($credit_thisweek)}}</h4>
                        </div> -->
                        <div class="pull-left">
                            <h5 class="md-title nomargin">0-30 Days</h5>
                            <h4 class="nomargin">{{replace_blank_negative($credit_030)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">30-60 Days</h5>
                            <h4 class="nomargin">{{replace_blank_negative($credit_3060)}}</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix mt20">
                        <div class="pull-left">
                            <h5 class="md-title nomargin">60-90 Days</h5>
                            <h4 class="nomargin">{{replace_blank_negative($credit_6090)}}</h4>
                        </div>
                        <div class="pull-right">
                            <h5 class="md-title nomargin">90-120 Days</h5>
                            <h4 class="nomargin">{{replace_blank_negative($credit_90120)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="row row-stat">
        <div class="col-md-6">
        <div class="panel panel-primary-head noborder">
            <div class="panel-heading noborder">
                    <div class="panel-icon"><i style="padding: 12px 0px 0px 12px" class="fa fa-money"></i></div>
                    <div class="media-body">
											<!--Remaining Stock Value -->
                        <h5 class="md-title nomargin">COST of stock remaining</h5>
                        <h1 class="mt5">{{replace_blank($remaining_stock)}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
	@endif
</div>
</div>
<script>
    function dashboardFilter(val){
        $.ajax({
        type: "GET",
        url: "{{url('/dashboard-filter')}}",
        data:'company_id='+val,
        beforeSend: function() {    
            $("#loading").show();
        },
        success: function(data){
            $("#loading").hide();
            var obj = JSON.parse(data);

            var company_name = obj[0].company_name;

			var sale_today = obj[0].sale_today;
            var sale_thisweek = obj[0].sale_thisweek;
            var sale_thismonth = obj[0].sale_thismonth;

            var salereturn_today = obj[0].salereturn_today;
            var salereturn_thisweek = obj[0].salereturn_thisweek;
            var salereturn_thismonth = obj[0].salereturn_thismonth;

            var cash_today = obj[0].cash_today;
            var cash_thisweek = obj[0].cash_thisweek;
            var cash_thismonth = obj[0].cash_thismonth;

            var purchase_today = obj[0].purchase_today;
            var purchase_thisweek = obj[0].purchase_thisweek;
            var purchase_thismonth = obj[0].purchase_thismonth;

            var purchasereturn_today = obj[0].purchasereturn_today;
            var purchasereturn_thisweek = obj[0].purchasereturn_thisweek;
            var purchasereturn_thismonth = obj[0].purchasereturn_thismonth;

            var credit_030 = obj[0].credit_030;
            var credit_3060 = obj[0].credit_3060;
            var credit_6090 = obj[0].credit_6090;
            var credit_90120 = obj[0].credit_90120;

            var remaining_stock = obj[0].remaining_stock;

            $("#company_name").html(company_name);

            $("#sale_today").html(sale_today);
            $("#sale_thisweek").html(sale_thisweek);
            $("#sale_thismonth").html(sale_thismonth);

            $("#salereturn_today").html(salereturn_today);
            $("#salereturn_thisweek").html(salereturn_thisweek);
            $("#salereturn_thismonth").html(salereturn_thismonth);

            $("#cash_today").html(cash_today);
            $("#cash_thisweek").html(cash_thisweek);
            $("#cash_thismonth").html(cash_thismonth);

            $("#purchase_today").html(purchase_today);
            $("#purchase_thisweek").html(purchase_thisweek);
            $("#purchase_thismonth").html(purchase_thismonth);

            $("#purchasereturn_today").html(purchasereturn_today);
            $("#purchasereturn_thisweek").html(purchasereturn_thisweek);
            $("#purchasereturn_thismonth").html(purchasereturn_thismonth);

            $("#credit_030").html(credit_030);
            $("#credit_3060").html(credit_3060);
            $("#credit_6090").html(credit_6090);
            $("#credit_90120").html(credit_90120);
            $("#remaining_stock").html(remaining_stock);
        }
        });
    }
</script>
@endsection