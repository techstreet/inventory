@extends('layouts.website')
@section('content')
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>Manufacturers</h4>
        <ul class="breadcrumb">
            <li><a href="{{url('')}}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Masters</li>
            <li><a href="{{url('/manufacturer')}}">Manufacturers</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		@include('flashmessage')
      	<form method="post" action="{{ url('/manufacturer/update/'.$manufacturer[0]->id) }}">
      	{{ csrf_field() }}
		  <div class="form-group">
		    <label>*Name:</label>
		    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="<?php echo $manufacturer[0]->name; ?>" required="">
		  </div>
		  <div class="form-group">
		      <label>*Status:</label>
		      <select class="form-control" name="status" id="status">
		      		<option value="1" @if($manufacturer[0]->is_active=='1') selected="" @endif>Active</option>
		      		<option value="0" @if($manufacturer[0]->is_active=='0') selected="" @endif>Inactive</option>
		      </select>
		  </div>
		  <button type="submit" class="btn btn-primary">Update</button>
		</form>
		</div>
	</div>
</div>
</div>
@endsection