@extends('layouts.website') @section('content')
<?php
$from_date = '';
$to_date = '';
if($_POST && $_POST>0){
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
}
?>
	<script>

		function printPage() {
			document.title = "Individual Sales Report";
			window.print();
		}

		$(function () {
			$(".basic-select").select2();
			var subtotal = 0;
			var discount = 0;
			var tax = 0;
			var qty = 0;
			var rate = 0;
			var total = 0;
			$('.subtotal').each(function(){
				subtotal += parseFloat($(this).text());
			});
			$('.discount').each(function(){
				discount += parseFloat($(this).text());
			});
			$('.tax').each(function(){
				tax += parseFloat($(this).text());
			});
			$('.qty').each(function(){
				qty += parseFloat($(this).text());
			});
			$('.rate').each(function(){
				rate += parseFloat($(this).text());
			});
			//var avg_rate = rate/{{$count}}.toFixed(2);
			var avg_rate = rate/{{$count}};
			$('.total').each(function(){
				total += parseFloat($(this).text());
			});
			$('.subtotal_sum').text(subtotal);
			$('.discount_sum').text(discount);
			$('.tax_sum').text(tax);
			$('.total_qty').text(qty);
			$('.avg_rate').text(avg_rate.toFixed(2));
			$('.total_sum').text(total);
			var nowDate = new Date();
			var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
			$('.input-group.date').datepicker({
				calendarWeeks: true,
				todayHighlight: true,
				autoclose: true,
				format: "dd-MM-yyyy",
				//startDate: today
			});
		});
	</script>
	<div class="pageheader">
		<div class="media">
			<div class="pull-right">
				<button onclick="printPage();" class="btn btn-primary">
					<i class="fa fa-print"></i> Print</button>
			</div>
			<div class="pageicon pull-left">
				<i class="fa fa-file-text-o"></i>
			</div>
			<div class="media-body">
				<h4>Individual Sales Report</h4>
				<ul class="breadcrumb">
					<li>
						<a href="{{ url('/') }}">
							<i class="glyphicon glyphicon-home"></i>
						</a>
					</li>
					<li>Reports</li>
					<li>Individual Sales Report</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="contentpanel">
		<div id="page-wrapper">
			<div class="row">
				<div class="col-md-12">
					<form class="form-inline" method="post" action="">
						{{ csrf_field() }}
						<div class="form-group" style="margin-right: 0">
							<div class="input-group">
							<select style="border: none; padding: 0px; min-width:200px" class="form-control basic-select" name="item" id="item">
							<option value="">Select</option>
									@foreach ($items as $value)
									<option value="{{ $value->id }}" <?php if($item_selected==$value->id ){echo "selected";} ?>>{{ $value->name }}</option>
									@endforeach
							</select>
							</div>
							<div class="input-group date">
								<input type="text" name="from_date" class="form-control" value="{{$from_date}}" placeholder="From Date" required="">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
							</div>
							<div class="input-group date">
								<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="To Date" required="">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
							</div>
						</div>
						<button type="submit" name="submit" class="btn btn-info">Find</button>
					</form>
				</div>
			</div>
			<?php if($_POST && $_POST > 0){ ?>
			<div class="row" style="margin-top: 10px">
				<div class="col-md-12">
					@include('flashmessage')
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable">
							<thead>
								<tr>
									<th width="50px">S.No</th>
									<th>Date</th>
									<th>Invoice No</th>
									<th>Customer Name</th>
									<th>Sale Type</th>
									<th>Item</th>
									<th>Quantity</th>
									<th>Cost</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=1; if($count>0){ ?>
									<?php foreach ($salesreport as $value){ ?>
										<tr>
											<td>{{ $i }}</td>
											<td>{{ date_dfy($value->entry_date) }}</td>
											<td><a target="_black" href="{{url('/saleregister/view')}}<?php echo '/'.$value->id; ?>">{{ $value->invoice_no }}</a></td>
											
											<td>{{ $value->client_name }}</td>
											<td>{{ $value->sale_type }}</td>
											<td>{{ $value->item_name }}</td>
											<td class="qty">{{ $value->quantity }}</td>
											<td class="rate">{{ $value->rate }}</td>
											<td class="total">{{ $value->amount }}</td>
										</tr>
									<?php $i++; } ?>
									<tr style="font-weight:bold">
										<td colspan="7" style="text-align:right">Total Quantity: <span class="total_qty">0</span></td>
										<td>Average Cost: <span class="avg_rate">0</span></td>
										<td>Total Amount: <span class="total_sum">0</span></td>
									</tr>
								<?php } else{ ?>
									<tr>
										<td colspan="9">There is no Record Found.</td>
									</tr>	
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	@endsection