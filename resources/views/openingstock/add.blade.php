@extends('layouts.website')
@section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$on_date = date("d-F-Y");
?>
<script>
function getCategory(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax')}}",
	data:'company_id='+val,
	success: function(data){
		$("#category").html(data);
		$("#item").html('<option value="">Select</option>');
	}
	});
}
function getItems(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax2')}}",
	data:'category_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}
function getBrands(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax4')}}",
	data:'item_name='+val,
	success: function(data){
		$("#brand").html(data);
	}
	});
}
function getItemsbyBrand(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax5')}}",
	data:'brand_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}

</script>
<script type='text/javascript'>
$(function(){
$(".basic-select").select2();
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
$('.input-group.date').datepicker({
    calendarWeeks: true,
    todayHighlight: true,
    autoclose: true,
    format: "dd-MM-yyyy",
    //startDate: today
});  
});
</script>
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>Opening Stocks</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Masters</li>
            <li><a href="{{ url('/openingstock') }}">Opening Stocks</a></li>
            <li>Add</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		@include('flashmessage')
      	<form method="post" action="{{ url('openingstock/save') }}">
      	{{ csrf_field() }}
		  <div class="form-group">
		      <label>*Category:</label>
		      <select class="form-control" name="category" id="category" required="">
		      		<option value="">Select</option>
		      		@foreach ($category as $value)
		      		@if (old('category') == $value->id)
					      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
					@else
					      <option value="{{ $value->id }}">{{ $value->name }}</option>
					@endif
		      		@endforeach
		      </select>
		  </div>
		  <div class="form-group">
		      <label>*Brand:</label>
		      <select class="form-control" name="brand" id="brand" required="" onChange="getItemsbyBrand(this.value);">
		      		<option value="">Select</option>
					 @foreach ($brand as $value)
		      		@if (old('brand') == $value->id)
					      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
					@else
					      <option value="{{ $value->id }}">{{ $value->name }}</option>
					@endif
		      		@endforeach
		      </select>
		  </div>
		  <div class="form-group">
		      <label>*Item:</label>
		      <select style="border: none; padding: 0px" class="form-control basic-select" name="item" id="item" required="">
		      		<option value="">Select</option>
		      </select>
		  </div>
		  <!-- <div class="form-group">
		      <label>Brand:</label>
			  <div id="brand">
			  <input type="text" class="form-control" disabled="">
			  </div>
		  </div> -->
		  <div class="form-group">
		    <label>Barcode:</label>
		    <input type="text" class="form-control" placeholder="Enter barcode" name="barcode" id="barcode" value="{{ old('barcode') }}">
		  </div>
		  <div class="form-group">
		      <label>*Expiry Date:</label>
			  <div class="input-group date">
				<input type="text" name="expiry_date" id="expiry_date" class="form-control" placeholder="Enter expiry date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			  </div>
		  </div>
		  <div class="form-group">
		    <label>*Quantity:</label>
		    <input type="number" class="form-control" placeholder="Enter quantity" name="quantity" id="quantity" value="{{ old('quantity') }}" min="1" required="">
		  </div>
		  <div class="form-group">
		    <label>*Cost:</label>
		    <input type="number" class="form-control" placeholder="Enter cost" name="rate" id="rate" value="{{ old('rate') }}" min="0" step="0.01" required="">
		  </div>
		  <div class="form-group">
		      <label>*Date:</label>
			  <div class="input-group date">
				<input type="text" name="on_date" id="on_date" value="{{$on_date}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			  </div>
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
      </div>
	</div>
</div>
</div>
@endsection