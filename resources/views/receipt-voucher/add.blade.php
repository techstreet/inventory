@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="media-body">
			<h4>Receipt Voucher</h4>
			<ul class="breadcrumb">
				<li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
				<li>Accounts</li>
				<li><a href="{{ url('/receipt-voucher') }}">Receipt Voucher</a></li>
				<li>add</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			@include('flashmessage')
			<form method="post" action="{{ url('receipt-voucher/save') }}">
				{{ csrf_field() }}
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h5 style="position: absolute;top: -18px;font-weight: bold;background: #fff;padding: 0 5px;">General Info</h5>
							<div class="form-group">
								<label>Voucher No:</label>
								<input type="text" class="form-control" value="{{$voucher_no}}" disabled="">
							</div>
							<div class="form-group">
								<label>Voucher Date:</label>
								<div class="input-group date">
									<input type="text" name="voucher_date" id="voucher_date" class="form-control" placeholder="Enter Voucher Date" value="{{Carbon\Carbon::now()->format('d-F-Y')}}"><span
									 class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label>Paid To:</label>
								<select class="form-control" name="account" id="account">
									@foreach ($account as $value)
									@if (old('parent') == $value->id)
									<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
									@else
									<option value="{{ $value->id }}">{{ $value->name }}</option>
									@endif
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Amount:</label>
								<input type="number" class="form-control" min="0" step="0.01" placeholder="Enter amount" name="amount" id="amount"
								 value="{{ old('amount') }}" required="">
							</div>
							<div class="form-group">
								<label>Remarks:</label>
								<textarea class="form-control" placeholder="Enter Remarks" name="remarks" id="remarks"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h5 style="position: absolute;top: -18px;font-weight: bold;background: #fff;padding: 0 5px;">Payment Info</h5>
							<div class="form-group">
								<label>Payment Mode:</label>
								<select class="form-control" name="mode" id="mode" onchange="receiptMode(this.value);">
									<option value="Cash">Cash</option>
									<option value="Cheque">Cheque</option>
								</select>
							</div>
							<div class="form-group" id="chequeNo_section" style="display: none">
								<label>Cheque No:</label>
								<input type="text" class="form-control" placeholder="Enter Cheque No" name="cheque_no" id="cheque_no">
							</div>
							<div class="form-group" id="chequeDate_section" style="display: none">
								<label>Cheque Date:</label>
								<div class="input-group date">
									<input type="text" name="cheque_date" id="cheque_date" class="form-control" placeholder="Enter Cheque Date"><span
									 class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="form-group" id="bankName_section" style="display: none">
								<label>Bank Name:</label>
								<input type="text" class="form-control" placeholder="Enter Bank Name" name="bank_name" id="bank_name">
							</div>
						</div>
					</div>
				</div>
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
</div>
<script>
	$(function () {
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.input-group.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
	});
	function receiptMode(mode) {
		if (mode == 'Cheque') {
			$('#chequeNo_section').show();
			$('#chequeDate_section').show();
			$('#bankName_section').show();
		}
		else {
			$('#chequeNo_section').hide();
			$('#chequeDate_section').hide();
			$('#bankName_section').hide();
		}
	}
</script>
@endsection
