@extends('layouts.website') @section('content')
<?php
use Carbon\Carbon;
$to_date = '';
if($_POST && $_POST>0){
	$to_date = $_POST['to_date'];
}
?>
<script>
	$(function () {

		var debit_total = 0;
		var credit_total = 0;

		$('.debit-total').each(function(){
			var amount = $(this).text();
			if(amount == ''){
				amount = 0;
			}
			debit_total += parseFloat(amount);
		});
		$('.credit-total').each(function(){
			var amount = $(this).text();
			if(amount == ''){
				amount = 0;
			}
			credit_total += parseFloat(amount);
		});

		$('.debit-total-sum').text(debit_total);
		$('.credit-total-sum').text(credit_total);

		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.input-group.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
	});
	function printPage() {
		document.title = "Trial Balance";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-file-text-o"></i>
		</div>
		<div class="media-body">
			<h4>Trial Balance</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>Account Register</li>
				<li>Trial Balance</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12">
			<h4 class="center onlyprint"><u>{{Auth::user()->name}}</u></h4>
			<h5 class="center onlyprint">{{Auth::user()->address}}</h4>
				@include('flashmessage')
				<form class="form-inline" method="post" action="">
					{{ csrf_field() }}
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="Enter Date" required="" autocomplete="off">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
					</div>
					<button type="submit" name="submit" class="btn btn-info">Find</button>
				</form>
			</div>
		</div>
		<?php if($_POST && $_POST > 0){ ?>
		<div class="row" style="margin-top: 10px">
			<div class="col-md-6">
				@include('flashmessage')
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable">
						<thead>
							<tr>
								<th colspan="3">Trial Balance as of {{ $to_date }}</th>
							</tr>
							<tr>
								<th>Account</th>
								<th>Debit</th>
								<th>Credit</th>
							</tr>
						</thead>
						<tbody>
							@foreach($accounts as $key=>$value)
							<?php
							$account_id = $value->id;
							$company = $value->company_id;
							$data = DB::select("SELECT account.name,sum(account_txn.`debit`) as debit,sum(account_txn.`credit`) as credit,sum(account_txn.`debit`-account_txn.`credit`) as balance FROM `account_txn` LEFT JOIN `account` ON account_txn.account_id = account.id WHERE account_txn.`voucher_no` IN (SELECT `voucher_no` FROM `account_txn` WHERE `account_id` = '$account_id') AND account_txn.`account_id` <> '$account_id' AND account_txn.`company_id` = '$company' AND account_txn.`status` = '1' AND account_txn.`created_at` <= '$to'");
							if($data[0]->balance == 0){
								continue;
							}
							?>
							<tr>
								<td>{{ $value->name }}</td>
								<td class="debit-total">@if($data[0]->balance > 0){{$data[0]->balance}} @else 0 @endif</td>
								<td class="credit-total">@if($data[0]->balance < 0){{$data[0]->balance*-1}} @else 0 @endif</td>
							</tr>
							@endforeach
							<!-- Total -->
							<tr style="background: beige">
								<th style="text-align:right">Total: </th>
								<th class="debit-total-sum">0</th>
								<th class="credit-total-sum">0</th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
@endsection