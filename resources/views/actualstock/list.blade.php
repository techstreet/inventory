@extends('layouts.website')
@section('content')
<?php
$current_date = '';
if($_POST && $_POST>0){
	$current_date = $_POST['current_date'];
}
?>
<script>
$(function(){
	$(".basic-select").select2();
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
	$('.input-group.date').datepicker({
		calendarWeeks: true,
		todayHighlight: true,
		autoclose: true,
		format: "dd-MM-yyyy",
		//startDate: today
	});
});
function printPage() {
    document.title = "Actual Stocks";
    window.print();
}
</script>
<div class="pageheader">
	<div class="media">
		<!-- <div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		</div> -->
		<div class="pageicon pull-left">
            <i class="fa fa-file-text-o"></i>
        </div>
		<div class="media-body">
		<h4>Actual Stocks</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Actual Stocks</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	@include('flashmessage')
	<form class="form-inline" method="post" action="">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-12">
				<div class="input-group">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="category" id="category" placeholder="Select Category">
						<option value="">Select Category</option>
						@foreach ($category as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $category_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
				</div>
				<div class="input-group" style="display: none">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="form" id="form" placeholder="Select Form">
						<option value="">Select Form</option>
						@foreach ($form as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $form_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
				</div>
				<div class="input-group" style="display: none">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="manufacturer" id="manufacturer" placeholder="Select Manufacturer">
						<option value="">Select Manufacturer</option>
						@foreach ($manufacturer as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $manufacturer_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
				</div>
				<div class="input-group">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="brand" id="brand" placeholder="Select Brand">
						<option value="">Select Brand</option>
						@foreach ($brand as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $brand_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
				</div>
				<div class="input-group date">
					<input type="text" name="current_date" class="form-control" value="{{$current_date}}" placeholder="Enter Date" required>
					<span class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</span>
				</div>
				<button type="submit" name="submit" class="btn btn-info">Find</button>
			</div>
		</div>
	</form>
	<?php if($_POST && $_POST > 0){ ?>     
    <div class="row" style="margin-top: 10px">
	    <div class="col-md-12">
		<div class="table-responsive">
		<form class="form-inline" method="post" action="{{ url('/actualstock/save/') }}">
		{{ csrf_field() }}
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Item</th>
				<th>Category</th>
				<th>Brand</th>
		        <th>Form</th>
		        <th>Closing Stock</th>
		        @if(Auth::user()->user_group !='3')
		        <th>Actual Stock</th>
		        @endif
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; 
								   
		   	if($count>0){

				foreach ($actualstock as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->name }}</td>
				<td>{{ $value->category_name }}</td>
				<td>{{ $value->brand_name }}</td>
	            <td>{{$value->form_name}}</td>
	            <td>{{ $value->closing_stock }}</td>
				<input type="hidden" name="item_id[]" value="{{ $value->id }}">
				<input type="hidden" name="company_id[]" value="{{ $value->company_id }}">
				<input type="hidden" name="closing_stock[]" value="{{ $value->closing_stock }}">
				<input type="hidden" name="entry_date[]" value="{{ date_ymd($current_date) }}">
				@if(getActualStock($value->id,$current_date) == '0')
				<td><input type="number" name="actual_stock[]" value="{{ $value->closing_stock }}" class="form-control"></td>
				@else
				<td><input type="number" name="actual_stock[]" value="{{ getActualStock($value->id,$current_date) }}" class="form-control"></td>
				@endif
	          </tr>
	          <?php $i++; }
			}
		   	else{
				?>
	          <tr>
	            <td colspan="11">There is no Record Found.</td>
	          </tr>				
				<?php
				}?>
		    </tbody>
		  </table>
		  <button type="submit" class="btn btn-primary">Submit</button>
		  </form>
		</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
@endsection