@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<a href="{{ url('/account/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add Account</button></a>
		</div>
		<div class="media-body">
		<h4>Accounts</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Account Register</li>
		    <li>Accounts</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
						<th>Name</th>
						<th>Account Group</th>
						<th>Financial Group</th>
						<th>Account Type</th>
						<th>Restricted Account</th>
		        @if(session('superadmin') == '1')
		        <th width="100px" style="min-width: 100px;">Action</th>
		        @endif
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($account as $value){ ?>
	          <tr>
							<td>{{ $i }}</td>
							<td>{{ $value->name }}</td>
							<td>{{ $value->account_group_name }}</td>
							<td>{{ $value->financial_group_name }}</td>
							<td>{{ $value->type }}</td>
							<td>
								@if($value->restricted == '1')
								<span class="label label-danger">Yes</span>
								@else
								<span class="label label-success">No</span>
								@endif
							</td>
	            @if(session('superadmin') == '1')
	            <td>
	            <a href="{{ url('/account/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
	            <a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/account/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
	            </td>
	            @endif
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    });
});
</script>
@endsection