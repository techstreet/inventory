@extends('layouts.website') @section('content')
<div class="pageheader">
	<div class="media">
		<div class="media-body">
			<h4>Account</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>Account Register</li>
				<li>
					<a href="{{ url('/account') }}">Accounts</a>
				</li>
				<li>add</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			@include('flashmessage')
			<form method="post" action="{{ url('account/save') }}">
				{{ csrf_field() }}
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h5 style="position: absolute;top: -18px;font-weight: bold;background: #fff;padding: 0 5px;">General Info</h5>
							<div class="form-group">
								<label>Parent:</label>
								<select class="form-control" name="parent" id="parent">
									<option value="0">None</option>
									@foreach ($parent as $value) @if (old('parent') == $value->id)
									<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
									@else
									<option value="{{ $value->id }}">{{ $value->name }}</option>
									@endif @endforeach
								</select>
							</div>
							<div class="form-group">
								<label>*Name:</label>
								<input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="{{ old('name') }}"
								 required="">
							</div>
							<div class="form-group">
								<label>Account Group:</label>
								<select class="form-control" name="account_group_id" id="account_group_id" required>
									@foreach ($account_group as $value) @if (old('account_group_id') == $value->id)
									<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
									@else
									<option value="{{ $value->id }}">{{ $value->name }}</option>
									@endif @endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Financial Group:</label>
								<select class="form-control" name="financial_group_id" id="financial_group_id" required>
									@foreach ($financial_group as $value) @if (old('financial_group_id') == $value->id)
									<option value="{{ $value->id }}">{{ $value->name }}</option>
									@else
									<option value="{{ $value->id }}" <?php if($value->name == 'Uncategorized'){ echo "selected";} ?>>{{ $value->name }}</option>
									@endif @endforeach
								</select>
							</div>
							<div class="form-group">
								<label>*Account Type:</label>
								<select class="form-control" name="type" id="type" required>
									<option value="DR">DR</option>
									<option value="CR">CR</option>
								</select>
							</div>
							<div class="form-group">
								<label>Opening Balance:</label>
								<input type="number" step="0.01" min="0" class="form-control" placeholder="Enter opening balance" name="opening_balance"
								 id="opening_balance" value="0.00">
							</div>
							<div class="form-group">
								<input style="margin-top: -3px;vertical-align: middle;" type="checkbox" name="restricted" id="restricted" value="1">
								Restricted Account
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<h5 style="position: absolute;top: -18px;font-weight: bold;background: #fff;padding: 0 5px;">Official Info</h5>
							<div class="form-group">
								<label>GST Number:</label>
								<input type="text" class="form-control" placeholder="Enter GST Number" name="gst_no" id="gst_no" value="{{ old('gst_no') }}">
							</div>
							<div class="form-group">
								<label>PAN/TAN/Tax ID:</label>
								<input type="text" class="form-control" placeholder="Enter PAN/TAN/Tax ID" name="taxid_no" id="taxid_no" value="{{ old('taxid_no') }}">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h5 style="position: absolute;top: -18px;font-weight: bold;background: #fff;padding: 0 5px;">Contact Info</h5>
							<div class="form-group">
								<label>Phone:</label>
								<input type="text" class="form-control" placeholder="Enter phone" name="phone" id="phone" value="{{ old('phone') }}">
							</div>
							<div class="form-group">
								<label>Email:</label>
								<input type="email" class="form-control" placeholder="Enter email" name="email" id="email" value="{{ old('email') }}">
							</div>
							<div class="form-group">
								<label>City:</label>
								<input type="text" class="form-control" placeholder="Enter city" name="city" id="city" value="{{ old('city') }}">
							</div>
							<div class="form-group">
								<label>State:</label>
								<input type="text" class="form-control" placeholder="Enter state" name="state" id="state" value="{{ old('state') }}">
							</div>
							<div class="form-group">
								<label>Country:</label>
								<input type="text" class="form-control" placeholder="Enter country" name="country" id="country" value="{{ old('country') }}">
							</div>
							<div class="form-group">
								<label>Address:</label>
								<textarea class="form-control" placeholder="Enter address" name="address" id="address">{{ old('address') }}</textarea>
							</div>
							<div style="text-align: right" id="address_characters"></div>
						</div>
					</div>
				</div>
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
</div>
<script>
	$(document).ready(function () {
		var text_max = 400;
		var text_length = $('#address').val().length;
		var text_remaining = text_max - text_length;
		$('#address_characters').html(text_remaining + ' characters left');

		$('#address').keyup(function () {
			var text_length = $('#address').val().length;
			var text_remaining = text_max - text_length;

			$('#address_characters').html(text_remaining + ' characters left');
		});
	});
</script>
@endsection
