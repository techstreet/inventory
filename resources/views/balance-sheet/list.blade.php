@extends('layouts.website') @section('content')
<?php
use Carbon\Carbon;
$to_date = '';
if($_POST && $_POST>0){
	$to_date = $_POST['to_date'];
}
?>
<script>
	$(function () {

		var liabilities_total = 0;
		var assets_total = 0;

		$('.liabilities-total').each(function(){
			var amount = $(this).text();
			if(amount == ''){
				amount = 0;
			}
			liabilities_total += parseFloat(amount);
		});
		$('.assets-total').each(function(){
			var amount = $(this).text();
			if(amount == ''){
				amount = 0;
			}
			assets_total += parseFloat(amount);
		});

		$('.liabilities-total-sum').text(liabilities_total);
		$('.assets-total-sum').text(assets_total);

		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.input-group.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
	});
	function printPage() {
		document.title = "Balance Sheet";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-file-text-o"></i>
		</div>
		<div class="media-body">
			<h4>Balance Sheet</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>Account Register</li>
				<li>Balance Sheet</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12">
			<h4 class="center onlyprint"><u>{{Auth::user()->name}}</u></h4>
			<h5 class="center onlyprint">{{Auth::user()->address}}</h4>
				@include('flashmessage')
				<form class="form-inline" method="post" action="">
					{{ csrf_field() }}
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="Enter Date" required="" autocomplete="off">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
					</div>
					<button type="submit" name="submit" class="btn btn-info">Find</button>
				</form>
			</div>
		</div>
		<?php if($_POST && $_POST > 0){ ?>
		<div class="row" style="margin-top: 10px">
			<div class="col-md-8">
				@include('flashmessage')
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable">
						<thead>
							<tr>
								<th colspan="4">Balance Sheet as of {{ $to_date }}</th>
							</tr>
							<tr>
								<th>Liabilities</th>
								<th>Amount</th>
								<th>Assets</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								@for ($i = 0; $i < $count; $i++)
								<tr>
									<td>@if(isset($liabilities[$i]->name)) {{$liabilities[$i]->name}} @endif</td>
									<td class="liabilities-total">@if(isset($liabilities[$i]->balance)) {{$liabilities[$i]->balance}} @endif</td>
									<td>@if(isset($assets[$i]->name)) {{$assets[$i]->name}} @endif</td>
									<td class="assets-total">@if(isset($assets[$i]->balance)) {{$assets[$i]->balance}} @endif</td>
								</tr>
								@endfor
							</tr>
							<!-- Total -->
							<tr style="background: beige">
								<th style="text-align:right">Total: </th>
								<th class="liabilities-total-sum">0</th>
								<th style="text-align:right">Total: </th>
								<th class="assets-total-sum">0</th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
@endsection