@extends('layouts.website')
@section('content')
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>Financial Group</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Account Register</li>
            <li><a href="{{ url('/financial-group') }}">Financial Group</a></li>
            <li>add</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		@include('flashmessage')
      	<form method="post" action="{{ url('financial-group/save') }}">
      	{{ csrf_field() }}
		  <div class="form-group">
		      <label>Parent:</label>
		      <select class="form-control" name="parent" id="parent">
		      		<option value="0">None</option>
		      		@foreach ($parent as $value)
		      		@if (old('parent') == $value->id)
					      <option value="{{ $value->id }}" selected>{{ $value->slug }}</option>
					@else
					      <option value="{{ $value->id }}">{{ $value->slug }}</option>
					@endif
		      		@endforeach
		      </select>
		  </div>
		  <div class="form-group">
		    <label>*Name:</label>
		    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="{{ old('name') }}" required="">
		  </div>
		  <div class="form-group">
		    <label>Account Type:</label>
			<select class="form-control" name="type" id="type">
				<option value="DR">DR</option>
				<option value="CR">CR</option>
		    </select>
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
      </div>
	</div>
</div>
</div>
@endsection