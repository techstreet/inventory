@extends('layouts.website')
@section('content')
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>Update Financial Group</h4>
        <ul class="breadcrumb">
            <li><a href="{{url('')}}"><i class="glyphicon glyphicon-home"></i></a></li>
			<li>Account Register</li>
            <li><a href="{{url('/financial-group')}}">Financial Group</a></li>
            <li>edit</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		@include('flashmessage')
      	<form method="post" action="{{ url('/financial-group/update/'.$financialgroup[0]->id) }}">
      	{{ csrf_field() }}
      	  <div class="form-group">
		      <label>Parent:</label>
		      <select class="form-control" name="parent" id="parent">
		      		<option value="0">None</option>
		      		@foreach ($parent as $value)
		      		<option value="<?php echo $value->id; ?>" <?php if($financialgroup[0]->parent_id==$value->id){ ?> selected="" <?php }?>><?php echo $value->slug; ?></option>
		      		@endforeach
		      </select>
		  </div>
		  <div class="form-group">
		    <label>*Name:</label>
		    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="<?php echo $financialgroup[0]->name; ?>" required="">
		  </div>
		  <div class="form-group">
			<label>Account Type:</label>
			<select class="form-control" name="type" id="type">
				<option value="DR" @if($financialgroup[0]->type=='DR') selected @endif>DR</option>
				<option value="CR" @if($financialgroup[0]->type=='CR') selected @endif>CR</option>
			</select>
		  </div>
		  <button type="submit" class="btn btn-primary">Update</button>
		</form>
		</div>
	</div>
</div>
</div>
@endsection