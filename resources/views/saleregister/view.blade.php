@extends('layouts.website')
@section('content')
<script>
	function printPage() {
	    document.title = "Sale Register";
	    window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
            <i class="fa fa-exchange"></i>
        </div>
		<div class="media-body">
		<h4>Sales Register</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li><a href="{{ url('/saleregister') }}">Sales Register</a></li>
		    <li>View</li>
		    <li>{{Request::segment(3)}}</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-8">
		  <h4 class="center onlyprint"><u>{{$saleregister[0]->company_name}}</u></h4>
<h5 class="center onlyprint">{{$saleregister[0]->address}}</h5><!--function updated by anurag company address-->
		  <table class="table">
            <thead>
              <tr>
                <th class="left-align" colspan="4">Order Details</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Invoice No:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$saleregister[0]->invoice_no}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Invoice Date:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{date_dfy($saleregister[0]->entry_date)}}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Client:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$saleregister[0]->client_name}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Mobile:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$saleregister[0]->client_mobile}}</td>
              </tr>
              <tr>
              	<td style="padding-top: 15px" class="left-align bt-none"><b>Ref:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$saleregister[0]->ref}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Sale Type:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$saleregister[0]->sale_type}}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Sales Person:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{getUserName($saleregister[0]->created_by)}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Remarks:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$saleregister[0]->remarks}}</td>
              </tr>
            </tbody>
	    </table> 
      	</div>
	</div>
	<div class="row">
	  <div class="col-md-10">
		<div class="table-responsive">
	          <table class="table table-bordered mb30">
	            <tbody>
	              <tr>
	                <td width="50px" class="left-align"><b>S.No</b></td>
	                <td class="left-align"><b>Name</b></td>
									<!-- <td class="left-align"><b>Batch No</b></td> -->
	                <td class="left-align"><b>Strength</b></td>
	                <td class="left-align"><b>Unit</b></td>
	                <td class="left-align"><b>Brand</b></td>
	                <td class="left-align"><b>Form</b></td>
<!---	                <td class="left-align"><b>Barcode</b></td>
	                <td class="left-align"><b>Expiry Date</b></td>
-->
	                <td class="left-align"><b>Quantity</b></td>
	                <td class="left-align"><b>Cost</b></td>
	                <td class="left-align"><b>Total</b></td>
	              </tr>
	              <?php $i=1; foreach ($saleregister_item as $value){ ?>
	              <tr>
					<td class="left-align">{{ $i }}</td>
					<td class="left-align">{{ $value->item_name }}</td>
					<!-- <td class="left-align">{{ $value->batch_no }}</td> -->
					<td class="left-align">{{ $value->strength }}</td>
					<td class="left-align">{{ $value->unit_name }}</td>
					<td class="left-align">{{ $value->brand_name }}</td>
					<td class="left-align">{{ $value->form_name }}</td>
<!---	                <td class="left-align">{{ $value->barcode }}</td>
	                <td class="left-align">{{ date_dfy($value->expiry_date) }}</td>
-->
					<td class="left-align">{{ $value->quantity }}</td>
					<td class="left-align">{{ $value->rate }}</td>
					<td class="left-align">{{ $value->amount }}</td>
	              </tr>
	              <?php $i++; }?>
	              <tr>
					<td colspan="8"	 class="right-align">Sub-Total:</td>
					<td class="left-align"><b>{{$saleregister[0]->sub_total}}</b></td>
	              </tr>
	              <tr>
					<td colspan="8" class="right-align">Discount:</td>
					<td class="left-align"><b>{{$saleregister[0]->discount}}</b></td>
	              </tr>
	              <tr>
					<td colspan="8" class="right-align">Tax:</td>
					<td class="left-align"><b>{{$saleregister[0]->tax}}</b></td>
	              </tr>
	              <tr>
					<td colspan="8" class="right-align">Total:</td>
					<td class="left-align"><b>{{$saleregister[0]->total}}</b></td>
	              </tr>
	            </tbody>
	          </table>
          </div>
	  </div>
	</div>
</div>
</div>
@endsection