@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="media-body">
		<h4>Notifications</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Notifications</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Item</th>
		        <th>Category</th>
		        <th>Brand</th>
		        <!--<th>Manufacturer</th>-->
		        <th>Form</th>
		        <th>In Stock</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($notification as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->name }}</td>
	            <td>{{ $value->category_name }}</td>
	            <td>{{ $value->brand_name }}</td>
	            <!--<td>{{ $value->manufacturer_name }}</td>-->
	            <td>{{ $value->form_name }}</td>
	            <td>{{ $value->in_stock }}</td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    });
});
</script>
@endsection