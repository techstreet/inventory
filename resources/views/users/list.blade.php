@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
            <i style="padding: 8px 0 0 0" class="fa fa-users"></i>
        </div>
        @if(Auth::user()->user_group != '4')
		<div class="pull-right">
			<a href="{{ url('/users/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add User</button></a>
		</div>
		@endif
		<div class="media-body">
		<h4>Users</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Users</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th>S.No</th>
		        <th>Name</th>
		        <th>Email</th>
		        <th>Mobile</th>
		        <th>Address</th>
		        <th width="200px" style="min-width: 200px;">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($users as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->name }}</td>
	            <td>{{ $value->email }}</td>
	            <td>{{ $value->mobile }}</td>
	            <td>{{ $value->address }}</td>
	            <td>
	            @if(Auth::user()->user_group != '4')
	            <a href="{{ url('/users/login/'.$value->id) }}" data-toggle="tooltip" title="Login" class="btn btn-success" data-original-title="Login"><i class="fa fa-sign-in"></i></a>
	            @endif
	            <a href="{{ url('/users/view/'.$value->id) }}" data-toggle="tooltip" title="View" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a>
	            @if(Auth::user()->user_group != '4')
	            <a href="{{ url('/users/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
	            <a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/users/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
	            @endif
	            </td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
	$('#dataTable').dataTable( {
	  "ordering": false
	} );
});
</script>
@endsection