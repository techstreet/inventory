@extends('layouts.website')
@section('content')
<script>
$(function(){
$(".basic-select").select2();
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('.input-group.date').datepicker({
    calendarWeeks: true,
    todayHighlight: true,
    autoclose: true,
    format: "dd-MM-yyyy",
    //startDate: today
});  
});
function printPage() {
    document.title = "Debt Report";
    window.print();
}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
            <i class="fa fa-file-text-o"></i>
        </div>
		<div class="media-body">
		<h4>Debt Report</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Reports</li>
		    <li>Debt Report</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
		<div class="col-md-12">
		@include('flashmessage')
			<form class="form-inline" method="post" action="">
			{{ csrf_field() }}
				<div class="form-group" style="margin-right: 0">
					<div class="input-group">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="debt" id="debt" placeholder="Debt" required="">
						<option value="">Select</option>
						<option value="1">1 Month Old</option>
						<option value="2">2 Month Old</option>
						<option value="3">3 Month Old</option>
						<option value="6">6 Month Old</option>
						<option value="12">1 Year Old</option>
						<option value="13">1+ Year Old</option>
					</select>
					</div>
				</div>
				<button type="submit" name="submit" class="btn btn-info">Find</button>
			</form>
		</div>
	</div>
	<?php if($_POST && $_POST > 0){ ?>     
    <div class="row" style="margin-top: 10px">
	    <div class="col-md-12">
	    @include('flashmessage')
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th>S.No</th>
		        <th>Customer</th>
		        <th>Contact name</th>
		        <th>Email</th>
		        <th>Phone</th>
		        <th>Mobile</th>
		        <th>Address</th>
		        <th>Amount</th>
		      </tr>
		    </thead>
		    <tbody>
			<?php 
				if(!empty($debtreport['items'])){
			?>

				
		      @foreach ($debtreport as $key=>$value)
	          <?php if($value->debit-$value->credit > 0){ ?>
	          <tr>
	            <td>{{ ++$key }}</td>
	            <td><a href="{{url('customer/account/'.$value->customer_id)}}">{{ $value->customer_name }}</a></td>
	            <td>{{ $value->contact_name }}</td>
	            <td>{{ $value->email }}</td>
	            <td>{{ $value->phone }}</td>
	            <td>{{ $value->mobile }}</td>
	            <td>{{ $value->address }}</td>
	            <td>{{ $value->debit-$value->credit }}</td>
	          </tr>
				<?php }?>
		          @endforeach
				<? } 		   	
				if(empty($debtreport['items'])){
				?>
	          <tr>
	            <td colspan="8">There is no Record Found.</td>
	          </tr>				
				<?php
				}?>

		    </tbody>
		  </table>
		</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
@endsection