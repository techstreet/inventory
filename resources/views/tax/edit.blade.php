@extends('layouts.website')
@section('content')
<script type='text/javascript'>
$(function(){
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('.input-group.date').datepicker({
    calendarWeeks: true,
    todayHighlight: true,
    autoclose: true,
    format: "dd-MM-yyyy",
    startDate: today
});  
});
</script>
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>Taxes</h4>
        <ul class="breadcrumb">
            <li><a href="{{url('')}}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Masters</li>
            <li><a href="{{url('/tax')}}">Taxes</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		@include('flashmessage')
      	<form method="post" action="{{ url('/tax/update/'.$tax[0]->id) }}">
      	{{ csrf_field() }}
		  <div class="form-group">
		    <label>*Name:</label>
		    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="<?php echo $tax[0]->name; ?>" required="">
		  </div>
		  <div class="form-group">
		    <label>Description:</label>
		    <textarea rows="4" class="form-control" placeholder="Enter description" name="description" id="description" maxlength="400"><?php echo $tax[0]->description; ?></textarea>
		  </div>
		  <div style="text-align: right" id="description_characters"></div>
		  <div class="form-group">
		    <label>*Rate(%):</label>
		    <input type="number" class="form-control" placeholder="Enter rate" name="rate" id="rate" value="<?php echo $tax[0]->rate; ?>" min="0" step="0.01" required="">
		  </div>
		  <div class="form-group">
		      <label>*Effective From:</label>
			  <div class="input-group date">
				<input type="text" name="effective_from" id="effective_from" value="<?php echo date_dfy($tax[0]->effective_from); ?>" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			  </div>
		  </div>
		  <div class="form-group">
			<label>*Status:</label>
			  <select class="form-control" name="is_active" id="is_active">
			  	<option value="1" @if($tax[0]->is_active=='1') {{'selected'}} @endif >Active</option>
			  	<option value="0" @if($tax[0]->is_active=='0') {{'selected'}} @endif >Inactive</option>
			  </select>
		  </div>
		  <button type="submit" class="btn btn-primary">Update</button>
		</form>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    var text_max = 400;
    var text_length = $('#description').val().length;
    var text_remaining = text_max - text_length;
    $('#description_characters').html(text_remaining + ' characters left');

    $('#description').keyup(function() {
        var text_length = $('#description').val().length;
        var text_remaining = text_max - text_length;

        $('#description_characters').html(text_remaining + ' characters left');
    });
});
</script>
@endsection