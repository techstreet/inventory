@extends('layouts.website')
@section('content')
<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>Banks</h4>
            <ul class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Masters</li>
                <li><a href="{{ url('/bank') }}">Banks</a></li>
                <li>Add</li>
            </ul>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div id="page-wrapper">
		<div class="row">
			@include('flashmessage')
			<form method="post" action="{{ url('bank/save') }}">
				{{ csrf_field() }}
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h5 style="position: absolute;top: -18px;font-weight: bold;background: #fff;padding: 0 5px;">General Info</h5>
							<div class="form-group">
                                <label>*Bank Name:</label>
                                <input type="text" class="form-control" placeholder="Enter bank name" name="bank_name" id="bank_name" value="{{ old('bank_name') }}"
                                    required>
                            </div>
                            <div class="form-group">
                                <label>*Sort Code:</label>
                                <input type="text" class="form-control" placeholder="Enter sort code" name="sort_code" id="sort_code" value="{{ old('sort_code') }}"
                                    required>
                            </div>
                            <div class="form-group">
                                <label>*Bank Address:</label>
                                <textarea rows="4" class="form-control" placeholder="Enter bank address" name="bank_address" id="bank_address"
                                    maxlength="400" required>{{ old('bank_address') }}</textarea>
                            </div>
                            <div style="text-align: right" id="bank_address_characters"></div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-body">
							<h5 style="position: absolute;top: -18px;font-weight: bold;background: #fff;padding: 0 5px;">Account Info</h5>
							<div class="form-group">
                                <label>*Account Number:</label>
                                <input type="text" class="form-control" placeholder="Enter account number" name="account_no" id="account_no" value="{{ old('account_no') }}"
                                    required>
                            </div>
                            <div class="form-group">
                                <label>*Account Name:</label>
                                <input type="text" class="form-control" placeholder="Enter account name" name="account_name" id="account_name" value="{{ old('account_name') }}"
                                    required>
                            </div>
						</div>
					</div>
				</div>
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
</div>
<script>
    $(document).ready(function () {
        var text_max = 400;
        var text_length = $('#bank_address').val().length;
        var text_remaining = text_max - text_length;
        $('#bank_address_characters').html(text_remaining + ' characters left');

        $('#bank_address').keyup(function () {
            var text_length = $('#bank_address').val().length;
            var text_remaining = text_max - text_length;

            $('#bank_address_characters').html(text_remaining + ' characters left');
        });
    });
</script>
@endsection