@extends('layouts.website')
@section('content')
<?php
$from_date = '';
$to_date = '';
if($_POST && $_POST>0){
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
}
?>
<script>
$(function(){
$(".basic-select").select2();
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('.input-group.date').datepicker({
    calendarWeeks: true,
    todayHighlight: true,
    autoclose: true,
    format: "dd-MM-yyyy",
    //startDate: today
});  
});
function printPage(){
	window.print();
}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-file-text-o"></i>
        </div>
		<div class="media-body">
		<h4>Stock Register</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Stock Register</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
		<div class="col-md-12">
			<form class="form-inline" method="post" action="">
			{{ csrf_field() }}
				<div class="form-group" style="margin-right: 0">
					<div class="input-group">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="category" id="category" placeholder="Select Category">
						<option value="">Select Category</option>
						<option value="Purchase Register" <?php if($category_selected == 'Purchase Register'){echo "selected";} ?>>Purchase Register</option>
						<option value="Sale Register" <?php if($category_selected == 'Sale Register'){echo "selected";} ?>>Sale Register</option>
					</select>
					</div>
					<div class="input-group date">
						<input type="text" name="from_date" class="form-control" value="{{$from_date}}" placeholder="From Date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="To Date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
				<button type="submit" name="submit" class="btn btn-info">Find</button>
			</form>
		</div>
	</div>
	<?php if($_POST && $_POST > 0){ ?>     
    <div class="row" style="margin-top: 10px">
	    <div class="col-md-12">
	    @include('flashmessage')
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Item</th>
		        <th>Category</th>
		        <th>Quantity</th>
		        <th>Cost</th>
		        <th>Amount</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($stockregister as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->item_name }}</td>
	            <td>{{ $value->category_name }}</td>
	            <td>{{ $value->item_quantity }}</td>
	            <td>{{ $value->price }}</td>
	            <td>{{number_format($value->price*$value->item_quantity, 2)}}</td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
@endsection