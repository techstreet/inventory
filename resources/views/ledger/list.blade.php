@extends('layouts.website') @section('content')
<?php
use Carbon\Carbon;
$from_date = '';
$to_date = '';
if($_POST && $_POST>0){
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
}
?>
<script>
	$(function () {
		$(".basic-select").select2();
		$("#account-name").text($("#account option:selected").text());
		
		var debit_total = 0;
		var credit_total = 0;

		$('.debit-total').each(function(){
			var amount = $(this).text();
			if(amount == ''){
				amount = 0;
			}
			debit_total += parseFloat(amount);
		});
		$('.credit-total').each(function(){
			var amount = $(this).text();
			if(amount == ''){
				amount = 0;
			}
			credit_total += parseFloat(amount);
		});

		$('.debit-total-sum').text(debit_total);
		$('.credit-total-sum').text(credit_total);

		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.input-group.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
	});
	function printPage() {
		document.title = "Ledger";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-file-text-o"></i>
		</div>
		<div class="media-body">
			<h4>Ledger</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>Account Register</li>
				<li>Ledger</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12">
			<h4 class="center onlyprint"><u>{{Auth::user()->name}}</u></h4>
			<h5 class="center onlyprint">{{Auth::user()->address}}</h4>
				@include('flashmessage')
				<form class="form-inline" method="post" action="">
					{{ csrf_field() }}
					<div class="input-group">
						<select style="border: none; padding: 0px; min-width:200px" class="form-control basic-select" name="account" id="account">
						<option value="">Select</option>
							@foreach ($accounts as $value)
							<option value="{{ $value->id }}" <?php if($account_id==$value->id ){echo "selected";} ?>>{{ $value->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="input-group date">
						<input type="text" name="from_date" class="form-control" value="{{$from_date}}" placeholder="From Date" required="" autocomplete="off">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
					</div>
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="To Date" required="" autocomplete="off">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
					</div>
					<button type="submit" name="submit" class="btn btn-info">Find</button>
				</form>
			</div>
		</div>
		<?php if($_POST && $_POST > 0){ ?>
		<div class="row" style="margin-top: 10px">
			<div class="col-md-12">
				@include('flashmessage')
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable">
						<thead>
							<tr>
								<th id="account-name" colspan="8"></th>
							</tr>
							<tr>
								<th colspan="4">Debit</th>
								<th colspan="4">Credit</th>
							</tr>
							<tr>
								<th>Date</th>
								<th>Particulars</th>
								<th>J.F</th>
								<th>Amount</th>
								<th>Date</th>
								<th>Particulars</th>
								<th>J.F</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@for ($i = 0; $i < $count; $i++)
							<tr>
								<td>@if(isset($debit[$i]->created_at)) {{date_dfy($debit[$i]->created_at)}} @endif</td>
								<td>@if(isset($debit[$i]->name)) To {{$debit[$i]->name}} @endif</td>
								<td>@if(isset($debit[$i]->voucher_no)) {{$debit[$i]->voucher_no}} @endif</td>
								<td class="debit-total">@if(isset($debit[$i]->debit)) {{$debit[$i]->debit}} @endif</td>
								<td>@if(isset($credit[$i]->created_at)) {{date_dfy($credit[$i]->created_at)}} @endif</td>
								<td>@if(isset($credit[$i]->name)) By {{$credit[$i]->name}} @endif</td>
								<td>@if(isset($credit[$i]->voucher_no)) {{$credit[$i]->voucher_no}} @endif</td>
								<td class="credit-total">@if(isset($credit[$i]->credit)) {{$credit[$i]->credit}} @endif</td>
							</tr>
							@endfor

							<!-- To balance c/d OR By balance c/d -->
							@if($credit_sum > $debit_sum)
							<tr>
								<td>{{$to_date}}</td>
								<td>To balance c/d</td>
								<td>N/A</td>
								<td>{{$credit_sum-$debit_sum}}</td>
								<td colspan="4"></td>
							</tr>
							@endif
							@if($debit_sum > $credit_sum)
							<tr>
								<td colspan="4"></td>
								<td>{{$to_date}}</td>
								<td>By balance c/d</td>
								<td>N/A</td>
								<td>{{$debit_sum-$credit_sum}}</td>
							</tr>
							@endif

							<!-- Total -->
							<tr style="background: beige">
								<td colspan="3" style="text-align:right">Total:</td>
								<td>@if($credit_sum > $debit_sum) {{empty_zero($credit_sum)}} @else {{empty_zero($debit_sum)}} @endif</td>
								<td colspan="3" style="text-align:right">Total:</td>
								<td>@if($credit_sum > $debit_sum) {{empty_zero($credit_sum)}} @else {{empty_zero($debit_sum)}} @endif</td>
							</tr>
							<!-- To balance b/d OR By balance b/d -->
							@if($debit_sum != $credit_sum)
							<tr>
								@if($debit_sum > $credit_sum)
								<td>{{ date_dfy(Carbon::parse($to_date)->addDays(1)) }}</td>
								<td>To balance b/d</td>
								<td>N/A</td>
								<td>{{$debit_sum - $credit_sum}}</td>
								<td colspan="4"></td>
								@else
								<td colspan="4"></td>
								<td>{{ date_dfy(Carbon::parse($to_date)->addDays(1)) }}</td>
								<td>By balance b/d</td>
								<td>N/A</td>
								<td>{{$credit_sum - $debit_sum}}</td>
								@endif
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
@endsection